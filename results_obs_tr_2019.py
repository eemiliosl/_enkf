# -*- coding: utf-8 -*-
"""
Plot some results from the EnKF assimilation
Created on Thursday 27 October 11:50:00 2015
@author: eesl
"""
import matplotlib as mpl
# mpl.use('Agg')
import numpy as np
import h5py
import sys
import os
import matplotlib.pylab as plt
plt.rcParams.update({'figure.max_open_warning': 0})
import scipy.stats as stats
import scipy.io as spio
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.mystats as mysst


def main():

    # Define important directories:
    source_dir = r'C:\Users\Emilio\Desktop\results_collection_2019_tt_field'
    test_id = 'test3a'

    enkf_data_dir = os.path.join(source_dir, 'enkf_results', test_id) # enkf_data_dir
    ref_data_file_enkf = 'Fieldtr_cum.dat'     #'Fieldfl_.dat'
    enkf_iter_steps = 34


    keg_data_dir = os.path.join(source_dir, 'keg_results', test_id.capitalize())


    keg_data_file = '3d_test_3aTR_results_transport_512mem_nodamp_03sigmah.mat'
                     # '3d_test_1b_results_heads_512mem_nodamp_03sigmah.mat'       '3d_test_3aTR_results_transport_512mem_nodamp_03sigmah.mat'
                     #   '3d_test_3a_results_heads_512mem_nodamp_03sigmah.mat'      '3d_test_2aTR_results_transport_512mem_nodamp_03sigmah_Tr2asecondround.mat'
                     #   '3d_test_3b_results_heads_512mem_nodamp_03sigmah.mat'

    # enkf_data_dir = r'Z:\Sanchez\FromHome\enkf_storetemp\RefinedGrid\2d\transport\2d_test2\realizations_2d_test2g_01df_noga_from_fl2e\modobs'\
    #                  # r'Z:\Sanchez\FromHome\enkf_storetemp\RefinedGrid\2d\transport\2d_test2\realizations_2d_test2i_001df_noga_from_fl2e\modobs'

    # Define the errors
    norm_data = False
    corr_coef = np.array([])
    mean_abs_error_enkf = np.array([])
    normres_enkf = np.array([])
    mean_res = np.array([])
    rmse_enkf = np.array([])

    ref_data = np.loadtxt(os.path.join(enkf_data_dir, ref_data_file_enkf ))
    # r'D:\Users\Emilio\Desktop\EnKF_paper\synthetic\model_output\Fieldtr_cum2.dat')

    # Load KEG data:
    # normres_keg = spio.loadmat(os.path.join(keg_heads_dir, 'keg_norm_res_heads1.mat'), squeeze_me=True)
    # normres_keg= normres_keg['normres']
    data_keg_all = h5py.File(os.path.join(keg_data_dir, keg_data_file), 'r')# spio.loadmat(os.path.join(keg_data_dir, keg_data_file), squeeze_me=True)  # ,'heads_pt1_after_ALL_assim.mat'),
    keg_mod, keg_ref = data_keg_all['c_sim'].value, data_keg_all['data_c'].value

    #heads_keg_ref = heads_keg_ref[heads_keg_ref>0]
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    # line_XY45 = np.linspace(ref_data.min()*0.9, ref_data.max()*1.1, 1000)
    # ax.plot(line_XY45, line_XY45, '--', color='grey', label='perfect fit line')

    """
    Load & treat EnKF data
    """
    for ii in range(1, enkf_iter_steps+1):
        data_enkf0 = np.load(os.path.join(enkf_data_dir,'modobs','ModMeas_tr_iter001_timestep_%.3d.npy' % ii) )

        # These are the normalized residuals:
        # for jj in range(0, 500):
        #     #cur_normres_enkf = (ref_data_dr[ii - 1, :] - heads0[:, jj])/np.sqrt(0.001)
        #     cur_normres_enkf = (ref_data[ii - 1, :] - data_enkf0[:, jj]) / ref_data[ii - 1, :]*.25 # ref_data[ii - 1, :]*.25    np.sqrt(0.005)
        #     normres_enkf = np.r_[normres_enkf, cur_normres_enkf]
        #     # plt.plot(heads0[:,jj], ref_data_dr[ii - 1, :], color='grey')

        meanheads0 = np.nanmean(data_enkf0, axis=-1)
        std_heads0 = np.nanstd(data_enkf0, axis=-1)

        if norm_data is True:
            ref_data_dr_norm = (ref_data[ii - 1, :] - np.nanmin(ref_data[ii - 1, :])) / (
                        np.nanmax(ref_data[ii - 1, :]) - np.nanmin(ref_data[ii - 1, :]))

            heads0norm = (meanheads0 - np.nanmin(meanheads0))/(np.nanmax(meanheads0) - np.nanmin(meanheads0))

            ax.plot(ref_data_dr_norm, heads0norm, '.', color='black')
        else:
            ax.plot(ref_data[ii - 1, :], meanheads0, '.', color='black')

        corr_coef = np.r_[corr_coef, mysst.Pearson(ref_data[ii - 1, :], np.nanmean(data_enkf0, axis=-1))[0]]
        mean_abs_error_enkf = np.r_[mean_abs_error_enkf, mysst.mean_abs_error(ref_data[ii - 1, :], np.nanmean(data_enkf0, axis=-1))]
        rmse_enkf = np.r_[rmse_enkf, np.sqrt(mysst.mean_squared_error(ref_data[ii - 1, :], np.nanmean(data_enkf0, axis=-1)))]

        # Plot error bars for last assimilation step
        if ii == enkf_iter_steps:
            if norm_data is True:
                ax.plot(ref_data_dr_norm, heads0norm, 's', color='blue', ms=5)
            else:
                #ax.plot( ref_data_dr[ii - 1, :], meanheads0,'s', color='blue', ms=5)
                ax.errorbar(ref_data[ii - 1, :], meanheads0, yerr=std_heads0,
                           fmt='bo', ecolor='b', capthick=2)

    keg_mod = keg_mod.T
    keg_ref = keg_ref.squeeze()
    rmse_enkf = np.mean(rmse_enkf)
    rmse_keg = np.sqrt(mysst.mean_squared_error(np.nanmean(keg_mod, axis=-1), keg_ref))

    mean_mean_abs_error_enkf = np.nanmean(mean_abs_error_enkf)
    mean_mean_abs_error_keg = np.nanmean(mysst.mean_abs_error(keg_ref, np.nanmean(keg_mod, axis=-1)))

    mean_cor_coef_enkf = np.nanmean(corr_coef)
    mean_cor_coef_keg, trash = mysst.Pearson(keg_ref, np.nanmean(keg_mod, axis=-1))

    #ax.errorbar(10-keg_ref,10.-np.nanmean(keg_mod, axis=-1), yerr=np.nanstd(keg_mod, axis=-1), fmt='rs', ecolor='red', capthick=2)
    ax.errorbar(keg_ref, np.nanmean(keg_mod, axis=-1), yerr=np.nanstd(keg_mod, axis=-1), fmt='ro',
                ecolor='red', capthick=2)

    ax.set_xlim([-0.05,0.2])
    ax.set_ylim([-0.05,0.2])
    ax.set_aspect(aspect=1)


    plt.figure(2)
    n, bins, patches = plt.hist(normres_enkf[-int(500 * 20):] , 60, density=1, color='grey', histtype='step')
    (mu, sigma) = stats.norm.fit(normres_enkf[-int(500 * 20):])
    y = stats.norm.pdf(bins, mu, sigma)
    l = plt.plot(bins, y, '--', color='grey', linewidth=2)


    plt.figure(2)
    n, bins, patches = plt.hist(normres_keg, 60, density=1, color='blue', histtype='step')
    (mu, sigma) = stats.norm.fit(normres_keg)
    y = stats.norm.pdf(bins, mu, sigma)
    l = plt.plot(bins, y, 'b--', linewidth=2)
    plt.grid()

    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #
    modelname = 'test2b'
    remarks = 'with_initialK_hete_transpparam'
    mymode = 'tr_'  # 'fl_' or 'tr_'
    # Filter settings:
    noreal = 300
    parallel = True
    nocpu = 4

    # Model output settings:

    # For single-updated-step observations (assimilated):
    assim_obs = True
    moments = False
    cumbtc = True
    porosity = True  # Update porosity (homogeneous)
    storativity = False  # Update storativity (homogeneous)
    dispersivity = True  # Update dispersivity (homogeneous)

    # For a full forward run to obtain final updated observations (updated)
    updated_obs = True
    resetheads = False
    runmodel = True
    remove = True
    neg2zero_upd = True
    moments_upd = False
    cumbtc_upd = False
    head2dwdn_upd = True
    norm_upd = False
    # If transport:
    ini_head = 10
    injection_point = [44.637, 59.90, 3.0] #[44.637, 59.90, 1.0]
    injected_mass = 0.001223

    # Output settings:
    plot = True  # Open plot windows?
    eps = True
    flag = False        # Just support variable, no need to modify
    """ """
    # Figure directories:
    # figdir = os.path.join(results_dir, '_figs')
    mainPath = os.path.join(dm.homedir(), '_dataAssim', modelname)
    # mainPath = os.path.join(r'E:\phd\_DataAssimilation\models', modelname)
    str_assimilation = '%s%s' % (modelname, remarks)
    results_dir = os.path.join(mainPath, 'results_%s' % str_assimilation)
    modeldatadir = os.path.join(mainPath, 'model_data')
    realizationdir = os.path.join(mainPath, 'realizations_%s' % str_assimilation)
    res_obsdir = os.path.join(results_dir, 'observations')
    sourc_obsdir = os.path.join(realizationdir, 'modobs')
    allprocDir = os.path.join(realizationdir, 'allproc')

    if not os.path.isdir(allprocDir):
        flag = True
        kkk_dir = os.path.join(realizationdir, 'kkk')
        last_kkk_file = dm.getdirs(kkk_dir, mystr='kkk_%s' % mymode, fullpath=True, onlylast=True)
        last_kkk = np.load(last_kkk_file)
        nnodes, nelem = gm.readmesh(os.path.join(mainPath, '%sToCopy' % mymode, 'meshtecplot.dat'),
                                    fulloutput=False, gridformat='2018')
        selgrid_elem = np.load(os.path.join(mainPath, '%sToCopy' % mymode, 'subGrid.elements.npy'))

    if mymode is 'fl_':
        myylabel = 'Drawdown [m]'
        obsString = 'observation_well_flow'
        myylabel_rmse = 'RMSE[meters]'
    elif mymode is 'tr_':
        obsString = 'observation_well_conc'
        myylabel = 'Concentration [g/L]'
        myylabel_rmse = 'RMSE[g/L]'
    for cur_dir in [results_dir, res_obsdir]:
        if os.path.isdir(cur_dir) is False:
            os.makedirs(cur_dir)

    # --------------------------------------------------- #
    # --------------------------------------------------- #
    # --------------------------------------------------- #
    # -------- Start with the calculations -------------- #
    # 0. Get the nodes and elements of the area of the model of interest
    # gridFile = os.path.join(mainPath, '%sToCopy' % mymode, 'meshtecplot.dat')
    # inNodes, inElem = gm.sel_grid(gridFile, np.array(xlim), np.array(ylim), np.lim(zlim))

    # 1. Load wells and adjust its coordinates for plotting:
    # ---------------------------------------------------
    well_coord = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(1, 2, 3))
    wells_ids = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(0,), dtype='S')
    well_coord = well_coord
    wells_ids = wells_ids

    # Plotting settings:
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    mystr = 'ModMeas_%s' % mymode

    # Define all relevant files in a list:
    lstresdir = dm.getdirs(sourc_obsdir, mystr=mystr, fullpath=False, onlylast=False)
    print('Using directory << %s >> ' % sourc_obsdir)
    print('Files to work with: \n\t%s...' % lstresdir[0])

    # 2. Load field ( or synthetic) observations:
    # ---------------------------------------------------------------- #
    if (moments is True) and (cumbtc is False):
        special_str = 'moments'
    elif (moments is False) and (cumbtc is True):
        special_str = 'cum'
    else:
        special_str = ''
    fielddata = np.loadtxt(os.path.join(modeldatadir, 'Field%s%s.dat' % (mymode, special_str)))
    outputtimes = np.loadtxt(os.path.join(modeldatadir, '_outputtimes%s.dat' % mymode))

    # 3.Load model outputs:
    # ---------------------
    if assim_obs is True:
        ModelHeads = np.zeros((len(well_coord), noreal, len(outputtimes)))
        for cur_id, cur_file in enumerate(lstresdir):
            cur_data = np.load(os.path.join(sourc_obsdir, cur_file))
            ModelHeads[:, :, cur_id] = cur_data

        # stdheads = np.std(ModelHeads, axis=1)
        # 4. Plot modeled observations and real (or synthetic) ones:
        # ---------------------
        for well_id, cur_well in enumerate(wells_ids):
            savefile = os.path.join(res_obsdir, '%s_%s.png' % (cur_well.astype(np.str), mymode))
            # meanheads = np.mean(ModelHeads[well_id, :, :], axis=0)
            if plot is True:
                myplt.xy_percentileplot(outputtimes, ModelHeads[well_id, :, :], ci=[5, 95], #[16, 84],
                                        Y_real=fielddata[:, well_id],
                                        median=True, title='%s' % cur_well.astype(np.str),
                                        xlabel='Time [seconds]',
                                        ylabel=myylabel,
                                        savefile=savefile,
                                        eps=True)

        # # 5. Calculate some statistics per time step:
        # # --------------------------------------------
        rmse = []
        nash = []
        rel_error = []
        aae = []
        aesd = []
        for ii in range(0, len(outputtimes)):
            cur_mean = np.mean(ModelHeads[:, :, ii], axis=1)
            rmse.append(np.sqrt(mysst.mean_squared_error(cur_mean, fielddata[ii, :])))
            nash.append(mysst.nash_sutcliffe(cur_mean, fielddata[ii, :]))
            rel_error.append(np.abs(np.mean(mysst.relative_error(cur_mean, fielddata[ii, :]))))
            aae.append(mysst.mean_abs_error(cur_mean, fielddata[ii, :]))
            aesd.append(mysst.mean_ensemble_std(ModelHeads[:, :, ii], cur_mean))

        np.savetxt(os.path.join(res_obsdir, 'rmse_ns_%s.txt' % mymode), np.array((rmse,nash,rel_error, aae, aesd)).T, header='rmse, nash, rel_error, aae, aesd')

        fig, (axarr1, axarr2, axarr3) = plt.subplots(3, 1)
        axarr1.set_title('RMSE', fontsize=9)
        axarr1.tick_params(axis='both', which='major', labelsize=7)
        axarr1.plot(rmse[1:], 'k-o')
        axarr1.grid()
        axarr1.set_ylabel(myylabel_rmse, fontsize=8)

        axarr2.tick_params(axis='both', which='major', labelsize=8)
        axarr2.set_title('Nash-Sutcliffe coef.', fontsize=9)
        axarr2.plot(nash[1:], 'k-o')
        axarr2.set_ylim([np.min(nash) * .97, np.max(nash) * 1.02])
        axarr2.set_ylabel('NS [-]', fontsize=8)
        axarr2.grid()

        axarr3.tick_params(axis='both', which='major', labelsize=8)
        axarr3.set_title('Relative error', fontsize=9)
        axarr3.plot(rel_error[1:], 'k-o')
        axarr3.set_ylim([np.min(rel_error) * .97, np.max(rel_error) * 1.02])
        axarr3.set_ylabel('Rel. error [-]', fontsize=8)
        axarr3.set_xlabel('Time step', fontsize=8)
        axarr3.grid()

        savefile_rms = os.path.join(res_obsdir, 'rmse_ns_%s.png' % mymode)
        plt.savefig(savefile_rms, dpi=600, format='png')
        plt.savefig('%s.eps' % savefile_rms[:-4], dpi=600, format='eps')
        plt.close()

        # Likelihoods:
        # # --------------------------------------------
        if plot is True:
            lk_list = dm.getdirs(os.path.join(realizationdir, 'likelihood'), mystr=mymode, fullpath=False,
                                        onlylast=False)
            # lk_list = os.listdir(os.path.join(realizationdir, 'likelihood'))
            lk_all = []
            for ii in range(0, len(lk_list)):
                cur_lk = np.load(os.path.join(realizationdir, 'likelihood', lk_list[ii]))
                lk_all.append(np.mean(cur_lk))
            lk_all = np.asarray(lk_all)
            plt.plot(np.arange(0, len(lk_list), 1), lk_all, 'bo-')
            # plt.xlim([-100, outputtimes[-1]])
            plt.xlabel('Iteration')
            plt.ylabel('$\Phi_{OF}$')
            plt.grid()
            # plt.legend(loc='best', shadow=True)

            # fig.text(0.5, 0.5, 'Q=10 [L/s]', fontsize=11, verticalalignment='top', bbox=props)
            plt.savefig(os.path.join(results_dir, 'ObjFun_%s.png' % mymode), dpi=600, format='png')
            if eps is True:
                plt.savefig(os.path.join(results_dir, 'ObjFun_%s.eps' % mymode), dpi=600, format='eps')
            plt.close()

        # Abs Mean norm error:
        # # --------------------------------------------
        if plot is True:
            res_list = dm.getdirs(os.path.join(realizationdir, 'residuals'), mystr=mymode, fullpath=False,
                                         onlylast=False)
            # res_list = os.listdir(os.path.join(realizationdir, 'residuals'))
            mean_norm_error = []
            for jj in range(0, len(res_list)):
                cur_res = np.load(os.path.join(realizationdir, 'residuals', res_list[jj]))
                norm_error = np.abs(cur_res).mean(axis=1) / fielddata[jj, :]
                mean_norm_error.append(np.mean(norm_error))
            mean_norm_error = np.asarray(mean_norm_error)
            plt.figure(1)
            plt.plot(np.arange(0, len(res_list), 1), np.abs(mean_norm_error) / np.max(np.abs(mean_norm_error)), 'bo-')
            # plt.xlim([-100, outputtimes[-1]])
            plt.xlabel('Iteration')
            plt.ylabel('Mean Normalized Residuals')
            plt.grid()
            # plt.legend(loc='best', shadow=True)
            # fig.text(0.5, 0.5, 'Q=10 [L/s]', fontsize=11, verticalalignment='top', bbox=props)
            plt.savefig(os.path.join(results_dir, 'Residuals_norm_%s.png' % mymode), dpi=600, format='png')
            if eps is True:
                plt.savefig(os.path.join(results_dir, 'Residuals_norm_%s.eps' % mymode), dpi=600, format='eps')
            plt.close()

            plt.figure(2)
            plt.plot(np.arange(0, len(res_list), 1), np.abs(mean_norm_error), 'bo-')
            # plt.xlim([-100, outputtimes[-1]])
            plt.xlabel('Iteration')
            plt.ylabel('Mean Residuals')
            plt.grid()
            # plt.legend(loc='best', shadow=True)
            # fig.text(0.5, 0.5, 'Q=10 [L/s]', fontsize=11, verticalalignment='top', bbox=props)
            plt.savefig(os.path.join(results_dir, 'Residuals_%s.png' % mymode), dpi=600, format='png')
            if eps is True:
                plt.savefig(os.path.join(results_dir, 'Residuals_%s.eps' % mymode), dpi=600, format='eps')
            plt.close()
    #
    # 6. Test final fields by running the full model using them for all realizations:
    # Models should have been run previously using the function fwdHGS_batch
    # For that run the following commands:
    # # --------------------------------------------

    # Reset heads:
    # ---------------------#
    if resetheads is True:
        #for ii in range(0, len(os.listdir(allprocDir))):
        for ii in range(0, noreal):
            if flag is True:
                hg.prepfwd(ii, mainPath, mymode, 1, outputtimes, headsFile='', str_assim=str_assimilation,
                           type_update='full', concFile='', porosity=False,
                           storativity=False, dispersivity=False)
                curkkk = np.ones((nelem,))*np.mean(last_kkk[:,ii])
                curkkk[selgrid_elem] = last_kkk[:,ii]
                np.savetxt(os.path.join(allprocDir, 'proc_%.5d' % (ii + 1), '%s%.5d' % (mymode, ii + 1), 'kkkGaus_%.5d.dat' %(ii+1)),
                           np.transpose((np.arange(1, len(curkkk) + 1, 1),curkkk, curkkk, curkkk)),fmt='%d %.6e %.6e %.6e')

            curgrok = dm.getdirs(os.path.join(allprocDir, 'proc_%.5d' % (ii + 1), '%s%.5d' % (mymode, ii + 1)),
                                 mystr='.grok', fullpath=False, onlylast=True)
            # Back transform head command to run since beginning of test:
            if mymode is 'fl_':
                myreplace = {'Initial head from file': 'Initial head',
                             'iniheadsFlow_%.5d.dat' % (ii + 1): '%s' % ini_head}

            elif mymode is 'tr_':
                myreplace = {'restart file for concentrations': '!restart file for concentrations',
                             'tr_sim_%.5do.cen_prev' % (ii + 1): '!tr_simo.cen_prev'}

                xtrastr = ['Initial concentration', '0.000e+00',
                           'clear chosen zones\nclear chosen segments\nclear chosen nodes']
                xtrastr.extend(
                    ['choose node', '%f, %f, %f' % (injection_point[0], injection_point[1], injection_point[2])])
                xtrastr.extend(['\nspecified mass flux\n1', '0.0,1.0,%f' % injected_mass])

                hg.addstr2grok(os.path.join(allprocDir, 'proc_%.5d' % (ii + 1), '%s%.5d' % (mymode, ii + 1), curgrok),
                               startstr='!---Injection point', endstr='!---End injection point', str2add=xtrastr)

            mypath_dict = dm.gendirtree(mainPath, str_assim=str_assimilation, proc_idx=ii + 1, mode=mymode,
                                        porosity=porosity, storativity=storativity, dispersivity=dispersivity)

            hg.read_extraparam(ii, mypath_dict, mymode, initialize=False, porosity=porosity, storativity=storativity,
                               dispersivity=dispersivity, update_mprops=True)

            hg.changeStrGrok(os.path.join(allprocDir, 'proc_%.5d' % (ii + 1), '%s%.5d' % (mymode, ii + 1)), curgrok,
                             myreplace)

            batch_replace = {'grok.x': 'grok', 'hgs.x': 'phgs'}
            hg.changeStrGrok(os.path.join(allprocDir, 'proc_%.5d' % (ii + 1), '%s%.5d' % (mymode, ii + 1)),
                             'letmerun.bat', batch_replace)

    if updated_obs is True:
        if (moments_upd is True) and (cumbtc_upd is False):
            special_str = 'moments'
        elif (moments_upd is False) and (cumbtc_upd is True):
            special_str = 'cum'
        else:
            special_str = ''

        fielddata = np.loadtxt(os.path.join(modeldatadir, 'Field%s%s.dat' % (mymode, special_str)))
        if runmodel is True:
            files2keep = np.array(
                ['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'iniheads', 'parallelindx', '.png',
                 '.lic', obsString, 'head_pm.%0.3d' % len(outputtimes)])
            hg.fwdHGS_batch_caller(allprocDir, 'letmerun.bat', updateGrok=True, newtimes=outputtimes, parallel=parallel,
                                   cpus=nocpu,
                                   remove=remove, files2keep=files2keep, mymode=mymode)

        allprocDir = os.path.join(realizationdir, 'allproc')
        procfolderlist = os.listdir(allprocDir)
        obsFiles_all = np.empty((len(well_coord), noreal), dtype='<U300')

        # 3.Load model outputs:
        # ---------------------
        for idx, cur_procfolder in enumerate(procfolderlist):

            modelfolder = os.listdir(os.path.join(allprocDir, cur_procfolder))
            for idy, cur_modelfolder in enumerate(modelfolder):
                if mymode in cur_modelfolder:
                    cur_proc_dir = os.path.join(allprocDir, cur_procfolder, cur_modelfolder)
                    obsFiles_all[:, idx] = dm.getdirs(cur_proc_dir, mystr=obsString, fullpath=True,
                                                                 onlylast=False)
                    # cur_obsFiles, dummypath = dm.getdirs(cur_proc_dir, mystr=obsString, fullpath=True, onlylast=False)
                    # obsFiles_all[:, idx] = cur_obsFiles#[:-2]

        ModelHeads_updated = np.zeros((len(well_coord), noreal, len(outputtimes)))
        for id_process in range(0, noreal):
            for id_well, cur_well in enumerate(wells_ids):
                # modOut_old = np.loadtxt(obsFiles_all[id_well, id_process], skiprows=2, usecols=(0, 1))
                modOut = hg.rmodout(obsFiles_all[id_well, id_process], outputtimes, mymode)
                ModelHeads_updated[id_well, id_process, :] = hg.procmodout(modOut, outputtimes, ini_head=ini_head,
                                                                           neg2zero=neg2zero_upd,
                                                                           head2dwdn=head2dwdn_upd,
                                                                           cum=cumbtc_upd, norm=norm_upd,
                                                                           moments=moments_upd,
                                                                           mymode=mymode)
        np.save(os.path.join(realizationdir, 'ModelOutput_fullrunUpdParameters.npy'), ModelHeads_updated)
        for well_id, cur_well in enumerate(wells_ids):
            savefile = os.path.join(res_obsdir, '%s_updKfield_%s.png' % (cur_well.astype(np.str), mymode))
            # meanheads = np.mean(ModelHeads[well_id, :, :], axis=0)
            if plot is True:
                myplt.xy_percentileplot(outputtimes, ModelHeads_updated[well_id, :, :], ci=[5, 95],
                                        Y_real=fielddata[:, well_id],
                                        median=True, title='%s' % cur_well.astype(np.str),
                                        xlabel='Time [seconds]',
                                        ylabel=myylabel,
                                        savefile=savefile,
                                        eps=eps)
    print('Finished')


if __name__ == '__main__':
    main()

    # # Load model grid geometry and statistical parameters
    # print('Loading model grid data and statistical parameters...')
    # xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(statparam)
    # nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
    # node_coord = np.genfromtxt(grid_filedir, skip_header=3, max_rows=nnodes, usecols=(0, 1, 2))
    #
    # # Separate inner-small from outer-big grid if working with flow
    # biggrid_elem_ids = np.arange(1, elements + 1, 1)
    # smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)
