# -*- coding: utf-8 -*-
"""
Plot some results from the EnKF assimilation
Created on Thursday 27 October 11:50:00 2015
@author: eesl
"""
import matplotlib as mpl
# mpl.use('Agg')
import numpy as np
import sys
import os, shutil
import multiprocessing as mp
from itertools import repeat

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.plotResults as myplt
import addfunc.fieldgen as fg
import addfunc.dirs as dm
import addfunc.gridmanip as gm
import addfunc.hgs as hg
import addfunc.mystats as mysst
import addfunc.matmult.matmul_opt as mm
import addfunc.matmult.mylikelihood as mml
import matplotlib.pylab as plt
plt.rcParams.update({'figure.max_open_warning': 0})




def main():
    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #
    modelname = 'pt_test_1a_fl'
    mymode = 'fl_'  # 'fl_' or 'tr_'
    # Filter settings:
    noreal = 500
    parallel = True
    nocpu = 4
    linux = False
    runbatch = True
    # Model output settings:
    # For single-updated-step observations (assimilated):
    moments = False
    cumbtc = False
    neg2zero = False
    head2dwdn = True
    norm = False
    porosity = False  # Update porosity (homogeneous)
    storativity = False  # Update storativity (homogeneous)
    dispersivity = False  # Update dispersivity (homogeneous)

    # For a full forward run to obtain final updated observations (updated)
    update_file_strings = False
    runmodel = True
    remove = True
    # If transport:
    ini_head = 10
    injection_point = [35, 60, 1] #[44.637, 59.90, 1.0]
    injected_mass = 0.01

    # Output settings:
    plot = False  # Open plot windows?
    """ """
    # Figure directories:
    mainPath = r'D:\Users\Emilio\Desktop\model_predictions_enkf'
    results_dir = os.path.join(mainPath, 'results')
    modeldatadir = os.path.join(mainPath, 'model_data')
    realizationdir = os.path.join(mainPath,'realizations' )
    res_obsdir = os.path.join(results_dir, 'observations')
    # sourc_obsdir = os.path.join(realizationdir, 'modobs')
    allprocDir = os.path.join(realizationdir, 'allproc')

    kkk_dir = mainPath

    last_kkk_file = dm.getdirs(kkk_dir, mystr='kkk_', fullpath=True, onlylast=True)
    last_kkk = np.load(last_kkk_file)
    nnodes, nelem = gm.readmesh(os.path.join(modeldatadir, 'meshtecplot.dat'),
                                fulloutput=False, gridformat='tp')
    selgrid_elem = np.load(os.path.join(modeldatadir, 'subGrid.elements.npy'))

    if mymode is 'fl_':
        myylabel = 'Drawdown [m]'
        obsString = 'observation_well_flow'
        myylabel_rmse = 'RMSE[meters]'
    elif mymode is 'tr_':
        obsString = 'observation_well_conc'
        myylabel = 'Concentration [g/L]'
        myylabel_rmse = 'RMSE[g/L]'
    for cur_dir in [results_dir, res_obsdir]:
        if os.path.isdir(cur_dir) is False:
            os.makedirs(cur_dir)

    # --------------------------------------------------- #
    # --------------------------------------------------- #
    # --------------------------------------------------- #
    # -------- Start with the calculations -------------- #
    # 0. Get the nodes and elements of the area of the model of interest
    # gridFile = os.path.join(mainPath, '%sToCopy' % mymode, 'meshtecplot.dat')
    # inNodes, inElem = gm.sel_grid(gridFile, np.array(xlim), np.array(ylim), np.lim(zlim))

    # 1. Load wells and adjust its coordinates for plotting:
    # ---------------------------------------------------
    well_coord = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(1, 2, 3))
    wells_ids = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(0,), dtype='S')
    well_coord = well_coord
    wells_ids = wells_ids

    # 2. Load field ( or synthetic) observations:
    # ---------------------------------------------------------------- #
    fielddata = np.loadtxt(os.path.join(modeldatadir, 'Fieldfl_.dat'))
    outputtimes = np.loadtxt(os.path.join(modeldatadir, '_outputtimes%s.dat' % mymode))

    # Reset heads:
    # ---------------------#
    for ii in range(0, noreal):
        # os.makedirs(os.path.join(realizationdir,  '%.5d'))
        store_dir = os.path.join(realizationdir, '%.5d' %ii)
        shutil.copytree(os.path.join(mainPath, modelname), store_dir)
        curkkk = np.ones((nelem,))*np.exp(np.mean(np.log(last_kkk[:,ii])))
        curkkk[selgrid_elem] = last_kkk[:,ii]
        np.savetxt(os.path.join(store_dir, 'kkkGaus.dat'),
                   np.transpose((np.arange(1, len(curkkk) + 1, 1),curkkk, curkkk, curkkk)),fmt='%d %.6e %.6e %.6e')
        if runmodel is True:
            files2keep = np.array(
                ['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'iniheads', 'parallelindx', '.png',
                 '.lic', obsString, 'head_pm.%0.3d' % len(outputtimes)])
            mypool = mp.Pool(nocpu)
            mypool.starmap(hg.fwdHGS, zip(store_dir, repeat('%s' %ii)))
            mypool.close()
            mypool.join()

    allprocDir = os.path.join(realizationdir)
    procfolderlist = os.listdir(allprocDir)
    obsFiles_all = np.empty((len(well_coord), noreal), dtype='<U300')

    # 3.Load model outputs:
    # ---------------------
    for idx, cur_procfolder in enumerate(procfolderlist):

        modelfolder = os.listdir(os.path.join(allprocDir, cur_procfolder))
        for idy, cur_modelfolder in enumerate(modelfolder):
            if mymode in cur_modelfolder:
                cur_proc_dir = os.path.join(allprocDir, cur_procfolder, cur_modelfolder)
                obsFiles_all[:, idx] = dm.getdirs(cur_proc_dir, mystr=obsString, fullpath=True,
                                                             onlylast=False)

    ModelHeads_updated = np.zeros((len(well_coord), noreal, len(outputtimes)))
    for id_process in range(0, noreal):
        for id_well, cur_well in enumerate(wells_ids):
            # modOut_old = np.loadtxt(obsFiles_all[id_well, id_process], skiprows=2, usecols=(0, 1))
            modOut = hg.rmodout(obsFiles_all[id_well, id_process], outputtimes, mymode)
            ModelHeads_updated[id_well, id_process, :] = hg.procmodout(modOut, outputtimes, ini_head=ini_head,
                                                                       neg2zero=neg2zero,
                                                                       head2dwdn=head2dwdn,
                                                                       cum=cumbtc, norm=norm,
                                                                       moments=moments,
                                                                       mymode=mymode)

    np.save(os.path.join(realizationdir, 'ModelOutput_full_prediction.npy'), ModelHeads_updated)
    if plot is True:
        for well_id, cur_well in enumerate(wells_ids):
            savefile = os.path.join(res_obsdir, '%s_prediction_%s.png' % (cur_well.astype(np.str), mymode))
            myplt.xy_percentileplot(outputtimes, ModelHeads_updated[well_id, :, :], ci=[5, 95],  # [16, 84],
                                                                        Y_real=fielddata[:, well_id],
                                                                        median=True, title='%s' % cur_well.astype(np.str),
                                                                        xlabel='Time [seconds]',
                                                                        ylabel=myylabel,
                                                                        savefile=savefile,
                                                                        eps=True)
        # 5. Calculate some statistics per time step:
    # # --------------------------------------------
    rmse = []
    nash = []
    rel_error = []
    aae = []
    aesd = []
    for ii in range(0, len(outputtimes)):
        cur_mean = np.mean(ModelHeads_updated[:, :, ii], axis=1)
        rmse.append(np.sqrt(mysst.mean_squared_error(cur_mean, fielddata[ii, :])))
        nash.append(mysst.nash_sutcliffe(cur_mean, fielddata[ii, :]))
        rel_error.append(np.abs(np.mean(mysst.relative_error(cur_mean, fielddata[ii, :]))))
        aae.append(mysst.mean_abs_error(cur_mean, fielddata[ii, :]))
        aesd.append(mysst.mean_ensemble_std(ModelHeads_updated[:, :, ii], cur_mean))

    np.savetxt(os.path.join(res_obsdir, 'rmse_ns_%s.txt' % mymode), np.array((rmse,nash,rel_error, aae, aesd)).T, header='rmse, nash, rel_error, aae, aesd')


    Xdata = fielddata.flatten()
    Ydata= ModelHeads_updated.mean(axis=1).T.flatten()
    orderPol = 1
    Fit_line = np.polyfit(Xdata, Ydata, orderPol)
    p = np.poly1d(Fit_line)
    pearson_coef = mysst.Pearson(Xdata, Ydata)
    line_XY45 = np.linspace(np.min(Xdata) - 0.1, np.max(Xdata), 1000)

    if plot is True:
        fig1 = plt.figure(1)
        ax1 = fig1.add_subplot(111)

        ax1.plot(Xdata, Ydata, 'k.', markersize=4)
        plt.plot(line_XY45,line_XY45,'b-',linewidth = 2, label = 'perfect fit line')
        # plt.plot(Xdata,Xdata*Fit_line[0]+Fit_line[1],'k-',alpha = 1, label = 'regression line')
        ax1.plot(line_XY45, p(line_XY45), 'k-', alpha=0.5, label='regression line')
        ax1.grid(True)
        ax1.set_xlim([np.min(Xdata) - 0.01, np.max(Xdata) + 0.01])
        ax1.set_ylim([np.min(Xdata) - 0.01, np.max(Xdata) + 0.01])
        ax1.set_ylabel('Modeled heads [m]')
        ax1.set_xlabel('Field heads[m]')
        # plt.legend(numpoints=1)
        ax1.set_facecolor('gainsboro')
        myleg = ax1.legend(loc='best', shadow=True, numpoints=1, fontsize=14)
        # get the individual lines inside legend and set line width
        for line in myleg.get_lines():
            line.set_linewidth(2)
        plt.show()

    print("Correlation factor (Pearson's): %5.4e" % (pearson_coef[0]))

    print('Finished')

if __name__ == '__main__':
    main()

    # # Load model grid geometry and statistical parameters
    # print('Loading model grid data and statistical parameters...')
    # xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(statparam)
    # nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
    # node_coord = np.genfromtxt(grid_filedir, skip_header=3, max_rows=nnodes, usecols=(0, 1, 2))
    #
    # # Separate inner-small from outer-big grid if working with flow
    # biggrid_elem_ids = np.arange(1, elements + 1, 1)
    # smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)
