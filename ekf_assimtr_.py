"""
Data assimilation with a transport model in HGS
States (conc) = g/L
Parameters = s
Log parameters = Y
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon
"""
import os
import scipy
import scipy.sparse as ssp
import sys
import time
import numpy as np
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.hgs as hgr
import addfunc.gridmanip as gm
import addfunc.dirs as dm
import addfunc.fieldgen as fg
import addfunc.matmult.mylikelihood as mml
import addfunc.kalman as kf
import addfunc.mystats as mysst


def assimtr(modelname, **mysettings):
    # ---------------------#
    # %% Define some settings for the data assimilation:
    # %% Type of data to integrate, and type and magnitude of time step updating:
    # ---------------------#
    required_input = {'Model name': str, 'Comments': str, 'Model mode': str, 'Subgrid': bool, 'Update scheme': str,
                      'Gaussian anam': bool, 'Damping factor': float, 'Initialize': bool, 'No realizations': int,
                      'Restart': bool, 'Total iters': int, 'Cur iter': int, 'Cur time': int,
                      'Server': bool, 'No cpu': int, 'Cluster': bool, 'Parallel run': bool, 'Parallel matmult': bool,
                      'Gen logk fields': bool, 'Porosity': bool, 'Storativity': bool, 'Dispersivity': bool,
                      'Gen porosity': bool, 'Gen storativity': bool, 'Gen dispersivity': bool,
                      'Ini head': float,
                      'Exist modoutput': bool,
                      'Meas std': float,
                      'Relative meas std': bool,
                      'Percentage': float,
                      'Max meas std': float,
                      'Min meas std': float,
                      'neg2zero': bool, 'head2dwdn': bool, 'cumbtc': bool, 'norm': bool, 'moments': bool}
    # Transform input to proper variable type
    for cur_key, cur_val in mysettings.items():
        if cur_key in required_input:
            if required_input[cur_key] is bool:
                mysettings[cur_key] = required_input[cur_key](dm.str_to_bool(cur_val))
            else:
                mysettings[cur_key] = required_input[cur_key](cur_val)

    mymodel = modelname  # Model under investigation: e.g '07072015'
    remarks = mysettings['Comments']  # Provide remarks to identify various aspects of the assimilation
    mymode = mysettings['Model mode']  # Type of model to run: 'fl_' or 'tr_'
    subgrid = mysettings['Subgrid']  # Deal with subgrid or full model domain
    type_update = mysettings[
        'Update scheme']  # Filter flavour: 'std'(EnKF) 'restart'(R-EnKF), 'cum'(RR-EnKF), 'full'(KEG)

    porosity = mysettings['Porosity']  # Update porosity (homogeneous)
    storativity = mysettings['Storativity']  # Update storativity (homogeneous)
    dispersivity = mysettings['Dispersivity']  # Update dispersivity (homogeneous)
    gen_porosity = mysettings['Gen porosity']
    gen_storativity = mysettings['Gen storativity']
    gen_dispersivity = mysettings['Gen dispersivity']
    gen_extra_parameters = False  # Generate an ensemble of any of those extra parameters # todo: check this variable

    GausAnam = mysettings['Gaussian anam']  # Applied Gaussian Anamorphosys
    damp_fc = mysettings['Damping factor']  # Dampening factor for the Kalman gain
    dY_max = 1  # Max step control bound
    dY_min = 0.30  # Min step control bound

    cluster = mysettings['Cluster']
    storeINroot = False
    rootFolder = 'kalmanfilter2'  # Just if storeinRoot is True
    initialize = mysettings['Initialize']  # Start assimilation from time = t_ini?
    restart = mysettings['Restart']  # Restart a previous assimilation?
    paralrun = mysettings['Parallel run']  # Run fwd models in parallel?
    paral_matmult = mysettings['Parallel matmult']  # Perform matrix multiplications in parallel?
    ncpus = mysettings['No cpu']  # CPUs to use in assimilation OR matrix multiplication
    exist_modoutput = mysettings[
        'Exist modoutput']  # Exist model outputs for time step to assimilate? or run model first?

    total_iter = mysettings['Total iters']  # Total number of (outer) iterations
    cur_iter = mysettings[
        'Cur iter']  # Inner iteration: equal to the last inner iteration recorded in the results files
    cur_time = mysettings[
        'Cur time']  # Time step idx to assimilate. Automatic 0 if initialize==true, (last time step in result files) - 1 if initialize==False

    ini_head = mysettings['Ini head']  # Initial head if running flow

    relative_meas_stddev = mysettings['Relative meas std']
    errorPercent = mysettings['Percentage']
    max_meas_stddev = mysettings['Max meas std']  # 4.0e-6 ---> kg/m3, 5.0e-2 I used for the first runs -> millimeters
    min_meas_stddev = mysettings['Min meas std']
    meas_stddev = mysettings['Meas std']  # meas_stddev
    neg2zero = mysettings['neg2zero']
    head2dwdn = mysettings['head2dwdn']
    cumbtc = mysettings['cumbtc']  # This option is just for transport
    norm = mysettings['norm']
    moments = mysettings['moments']  # Invert using temporal moments rather than single points

    start = time.clock()
    scipy.random.seed()

    """ """
    """ If you are not sure on what's going on, don't modify the next section """
    """ """
    # Initialize the process according to selected  settings:
    # 0. Define/create directories:
    # -----------------------#
    mymaindir = os.path.join(dm.homedir(), '_dataAssim', mymodel)  # Inversion directory
    str_assimilation = '%s%s' % (mymodel, remarks)
    results_name = 'results_%s' % str_assimilation  # ID of the assimilation for a unique string in the results directory
    print('Working with model: << %s >>' % str_assimilation)

    mypath_dict = dm.gendirtree(mymaindir, str_assim=str_assimilation, proc_idx=-9999, mode=mymode,  porosity=porosity, storativity=storativity, dispersivity=dispersivity)

    # Store all settings:
    mylines_torec = []
    with open(os.path.join(mypath_dict['fld_modeldata'], '_RandFieldsParameters.dat'), 'r') as sett:
        mylines_torec.append(sett.readlines())
        mylines_torec.append(['\n', '\n'])
    if gen_porosity is True:
        with open(os.path.join(mypath_dict['fld_modeldata'], '_RandPoros.dat'), 'r') as sett:
            mylines_torec.append(sett.readlines())
            mylines_torec.append(['\n', '\n'])
    if gen_storativity is True:
        with open(os.path.join(mypath_dict['fld_modeldata'], '_RandStorage.dat'), 'r') as sett:
            mylines_torec.append(sett.readlines())
            mylines_torec.append(['\n', '\n'])
    if gen_dispersivity is True:
        with open(os.path.join(mypath_dict['fld_modeldata'], '_RandDispers.dat'), 'r') as sett:
            mylines_torec.append(sett.readlines())
            mylines_torec.append(['\n', '\n'])
    recfile = os.path.join(mypath_dict['fld_allproc'], '..', '%s_assimtr_.rec' % str_assimilation)
    print('Settings stored in < %s >' % recfile)
    with open(recfile, 'w') as recording:
        for cur_key, cur_val in mysettings.items():
            recording.write('%s :: %s\n' % (cur_key, cur_val))
        for cur_lin in mylines_torec:
            recording.writelines(cur_lin)

    store_path = os.path.join(mymaindir, results_name)
    if not os.path.exists(store_path):
        os.makedirs(store_path)
    fld_modeldata = mypath_dict['fld_modeldata']

    # # %% Read grid data:
    grid_filedir = os.path.join(mymaindir, '%sToCopy' % mymode, 'meshtecplot.dat')
    nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
    if subgrid is True:
        xlen, ylen, zlen, nxel, nyel, nzel, \
        xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, \
        lambda_y, lambda_z = fg.readParam(os.path.join(fld_modeldata, '_RandFieldsParameters.dat'))
        if os.path.isfile(os.path.join(mymaindir, mymode + 'ToCopy','subGrid.elements.npy')) is False:
            subgridnodes, subgridelem = gm.sel_grid(grid_filedir, xlim, ylim, zlim, store=True)
        subgridelem = np.load(os.path.join(mymaindir, mymode + 'ToCopy', 'subGrid.elements.npy'))
        subgridnodes = None

    # Read-in all field measurements if they exist. Dimensions: (Ntimes x Nobspoints)
    if (moments is True) and (cumbtc is False):
        special_str = 'moments'
    elif (moments is False) and (cumbtc is True):
        special_str = 'cum'
    else:
        special_str = ''
    measfile = 'Field%s%s.dat' % (mymode, special_str)
    fieldmeas = np.loadtxt(os.path.join(fld_modeldata, measfile))  # Note: Flat observations using .flatten('F')
    all_steptimes = np.loadtxt(os.path.join(fld_modeldata, '_outputtimes%s.dat' % mymode))  # .astype('f4')
    No_timeobs = fieldmeas.shape[0]

    if type_update == 'std':
        all_steptimes = np.r_[all_steptimes[0], np.diff(all_steptimes)]

    # Read well Labels:
    # Read well (model) information:
    ObsWell_name = np.loadtxt(os.path.join(fld_modeldata, '_obswellscoord%s.dat' % mymode), usecols=(0,), dtype='|S')
    try:
        ObsWell_node = np.loadtxt(os.path.join(fld_modeldata, '_obswellscoord%s.dat' % mymode), usecols=(-1,), dtype=int)
    except ValueError:
        ObsWell_node = np.loadtxt(os.path.join(fld_modeldata, '_obswellscoord%s.dat' % mymode), usecols=(-1,),
                                  dtype='|S')
        ObsWell_node = ObsWell_node.astype(float).astype(int)
    # ObsWell_coor = np.loadtxt(os.path.join(fld_modeldata, '_obswellscoord%s.dat' % mymode), usecols=(1, 2, 3))
    assert len(ObsWell_name) == fieldmeas.shape[1], "# of obs. points in data file =! # obs. wells in well labels file"

    # Initialize the time update loop:
    # -----------------------------------#
    if (initialize is True) and (type_update is not 'full'):
        cur_time = 0
    elif (restart is True) and (type_update is not 'full'):
        cur_time += 1

    while cur_time < No_timeobs:

        # Select the right shape of field data vector according to the flavor of KF selected:
        # -----------------------------------#
        if type_update == 'full':
            fieldmeas_length = fieldmeas.shape[0] * fieldmeas.shape[1]
            print('Preparing for a << KEG >> data assimilation')
            print('Initializing...')
        elif type_update == 'cum':
            fieldmeas_length = fieldmeas.shape[1] * (cur_time + 1.)
            cur_iter = 0
            print('Preparing for a << Restart-Accum EnKF >> data assimilation')
        elif (type_update == 'restart') or (type_update == 'std'):
            fieldmeas_length = fieldmeas.shape[1]
            cur_iter = 0
            if type_update == 'restart':
                print('Preparing for a << Restart EnKF >> data assimilation')
                print('Initializing time step no. %s (%s seconds), out of %s...' % (cur_time, all_steptimes[cur_time], No_timeobs - 1))
            elif type_update == 'std':
                print('Preparing for a << standard EnKF >> data assimilation')
                print('Initializing time step no. %s (%s seconds), out of %s...' % (cur_time, all_steptimes[cur_time], No_timeobs - 1))
        else:
            sys.exit('WRONG type of assimilation selected!')
        # if type_update is not 'full':
        #     print('Initializing time step no. %s (%s seconds), out of %s...' % (cur_time, all_steptimes[cur_time], No_timeobs - 1))
        # -----------------------------------#
        # Read Parameter fields: Y = log(k), and model outputs (or states e.g. heads or concentration)
        # If first iteration, read from received binary files:
        # -----------------------------------#
        if initialize is True:
            mylist = dm.getdirs(mypath_dict['fld_kkkfiles'], mystr='kkk_fl_iter', fullpath=False, onlylast=True)
            thefinaltime = int((mylist.split('_')[-1].split('.')[0]))
            Y = np.log(kf.read_iter_data(mypath_dict['fld_kkkfiles'], myType='kkk', my_timest='timestep_%.3d' % thefinaltime))
            nel, nreal = Y.shape
            # Generate extra parameter ensemble, if True:
            # if gen_extra_parameters is True:
            if porosity is True:
                if gen_porosity is True:
                    pp = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandPoros.dat', nreal)
                    np.save(os.path.join(mypath_dict['fld_porosity'], 'porosity_%siter000_timestep_000' % mymode), pp)

            if storativity is True: # Load of last fl_ iter storativity ensemble to start transport
                if gen_storativity is True:
                    ss = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandStorage.dat', nreal)
                    np.save(os.path.join(mypath_dict['fld_storativity'], 'storativity_%siter000_timestep_000' % mymode), ss)
                if gen_storativity is False:
                    fl_storage = dm.getdirs(mypath_dict['fld_storativity'], mystr='storativity_fl_', fullpath=True,
                                               onlylast=True)
                    fl_storage_ens = np.load(fl_storage)
                    np.save(os.path.join(mypath_dict['fld_storativity'], 'storativity_%siter000_timestep_000' % mymode), fl_storage_ens)

            if dispersivity is True:
                if gen_dispersivity is True:
                    dd = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandDispers.dat', nreal)
                    np.save(os.path.join(mypath_dict['fld_dispersivity'], 'dispersivity_%siter000_timestep_000' % mymode), dd)

            xtra_param, xtra_param_idx = hgr.read_extraparam('dummy', mypath_dict, mymode, initialize=initialize,
                                                             porosity=porosity, storativity=storativity,
                                                             dispersivity=dispersivity, update_mprops=False)
            # Read the final heads from flow run:
            # ini_head = kf.read_iter_data(mypath_dict['fld_heads'], myType='hydheads', my_timest='timestep_%.3d' % thefinaltime)
            # Read first model outputs if they were already computed:
            try:
                modmeas = kf.read_iter_data(mypath_dict['fld_modobs'], myType='ModMeas_%s' % mymode, my_timest='timestep_001')
                # num_modmeas = modmeas.shape[0]
            except (UnboundLocalError, FileNotFoundError):  # Make a first model run to get model outputs for transport:
                print('<< Model outputs for 1st  delta t in transport not found, running models...>>')
                exist_modoutput = False
                pass

            # if exist_modoutput is True:
            #     num_modmeas, modmeas = hgr.rdmodelout_bin(mypath_dict['fld_modobs'], mymode=mymode)
            # this one option gives a flatten array

            if exist_modoutput is False:
                print('Getting initial array of model outputs...')
                modmeas = hgr.caller_getstates(Y, mymaindir, str_assimilation, nnodes, mymode, fieldmeas_length,
                                               cur_time, all_steptimes,
                                               type_update, parallel=paralrun, cpus=ncpus, genfield=False,
                                               plot=False, initial_head=ini_head, neg2zero=neg2zero,
                                               head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments,
                                               OWnode=ObsWell_node, porosity=porosity, storativity=storativity,
                                               dispersivity=dispersivity,
                                               subgrid=subgrid, subgridnodes=subgridnodes,
                                               subgridelem=subgridelem)
                np.save(os.path.join(mypath_dict['fld_modobs'], 'ModMeas_%siter001_timestep_001.npy' % mymode), modmeas)
                # num_modmeas = modmeas.shape[0]
            initialize = False

        # elif initialize is False:  # Means process comes from previous iter-step, no need to read files!
        #     if cur_time >= No_timeobs:
        #         sys.exit('Done with the last time step update. Exiting...')
        if restart is True:  # Means the inversion was stopped and now we want to read results from last full iteration!
            Y = np.log(kf.read_iter_data(mypath_dict['fld_kkkfiles'], myType='kkk_%s' % mymode, my_timest='timestep_%.3d' % cur_time))
            # ini_head = kf.read_iter_data(mypath_dict['fld_heads'], myType='hydheads_%s' % mymode, my_timest='timestep_%.3d' % cur_time)
            nel, nreal = Y.shape
            # Generate extra parameter ensemble, if True:
            # if gen_extra_parameters is True: # todo: this has to be corrected as when restart is False
            if porosity is True:
                if gen_porosity is True:
                    pp = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandPoros.dat', nreal)
                    np.save(os.path.join(mypath_dict['fld_porosity'], 'porosity_%siter000_timestep_000' % mymode), pp)
            if storativity is True: # Load of last fl_ iter storativity ensemble to start transport
                if gen_storativity is True:
                    ss = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandStorage.dat', nreal)
                    np.save(os.path.join(mypath_dict['fld_storativity'], 'storativity_%siter000_timestep_000' % mymode), ss)
                if gen_storativity is False:
                    fl_storage = dm.getdirs(mypath_dict['fld_storativity'], mystr='storativity_fl_', fullpath=True,
                                               onlylast=True)
                    fl_storage_ens = np.load(fl_storage)
                    np.save(os.path.join(mypath_dict['fld_storativity'], 'storativity_%siter000_timestep_000' % mymode), fl_storage_ens)
            if dispersivity is True:
                if gen_dispersivity is True:
                    dd = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandDispers.dat', nreal)
                    np.save(os.path.join(mypath_dict['fld_dispersivity'], 'dispersivity_%siter000_timestep_000' % mymode), dd)

            xtra_param, xtra_param_idx = hgr.read_extraparam('dummy', mypath_dict, mymode, initialize=initialize,
                                                             porosity=porosity, storativity=storativity,
                                                             dispersivity=dispersivity, update_mprops=False)

            modmeas = kf.read_iter_data(mypath_dict['fld_modobs'], myType='ModMeas_%s' % mymode, my_timest='timestep_%.3d' % (cur_time + 1))
            # num_modmeas = modmeas.shape[0]
            restart = False
        print('Working with an ensemble of << %d >> realizations.' % nreal)
        # %% Read the corresponding set of field observations:
        if (type_update == 'restart') or (type_update == 'std'):
            selfieldmeas = fieldmeas[cur_time, :].flatten('F')
        elif type_update == 'full':
            selfieldmeas = fieldmeas.flatten('F')
        elif type_update == 'cum':
            selfieldmeas = fieldmeas[0:cur_time + 1, :].flatten('F')

        # Concatenate kkk and extra parameters, if True. Log transforming the xtra parameters:
        if (porosity or storativity or dispersivity) is True:
            for bb in range(0, len(xtra_param)):
                Y = np.r_[Y, np.log(xtra_param[bb])]

        # %% Initialize parameter-update of the whole ensemble:
        while cur_iter < total_iter:  # and (acceptedReal < nreal):

            print('Inner iteration No. %s started...' % (cur_iter + 1))
            # %% For restart options save a copy of old parameters:
            # Y_old = np.copy(Y)

            # if (porosity or storativity or dispersivity) is True:
            #     xtraParamList = ['fld_porosity', 'fld_storativity', 'fld_dispersivity']
            #     xtraParamFiles = ['porosity_%siter%.3d_timestep_%.3d_temp' % (mymode, cur_iter + 1, cur_time + 1),
            #                       'storage_%siter%.3d_timestep_%.3d_temp' % (mymode, cur_iter + 1, cur_time + 1),
            #                       'dispersivity_%siter%.3d_timestep_%.3d_temp' % (mymode, cur_iter + 1, cur_time + 1)]
            #     for bb in range(0, len(xtraParamList)):
            #         if xtra_param_idx[bb+1] != 0:
            #             np.save(os.path.join(mypath_dict['%s' % xtraParamList[bb]], xtraParamFiles[bb]), np.zeros((xtra_param_idx[bb+1], nreal)))

            # %% Calculate errors:
            # R: Cov matrix of measurement error (diag=uncorrelated)(artificial error), (num_modmeas x num_modmeas)
            # E: Measurement noise from a multigaussian dist with mean=0 and std_dev=R, (num_modmeas x nreal)
            # ----------------------------------------------------------------------
            if relative_meas_stddev is True:
                temp_meas_stddev = np.abs(selfieldmeas * errorPercent)
                if np.any(temp_meas_stddev > max_meas_stddev):
                    temp_meas_stddev[np.where(temp_meas_stddev > max_meas_stddev)] = max_meas_stddev
                if np.any(temp_meas_stddev < min_meas_stddev):
                    temp_meas_stddev[np.where(temp_meas_stddev < min_meas_stddev)] = min_meas_stddev
                meas_stddev = temp_meas_stddev
            num_modmeas = modmeas.shape[0]
            modmeas_pert, R = kf.perturbedMeas(modmeas, meas_stddev)

            # Apply Gaussian anamorphosis to the modeled and field data:
            if GausAnam is True:
                transf_fctns = []
                transf_modmeas = np.zeros((num_modmeas, nreal))
                transf_selfieldmeas = np.zeros((num_modmeas,))
                transf_modmeas_noerror = np.zeros((num_modmeas, nreal))
                for ii in range(0, num_modmeas, 1):
                    transf_modmeas[ii, :], cur_fctn = mysst.GausAnam(modmeas_pert[ii, :], '', myplot=False,
                                                                     oneDstats=False)
                    transf_fctns.append(cur_fctn)
                    transf_modmeas_noerror[ii, :] = cur_fctn(modmeas[ii, :])
                    transf_selfieldmeas[ii] = cur_fctn(selfieldmeas[ii])

            # %% State auto-covariance matrix Qmm (state means modeled outputs, e.g. heads or concentrations):
            # No transformed:   Qmm = E[(m_u - E[m_u]) x (m_u - E[m_u])' ] (num_modmeas x num_modmeas)
            # Yes transformed:  tQmm = E[(tm_u - E[tm_u]) x (tm_u - E[m_u])' ] (num_modmeas x num_modmeas)
            # %% New covariance matrix of measurements(state means modeled outputs, e.g. heads or concentrations):
            # No transformed:   Qmm_new = Qmm_old + R
            # Yes transformed:  Qmm_new = directly from perturbed measurements
            # ------------------------------------------------------------------------------------------------
            if GausAnam is False:
                Qmm_old = mml.autocov(modmeas, diag=False, parallel=paral_matmult, ncpu=ncpus)
                Qmm_new = np.asarray((Qmm_old + R))

            elif GausAnam is True:
                Qmm_new = mml.autocov(transf_modmeas, diag=False, parallel=paral_matmult, ncpu=ncpus)

            # %% Cross covariance matrix Qym (between parameters and states):
            # No transformed:       Qym = E[(yu - E[yu]) x (mu - E[mu])' ] (nel x num_modmeas)
            # Yes transformed:      Qym = E[(yu - E[yu]) x (tm_u - E[tm_u])' ] transf measurements but not perturbed
            # ------------------------------------------------------------------------------------------------
            if GausAnam is False:
                Qym = mml.crosscov(Y, modmeas, diag=False, parallel=paral_matmult, ncpu=ncpus)
            elif GausAnam is True:
                Qym = mml.crosscov(Y, transf_modmeas_noerror, diag=False, parallel=paral_matmult, ncpu=ncpus)

            # %% Compute the Kalman gain:
            # K = Qym inv( Qmm + R ) (nel x nmeas)
            # ------------------------------------------------------------------------------------------------
            Kgain = damp_fc * (Qym.dot(np.linalg.inv(Qmm_new)))
            # Kgain = np.linalg.lstsq(Qym.T, Qmm_new)[0][:][:]

            # %% Update realizations and get model outputs for new time step t + 1:
            # ------------------------------------------------------------------------------------------------
            if GausAnam is True:
                modmeas_pert = transf_modmeas
                selfieldmeas = transf_selfieldmeas
            Y, modmeas_upd, residuals, likelihood = kf.caller_kfupdate(mymaindir, str_assimilation, nnodes, mymode, type_update,
                                                           dY_max, dY_min, modmeas_pert, Y,
                                                           cur_time + 1, all_steptimes, len(ObsWell_name), selfieldmeas,
                                                           R, Kgain, parallel=paralrun, cpus=ncpus,
                                                           parmatmult=paral_matmult, initial_head=ini_head,
                                                           neg2zero=neg2zero,
                                                           head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm,
                                                           moments=moments, OWnode=ObsWell_node,
                                                           porosity=porosity, storativity=storativity,
                                                           dispersivity=dispersivity, extraparm_idx=xtra_param_idx,
                                                           cluster=cluster,
                                                           subgrid=subgrid,
                                                           subgridnodes=subgridnodes,
                                                           subgridelem = subgridelem)
            # %% Calculate conditional covariance matrix of parameter, or error covariance ( nel x nel )
            # Qyy = E[(Y_c - E[Y_c])(Y_c - E[Y_c])']
            # -------------------------------------------------------------------------------------------
            # temp2 = (Y.transpose() - np.average(Y, axis=1, weights=None, returned=False)).transpose()
            # Qyy_c = (1. / (nreal - 1)) * (mm.matmul_locopt(temp2, temp2.T, ncpu=ncpus))

            # %% Store stuff:
            # Separate extra parameters from kkk field:
            if (porosity or storativity or dispersivity) is True:
                # extra_param = Y_i[-xtra_param_idx[0]:]
                Y = Y[0:-(xtra_param_idx[0]), :]

            # Updated log parameter field
            upd_kfile = 'kkk_%siter%.3d_timestep_%.3d' % (mymode, cur_iter + 1, cur_time + 1)
            np.save(os.path.join(mypath_dict['fld_kkkfiles'], upd_kfile), np.exp(Y))
            np.save(os.path.join(mypath_dict['fld_residuals'], 'Residuals_%siter%.3d_timestep_%.3d'
                                 % (mymode, cur_iter + 1, cur_time + 2)), residuals)
            np.save(os.path.join(mypath_dict['fld_likelihood'], 'llh_%siter%.3d_timestep_%.3d'
                                 % (mymode, cur_iter + 1, cur_time + 2)), likelihood)

            if porosity is True:
                hgr.packXtra_params('porosity', mypath_dict['fld_porosity'], nreal, cur_iter=(cur_iter + 1),
                                    cur_time=(cur_time + 1), mymode=mymode)
            if storativity is True:
                hgr.packXtra_params('storativity', mypath_dict['fld_storativity'], nreal, cur_iter=(cur_iter + 1),
                                    cur_time=(cur_time + 1), mymode=mymode)

            if dispersivity is True:
                hgr.packXtra_params('dispersivity', mypath_dict['fld_dispersivity'], nreal, cur_iter=(cur_iter + 1),
                                    cur_time=(cur_time + 1), mymode=mymode)

            if (cur_time + 1) < No_timeobs:
                # Updated modeled states (i.e. modeled measurements)
                np.save(os.path.join(mypath_dict['fld_modobs'], 'ModMeas_%siter%.3d_timestep_%.3d'
                                     % (mymode, cur_iter + 1, cur_time + 2)), modmeas_upd)

                # Cross covariance matrix Qsy
                # np.save(os.path.join(mypath_dict['fld_kkkfiles'], 'Qym_%siter%.3d_timestep%.3d' % (mymode, cur_iter + 1, cur_time + 1)), Qym)
                # Error covariance of parameters
                # np.save(os.path.join(store_path, 'Qyy_c_%siter%.3d_timestep%.3d' % (mymode, cur_iter+1, cur_time+1)), Qyy_c)

            cur_iter += 1
            print(u'Inner iteration No. {0:d} completed.'.format(cur_iter))
            print('It took %f seconds to go through full inner iteration' % (time.clock() - start))
            modmeas = modmeas_upd

        print('Time step No. %s (%s seconds)out of %s, finalized...' % (cur_time+1, all_steptimes[cur_time], No_timeobs - 1))
        cur_time += 1

        if type_update == 'full':     # To set the number of time-steps needed == 1 and go out of the loop
            cur_time = No_timeobs

        if cur_time >= No_timeobs:
            print(' Work done. Total processing time: %s \nPreparing to exit...' % (time.clock() - start))
            sys.exit('Terminated')
