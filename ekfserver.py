""" Builds up a server to distribute the generation of realizations via TCP/IP
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon
"""
import socket
import ssl
import os
import sys
import numpy as np
import datetime
import shutil
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
from myfunctions.hgs import GenReal
import myfunctions.dirmanip as dm
import myfunctions.mysocket.mysocket as sock
import myfunctions.mysocket.server_client as sercl
#todo: reformat the file so it can be called as a function
# def iniens(modelname, **mysettings):
if __name__ == "__main__":

    # 0. Directory data:
    # ---------------#
    modelname = '2d_withnoise_2'
    remarks = ''        # Provide remarks to identify various aspects of the assimilation
    mymode = 'fl_'
    mainPath = os.path.join(dm.homedir(), '_dataAssim', modelname)
    str_assimilation = '%s%s' % (modelname, remarks)

    # 1. Ensemble data:
    # ---------------#
    mode_flag = 'genreal'  # recvdata: Receive data mode   genreal: distribute work mode
    nFields = 10
    NoComputers = 1  # Controller to wait for the last realization, before start receiving data

    # 2. Server data:
    # ---------------#
    port = 4001
    myip = sock.getIP()
    print('Host name (IP): %s ' % myip)

    # 3. Generate directories:
    # Note: this one does not generate nothing, it only gets the dir dictionary
    # ----------------#
    mypath_dict = dm.gendirtree(mainPath, str_assim=str_assimilation, proc_idx=-9999, mode='')

    Source_files2transfer = [mypath_dict['fld_reckkk'],
                             mypath_dict['fld_recmodobs'],
                             mypath_dict['fld_recheads']]

    Target_files2transfer = [mypath_dict['fld_kkkfiles'],
                             mypath_dict['fld_modobs'],
                             mypath_dict['fld_heads']]

    Target_filenames = ['kkk_iter000_timestep_000',
                        'ModMeas%siter001_timestep_001' % mymode,
                        'hydheads%siter0001_timestep001' % mymode]

    if mode_flag == 'genreal':

        #   %% Initialize indices:
        idall = np.arange(1, nFields + 1, 1)
        iddone = np.array([])
        #   #basket = 0

        #   %% Create TCP/IP SERVER socket and initialize server:
        server = sock.server(myip, port)

        #   %% Waiting for connections and distribute work:
        while len(idall) >= len(iddone):  # while basket <= nFields + (NoComputers-1):

            # Wait for a connection:
            print('Waiting for a connection. Date: %s...' % (datetime.datetime.now()))

            # Accept connection from a client:
            connection, client = server.accept()
            # wrap the socket for a secured connection using SSL
            try:
                connstream = ssl.wrap_socket(connection, server_side=True, certfile="cert", keyfile="key")
                print('Successful SSL connection! ')
            except IOError:
                print('SSL connection was not possible')

            try:
                num_cpu = connstream.recv(1024).decode('utf16')
                print(client, 'connected with %s CPUs. Date: %s' % (num_cpu, datetime.datetime.now()))

                #   %% Create proper indices to distribute work:
                id2send = sercl.mk_id2send(idall, iddone, num_cpu)
                print('Realization indices to send: ', id2send)

                #   %% Send the indices if there are still left to send:
                if len(id2send) > 0:
                    flag_work = 'stillworking'

                    # Prepare INDICES with a proper string format:
                    message = id2send.astype(str)
                    ids_message = sercl.fmt_id2send(id2send, message)

                    # Send indices to the client:
                    connstream.send(ids_message.encode('utf16'))
                    connstream.send(flag_work.encode('utf16'))

                    # Receive indices back from the client and add them to iddone:
                    idrecv = connstream.recv(1024).decode('utf16')
                    iddone = sercl.fmt_idrecv(idrecv, iddone)
                    #   #basket = len(iddone)

                elif len(id2send) == 0:
                    print('All realizations ids sent')
                    flag_work = 'done'
                    message = ['work,', 'completed']
                    message = ''.join(message)
                    iddone = np.r_[iddone, 1]

                    # Send finalizing message so clients can get disconnected:
                    connstream.sendall(message.encode('utf16'))
                    connstream.sendall(flag_work.encode('utf16'))
                    #   #basket = basket + 1

            except (
                    KeyboardInterrupt, socket.error, IOError, OSError, EOFError, IndexError, ValueError,
                    WindowsError) as err:
                repr(err)
                raise

        try:
            connstream.shutdown(socket.SHUT_RDWR)
            connstream.close()
            server.close()
            print('Shutting down server')
            #   server_sock.shutdown(socket.SHUT_RDWR)
            mode_flag = 'recvdata'
        except:
            print('Unexpected error, contact your clients!!!')
            raise
            # """------------------------------------------------------------------------------#
            #       Reinitialize server to receive all generated data:
            # ------------------------------------------------------------------------------"""
    if mode_flag == 'recvdata':
        #   %% Create TCP/IP SERVER socket:
        server = sock.server(myip, port + 1)
        buffersize = 4096
        basket = 0

        inputs = [server]
        outputs = []

        while basket < nFields:
            # Wait for a connection:
            print('Waiting to receive data... (Time: %s)...' % (datetime.datetime.now()))
            connection, client = server.accept()

            # wrap the socket for a secured connection using SSL
            try:
                connstream = ssl.wrap_socket(connection, server_side=True, certfile="cert", keyfile="key")
                print('Successful SSL connection! ')
            except IOError:
                print('SSL connection was not possible')

            # Start receiving data
            try:
                print('Receiving data from:', client, ', Date: %s' % (datetime.datetime.now()))
                print('Writing files...')

                while True:
                    myFilename = connstream.recv(1024).decode('utf16')  # Receive filename
                    if myFilename == 'Allfilessent':
                        break
                    connstream.sendall('File name received'.encode('utf16'))

                    # Select the folder and create file:
                    mypath = sercl.storedir(myFilename, mypath_dict)
                    myfile = open(os.path.join(mypath, myFilename), 'wb')
                    mystr = ''
                    while True:
                        data = connstream.recv(buffersize)  # Receive the file content
                        try:
                            mystr = data.decode('utf16')
                        except:
                            pass
                        if mystr:
                            if mystr.endswith(',filesent'):
                                break
                        myfile.write(data)

                    myfile.flush()
                    myfile.close()

                print('Finished files from client:,', client, ' Date: %s' % (datetime.datetime.now()))
                print('Client disconnected')

                Files = np.r_[os.listdir(mypath_dict['fld_reckkk']),
                              os.listdir(mypath_dict['fld_recmodobs']),
                              os.listdir(mypath_dict['fld_recheads'])]

                basket = len(Files) / 2

            except (KeyboardInterrupt, socket.error, IOError, OSError, EOFError, IndexError, ValueError, WindowsError):
                connstream.shutdown(socket.SHUT_RDWR)
                connstream.close()
                sys.exit("You pressed Ctrl+C")

            finally:
                # Always clean up the connection
                print('Closing connection...\n')
                connstream.shutdown(socket.SHUT_RDWR)
                connstream.close()

        connstream.close()
        server.close()
        print('Data from all realizations received (a total of %d files)...' % len(Files))

        print('Moving received files to a single binary file in the corresponding folders...')

        # Relocate the received files and store them in a single binary file:
        for jj in range(0, len(Target_files2transfer)):

            inFileList, dummy = dm.getdirs(Source_files2transfer[jj], mystr='ini', fullpath=False, onlylast=False)
            tempData = np.load(os.path.join(Source_files2transfer[jj], inFileList[0]))
            if (mytype == 'full') and ('mod' in inFileList[0]):
                tempData = tempData.transpose().flatten(order='F')
            DataArray = np.empty((len(tempData), nFields))

            for ii, cur_file in enumerate(inFileList):

                if ii == 0:
                    if tempData.ndim > 1:
                        if not isinstance(tempData, (int, float, str)):
                            tempData = np.squeeze(tempData, axis=1)
                    DataArray[:, ii] = tempData
                else:
                    addData = np.load(os.path.join(Source_files2transfer[jj], cur_file))
                    if (mytype == 'full') and ('mod' in cur_file):
                        addData = addData.transpose().flatten(order='F')
                    if addData.ndim > 1:
                        if not isinstance(addData, (int, float, str)):
                            addData = np.squeeze(addData, axis=1)
                    DataArray[:, ii] = addData

            np.save(os.path.join(Target_files2transfer[jj], Target_filenames[jj]), DataArray)

        #     shutil.copyfile(os.path.join(mypath_dict['fld_recheads'], ii), os.path.join(mypath_dict['fld_heads'], ii))
        # for ii in os.listdir(mypath_dict['fld_recmodobs']):
        #     shutil.copyfile(os.path.join(mypath_dict['fld_recmodobs'], ii), os.path.join(mypath_dict['fld_modobs'], ii))
        # for ii in os.listdir(mypath_dict['fld_reckkk']):
        #     shutil.copyfile(os.path.join(mypath_dict['fld_reckkk'], ii), os.path.join(mypath_dict['fld_kkkfiles'], ii))
        # Remove receiving folder?:
        # del_recfolder = str(input('Delete datarecieved folder? y/n: '))
        del_recfolder = 'y'
        if del_recfolder == 'y':
            shutil.rmtree(mypath_dict['fld_recdata'])
            print('< datarecieved > folder removed')
        else:
            print('< datarecieved > folder NOT removed')

            # Notes:
            #    Steps to create a self-signed with openssl in the command line:
            #       from: https://devcenter.heroku.com/articles/ssl-certificate-self
            #   $ which openssl      if path is not returned openssl need to be installed
            #   $ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
            #
            #   For a private key and a signing request:
            #   $ openssl genrsa -des3 -out server.orig.key 2048
            #   $ openssl rsa -in server.orig.key -out server.key
            #   $ openssl req -new -key server.key -out server.csr
            #   $ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
