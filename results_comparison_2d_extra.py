# -*- coding: utf-8 -*-
"""
Plot some results from the EnKF assimilation
Created on Thursday 27 October 11:50:00 2015
@author: eesl
"""
import matplotlib as mpl
# mpl.use('Agg')
import numpy as np
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.plotResults as myplt
import addfunc.fieldgen as fg
import addfunc.dirs as dm
import addfunc.gridmanip as gm
import addfunc.hgs as hg
import addfunc.mystats as mysst
import addfunc.matmult.matmul_opt as mm
import addfunc.matmult.mylikelihood as mml
import matplotlib.pylab as plt
plt.rcParams.update({'figure.max_open_warning': 0})


def main():
    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #
    modelname = '2d_extra'
    remarks = 'kkk_initial_ensemble'
    sourcePath = r'\\134.2.49.164\emilio\PhD_work\enkf_storetemp\RefinedGrid\2d\2d_extra'

    # Figure directories:
    modelData_file = 'ModelOutput_full_prediction.npy'
    reference_data = np.loadtxt(os.path.join(sourcePath, 'model_data', 'Fieldtr_.dat'))
    reference_time = np.loadtxt(os.path.join(sourcePath, 'model_data', '_outputtimestr_.dat'))

    ini_ensemble_data = np.load(os.path.join(sourcePath, 'realizations_2d_extrakkk_initial_ensemble', modelData_file))
    after_flow_data = np.load(os.path.join(sourcePath, 'realizations_2d_extrakkk_afterTwoFlowAssim', modelData_file))
    after_transport_g_data = np.load(os.path.join(sourcePath, 'realizations_2d_extrakkk_2d_test2g', modelData_file))
    after_transport_i_data = np.load(os.path.join(sourcePath, 'realizations_2d_extrakkk_2d_test2i_001df_noga_from_fl2e', modelData_file))

    # w08, index = 13
    ci = [10, 90]
    sel_wells = [1, 4, 6, 13, 18 ]
    # cur_idx = 16
    for cur_idx in sel_wells:
        cur_well_initial = ini_ensemble_data[cur_idx, :, :]
        cur_well_after_flow = after_flow_data[cur_idx, :, :]
        cur_well_aftertransportg = after_transport_g_data[cur_idx, :, :]
        cur_well_aftertransporti = after_transport_i_data[cur_idx, :, :]

        # for ii in range(0, 500):
        #     plt.plot(reference_time, cur_well_initial[ii, :], '--', c='grey', label='Initial ensemble')
        plt.figure(cur_idx)
        plt.plot(reference_time, reference_data[:, cur_idx], 'k-o')
        plt.plot(reference_time, np.median(cur_well_initial, axis=0), 'b--', label='Initial')
        # plt.plot(reference_time, cur_well_initial.mean(axis=0), 'b-')
        plt.plot(reference_time, np.median(cur_well_after_flow, axis=0), '--', c='grey', label='After flow')
        # plt.plot(reference_time, cur_well_after_flow.mean(axis=0), '-', c='grey')
        plt.plot(reference_time, np.median(cur_well_aftertransportg, axis=0), 'r--', label='transport g')
        # plt.plot(reference_time, cur_well_aftertransportg.mean(axis=0), 'r-')
        plt.plot(reference_time, np.median(cur_well_aftertransporti, axis=0), 'g--', label='transport i')
        # plt.plot(reference_time, cur_well_aftertransporti.mean(axis=0), 'g-')
        plt.legend()
        plt.grid()
    plt.show()
    print('Pause')

    rmse_ini, nash_ini, rel_error_ini, aae_ini, aesd_ini = [], [], [], [], []
    rmse_afterflow, nash_afterflow, rel_error_afterflow, aae_afterflow, aesd_afterflow = [], [], [], [], []
    rmse_tr_g, nash_tr_g, rel_error_tr_g, aae_tr_g, aesd_tr_g = [], [], [], [], []
    rmse_tr_i, nash_tr_i, rel_error_tr_i, aae_tr_i, aesd_tr_i = [], [], [], [], []
    for ii in range(0, len(reference_time)):
        cur_mean_initial = np.median(ini_ensemble_data[:, :, ii], axis=1)
        rmse_ini.append(np.sqrt(mysst.mean_squared_error(cur_mean_initial, reference_data[ii, :])))
        nash_ini.append(mysst.nash_sutcliffe(cur_mean_initial, reference_data[ii, :]))
        rel_error_ini.append(np.abs(np.mean(mysst.relative_error(cur_mean_initial, reference_data[ii, :]))))
        aae_ini.append(mysst.mean_abs_error(cur_mean_initial, reference_data[ii, :]))
        aesd_ini.append(mysst.mean_ensemble_std(ini_ensemble_data[:, :, ii], cur_mean_initial))

        cur_mean_afterflow = np.median(after_flow_data[:, :, ii], axis=1)
        rmse_afterflow.append(np.sqrt(mysst.mean_squared_error(cur_mean_afterflow, reference_data[ii, :])))
        nash_afterflow.append(mysst.nash_sutcliffe(cur_mean_afterflow, reference_data[ii, :]))
        rel_error_afterflow.append(np.abs(np.mean(mysst.relative_error(cur_mean_afterflow, reference_data[ii, :]))))
        aae_afterflow.append(mysst.mean_abs_error(cur_mean_afterflow, reference_data[ii, :]))
        aesd_afterflow.append(mysst.mean_ensemble_std(after_flow_data[:, :, ii], cur_mean_afterflow))

        cur_mean_trg = np.median(after_transport_g_data[:, :, ii], axis=1)
        rmse_tr_g.append(np.sqrt(mysst.mean_squared_error(cur_mean_trg, reference_data[ii, :])))
        nash_tr_g.append(mysst.nash_sutcliffe(cur_mean_trg, reference_data[ii, :]))
        rel_error_tr_g.append(np.abs(np.mean(mysst.relative_error(cur_mean_trg, reference_data[ii, :]))))
        aae_tr_g.append(mysst.mean_abs_error(cur_mean_trg, reference_data[ii, :]))
        aesd_tr_g.append(mysst.mean_ensemble_std(after_transport_g_data[:, :, ii], cur_mean_trg))

        cur_mean_tri = np.median(after_transport_i_data[:, :, ii], axis=1)
        rmse_tr_i.append(np.sqrt(mysst.mean_squared_error(cur_mean_tri, reference_data[ii, :])))
        nash_tr_i.append(mysst.nash_sutcliffe(cur_mean_tri, reference_data[ii, :]))
        rel_error_tr_i.append(np.abs(np.mean(mysst.relative_error(cur_mean_tri, reference_data[ii, :]))))
        aae_tr_i.append(mysst.mean_abs_error(cur_mean_tri, reference_data[ii, :]))
        aesd_tr_i.append(mysst.mean_ensemble_std(after_transport_i_data[:, :, ii], cur_mean_tri))

    np.savetxt(os.path.join(sourcePath, 'rmse_ns_initial_ensemble.txt'),
               np.array((rmse_ini, nash_ini, rel_error_ini, aae_ini, aesd_ini)).T,
               header='rmse, nash, rel_error, aae, aesd')
    np.savetxt(os.path.join(sourcePath, 'rmse_ns_after_flow.txt'),
               np.array((rmse_afterflow, nash_afterflow, rel_error_afterflow, aae_afterflow, aesd_afterflow)).T,
               header='rmse, nash, rel_error, aae, aesd')
    np.savetxt(os.path.join(sourcePath, 'rmse_ns_after_trg.txt'),
               np.array((rmse_tr_g, nash_tr_g, rel_error_tr_g, aae_tr_g, aesd_tr_g)).T,
               header='rmse, nash, rel_error, aae, aesd')
    np.savetxt(os.path.join(sourcePath, 'rmse_ns_after_tri.txt'),
               np.array((rmse_tr_i, nash_tr_i, rel_error_tr_i, aae_tr_i , aesd_tr_i)).T,
               header='rmse, nash, rel_error, aae, aesd')

    pearson_coef_ini = mysst.Pearson(reference_data.flatten(), np.median(ini_ensemble_data, axis=1).T.flatten())
    print('Pearson Ini:%s, %s' % pearson_coef_ini)
    pearson_coef_after_flow = mysst.Pearson(reference_data.flatten(), np.median(after_flow_data, axis=1).T.flatten())
    print('Pearson After Flow:%s, %s' % pearson_coef_after_flow)
    pearson_coef_tr_g = mysst.Pearson(reference_data.flatten(), np.median(after_transport_g_data, axis=1).T.flatten())
    print('Pearson After Tr g:%s, %s' % pearson_coef_tr_g)
    pearson_coef_tr_i = mysst.Pearson(reference_data.flatten(), np.median(after_transport_i_data, axis=1).T.flatten())
    print('Pearson After Tr i:%s, %s' % pearson_coef_tr_i)

if __name__ == '__main__':
    main()

    # # Load model grid geometry and statistical parameters
    # print('Loading model grid data and statistical parameters...')
    # xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(statparam)
    # nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
    # node_coord = np.genfromtxt(grid_filedir, skip_header=3, max_rows=nnodes, usecols=(0, 1, 2))
    #
    # # Separate inner-small from outer-big grid if working with flow
    # biggrid_elem_ids = np.arange(1, elements + 1, 1)
    # smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)
