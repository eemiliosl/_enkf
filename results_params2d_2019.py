# -*- coding: utf-8 -*-
"""
Plot some results from the EnKF assimilation
Created on Thursday 27 October 11:50:00 2015
@author: eesl
"""
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pylab as plt
import numpy as np
import itertools
import sys
import os
import scipy.io as spio
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.dirs as dm
import addfunc.gridmanip as gm
import addfunc.hgs as hg
import addfunc.mystats as mysst


def main():
    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #

    datadir = r'D:\Users\Emilio\Dropbox\PhD\papers\enkf_synthetic\_2019\additional_data'
    # Model grid settings
    dx = 0.1
    dy = dx
    xlim = [34, 67]  # [30, 70]     # xlim = [0, 100]
    ylim = [52, 68]  # [15, 35]      # ylim = [0, 50]
    zlim = [0, 1]  # zlim = [0, 1]
    dxtick = 33/5
    dytick = 16/5
    # Output settings:

    myxticks = [34, xlim[0]+dxtick, xlim[0]+(2*dxtick), xlim[0]+(3*dxtick), xlim[0]+(3*dxtick), xlim[0]+(4*dxtick), xlim[0]+(5*dxtick)]
    myyticks = [52, ylim[0]+dytick, ylim[0]+(2*dytick), ylim[0]+(3*dytick), ylim[0]+(3*dytick), ylim[0]+(4*dytick), ylim[0]+(5*dytick)]

    # Additional parameters todo: integrate additional plotting features
    varMax = 1.0
    logkMax = -2.4
    logKMin = -8
    storativity = False  # Update storativity (homogeneous)
    ssTruth = 3.0e-3

    """ """
    elY, elX = len(np.arange(ylim[0], ylim[1], dy)), len(np.arange(xlim[0], xlim[1], dx))

    # 1. Load wells and adjust its coordinates for plotting:
    # ---------------------------------------------------
    well_coord = np.loadtxt(os.path.join(datadir, '_obswellscoordfl_.dat'), usecols=(1, 2, 3))
    # wells_ids = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(0,), dtype='S')
    well_coord *= 1 / dx
    well_points = np.array((well_coord[:, 0] - xlim[0] / dx, well_coord[:, 1] - ylim[0] / dy)).T

    # 2. Load K values:
    orig_field = spio.loadmat(os.path.join(datadir, 'ref_lnk.mat'), squeeze_me=True)
    orig_field=orig_field['lnK'][10:-11,0:-1]
    mean_kkk_orig = np.nanmean(orig_field)

    orig_kkk_ensemble = np.log(np.load(os.path.join(datadir, 'lnk_ensemble_initial.npy')))
    orig_kkk_ensemble_mean = np.nanmean(orig_kkk_ensemble, axis=-1).reshape((160, -1))

    enkf_kkk = np.log(np.load(os.path.join(datadir, 'enKF_kkk_fl2_iter001_timestep_028_id_c.npy')))
    enkf_kkk_mean = np.nanmean(enkf_kkk, axis=-1).reshape((160,-1))
    enkf_kkk_tr = np.log(np.load(os.path.join(datadir, 'enKF_kkk_tr2_iter001_timestep_040_g.npy')))
    enkf_kkk_tr_mean = np.nanmean(enkf_kkk_tr, axis=-1).reshape((160, -1))
    enkf_std_kkk_tr = np.loadtxt(os.path.join(datadir,'Qyy_diag_k_tr2_iter001_timestep_040_g.txt'))

    keg_kkk_all = spio.loadmat(os.path.join(datadir, 'keg_kkk_flow2_nodamp.mat'), squeeze_me=True)
    keg_kkk = keg_kkk_all['lnKpost']
    keg_std_kkk = keg_kkk_all['stdlnKpost']
    keg_kkk_mean = np.nanmean(keg_kkk, axis=-1).reshape((-1, 181))[0:-1, 10:-11].T

    keg_kkk_tr_all = spio.loadmat(os.path.join(datadir, 'keg_kkk_transport2_nodamp.mat'), squeeze_me=True)
    keg_kkk_tr = keg_kkk_tr_all['lnKpost_c']
    keg_std_kkk_tr = keg_kkk_tr_all['stdlnKpost_c']
    keg_kkk_tr_mean = np.nanmean(keg_kkk_tr, axis=-1).reshape((-1, 181))[0:-1, 10:-11].T

    # --------------------------------

    nash, pears, rel_error, abs_error, param_bias, total_var, AAE, AAESD = [], [], [], [], [], [], [], []
    # 3. Load output times and wells:
    # ---------------------------------------------------------------- #
    #outputtimes = np.loadtxt(os.path.join(datadir, '_outputtimesfl_.dat'))

    fig = plt.figure(1)
    ax1 = fig.add_subplot(111)
    ax1.set_title('Original synthetic field')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')
    ax1.set_xlabel('X coordinate', fontsize='x-small')
    ax1.set_ylabel('Y coordinate', fontsize='x-small')
    ax1.set_xticklabels(myxticks)
    ax1.set_yticklabels(myyticks)
    im1 = ax1.imshow(np.flipud(orig_field.reshape(elY, elX)), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax1.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm = mpl.colors.Normalize(vmin=logKMin, vmax=logkMax)
    im1.set_norm(norm)
    cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
    cb1.set_label('$ln(K)$', fontsize='x-small')

    fig2 = plt.figure(2)
    ax2 = fig2.add_subplot(111)
    ax2.set_title('EnKF mean ensemble flow')
    ax2.tick_params(axis='both', which='major', labelsize='x-small')
    ax2.set_xlabel('X coordinate', fontsize='x-small')
    ax2.set_ylabel('Y coordinate', fontsize='x-small')
    ax2.set_xticklabels(myxticks)
    ax2.set_yticklabels(myyticks)
    im2 = ax2.imshow(np.flipud(enkf_kkk_mean), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax2.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm2 = mpl.colors.Normalize(vmin=logKMin, vmax=logkMax)
    im2.set_norm(norm2)
    cb2 = fig2.colorbar(im2, orientation='horizontal', fraction=0.08, pad=0.1)
    cb2.set_label('$ln(K)$', fontsize='x-small')

    fig3 = plt.figure(3)
    ax3 = fig3.add_subplot(111)
    ax3.set_title('KEG mean ensemble flow')
    ax3.tick_params(axis='both', which='major', labelsize='x-small')
    ax3.set_xlabel('X coordinate', fontsize='x-small')
    ax3.set_ylabel('Y coordinate', fontsize='x-small')
    ax3.set_xticklabels(myxticks)
    ax3.set_yticklabels(myyticks)
    im3 = ax3.imshow(np.flipud(keg_kkk_mean), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax3.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm3 = mpl.colors.Normalize(vmin=logKMin, vmax=logkMax)
    im3.set_norm(norm3)
    cb3 = fig3.colorbar(im3, orientation='horizontal', fraction=0.08, pad=0.1)
    cb3.set_label('$ln(K)$', fontsize='x-small')

    fig4 = plt.figure(4)
    ax4 = fig4.add_subplot(111)
    ax4.set_title('Initial mean ensemble flow')
    ax4.tick_params(axis='both', which='major', labelsize='x-small')
    ax4.set_xlabel('X coordinate', fontsize='x-small')
    ax4.set_ylabel('Y coordinate', fontsize='x-small')
    ax4.set_xticklabels(myxticks)
    ax4.set_yticklabels(myyticks)
    im4 = ax4.imshow(np.flipud(orig_kkk_ensemble_mean), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax4.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm4 = mpl.colors.Normalize(vmin=logKMin, vmax=logkMax)
    im4.set_norm(norm4)
    cb4 = fig4.colorbar(im4, orientation='horizontal', fraction=0.08, pad=0.1)
    cb4.set_label('$ln(K)$', fontsize='x-small')

    # Plots for transport
    fig2 = plt.figure(5)
    ax2 = fig2.add_subplot(111)
    ax2.set_title('EnKF mean ensemble transport')
    ax2.tick_params(axis='both', which='major', labelsize='x-small')
    ax2.set_xlabel('X coordinate', fontsize='x-small')
    ax2.set_ylabel('Y coordinate', fontsize='x-small')
    ax2.set_xticklabels(myxticks)
    ax2.set_yticklabels(myyticks)
    im2 = ax2.imshow(np.flipud(enkf_kkk_tr_mean), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax2.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm2 = mpl.colors.Normalize(vmin=logKMin, vmax=logkMax)
    im2.set_norm(norm2)
    cb2 = fig2.colorbar(im2, orientation='horizontal', fraction=0.08, pad=0.1)
    cb2.set_label('$ln(K)$', fontsize='x-small')

    fig3 = plt.figure(6)
    ax3 = fig3.add_subplot(111)
    ax3.set_title('KEG mean ensemble transport')
    ax3.tick_params(axis='both', which='major', labelsize='x-small')
    ax3.set_xlabel('X coordinate', fontsize='x-small')
    ax3.set_ylabel('Y coordinate', fontsize='x-small')
    ax3.set_xticklabels(myxticks)
    ax3.set_yticklabels(myyticks)
    im3 = ax3.imshow(np.flipud(keg_kkk_tr_mean), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax3.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm3 = mpl.colors.Normalize(vmin=logKMin, vmax=logkMax)
    im3.set_norm(norm3)
    cb3 = fig3.colorbar(im3, orientation='horizontal', fraction=0.08, pad=0.1)
    cb3.set_label('$ln(K)$', fontsize='x-small')

    # Calculate error metrics: First the initial ensemble, then EnKF, then KEG, then transport EnKF, transport KEG
    AAE.append(mysst.mean_abs_error(orig_kkk_ensemble_mean.flatten(), orig_field.flatten()))
    AAESD.append(mysst.mean_ensemble_std(orig_kkk_ensemble, orig_kkk_ensemble_mean.flatten()))

    AAE.append(mysst.mean_abs_error(enkf_kkk_mean.flatten(), orig_field.flatten()))
    AAESD.append(mysst.mean_ensemble_std(enkf_kkk, enkf_kkk_mean.flatten()))

    AAE.append(mysst.mean_abs_error(keg_kkk_mean.flatten(), orig_field.flatten()))
    AAESD.append(mysst.mean_ensemble_std(keg_kkk, np.nanmean(keg_kkk, axis=1)))

    AAE.append(mysst.mean_abs_error(enkf_kkk_tr_mean.flatten(), orig_field.flatten()))
    AAESD.append(mysst.mean_ensemble_std(enkf_kkk_tr, enkf_kkk_tr_mean.flatten()))

    AAE.append(mysst.mean_abs_error(keg_kkk_tr_mean.flatten(), orig_field.flatten()))
    AAESD.append(mysst.mean_ensemble_std(keg_kkk_tr, np.nanmean(keg_kkk_tr, axis=1)))


    # Work with covariances:
    orig_Qyy = np.empty((orig_kkk_ensemble.shape[0],))
    for ii in range(0, orig_Qyy.shape[0]):
        cur_chunk = orig_kkk_ensemble[ii, :]
        cur_calc = (cur_chunk - cur_chunk.mean())
        orig_Qyy[ii] = (1. / (orig_kkk_ensemble.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)

    enkf_variance = np.empty((enkf_kkk.shape[0],))
    for ii in range(0, enkf_variance.shape[0]):
        cur_chunk = enkf_kkk[ii, :]
        cur_calc = (cur_chunk - cur_chunk.mean())
        enkf_variance[ii] = (1. / (enkf_kkk.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)

    keg_variance = np.empty((keg_kkk.shape[0],))
    for ii in range(0, keg_variance.shape[0]):
        cur_chunk = keg_kkk[ii, :]
        cur_calc = (cur_chunk - cur_chunk.mean())
        keg_variance[ii] = (1. / (keg_kkk.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)
    keg_variance = keg_variance.reshape((-1, 181))[0:-1, 10:-11].T
    keg_variance = keg_variance.flatten()

    enkf_variance_tr = np.empty((enkf_kkk_tr.shape[0],))
    for ii in range(0, enkf_variance_tr.shape[0]):
        cur_chunk = enkf_kkk_tr[ii, :]
        cur_calc = (cur_chunk - cur_chunk.mean())
        enkf_variance_tr[ii] = (1. / (enkf_kkk_tr.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)

    keg_variance_tr = np.empty((keg_kkk_tr.shape[0],))
    for ii in range(0, keg_variance_tr.shape[0]):
        cur_chunk = keg_kkk_tr[ii, :]
        cur_calc = (cur_chunk - cur_chunk.mean())
        keg_variance_tr[ii] = (1. / (keg_kkk_tr.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)
    keg_variance_tr = keg_variance_tr.reshape((-1, 181))[0:-1, 10:-11].T
    keg_variance_tr = keg_variance_tr.flatten()

    fi5 = plt.figure(7)
    ax5 = fi5.add_subplot(111)
    ax5.set_title('Variance Initial Ensemble')
    ax5.tick_params(axis='both', which='major', labelsize='x-small')
    ax5.set_xlabel('X coordinate', fontsize='x-small')
    ax5.set_ylabel('Y coordinate', fontsize='x-small')
    ax5.set_xticklabels(myxticks)
    ax5.set_yticklabels(myyticks)
    im5 = ax5.imshow(np.flipud(orig_Qyy.reshape(160,-1)), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax5.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm5 = mpl.colors.Normalize(vmin=0.01, vmax=varMax)
    im5.set_norm(norm5)
    cb5 = fi5.colorbar(im5, orientation='horizontal', fraction=0.08, pad=0.1)
    cb5.set_label('Variance of ln(K)$', fontsize='x-small')

    fi6 = plt.figure(8)
    ax6 = fi6.add_subplot(111)
    ax6.set_title('Variance of ln(K) EnKF flow')
    ax6.tick_params(axis='both', which='major', labelsize='x-small')
    ax6.set_xlabel('X coordinate', fontsize='x-small')
    ax6.set_ylabel('Y coordinate', fontsize='x-small')
    ax6.set_xticklabels(myxticks)
    ax6.set_yticklabels(myyticks)
    im6 = ax6.imshow(np.flipud(enkf_variance.reshape(160, -1)), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax6.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm6 = mpl.colors.Normalize(vmin=0.01, vmax=varMax)
    im6.set_norm(norm6)
    cb6 = fi6.colorbar(im6, orientation='horizontal', fraction=0.08, pad=0.1)
    cb6.set_label('Variance of ln(K) EnKF', fontsize='x-small')

    fi7 = plt.figure(9)
    ax7 = fi7.add_subplot(111)
    ax7.set_title('Variance of ln(K) KEG flow')
    ax7.tick_params(axis='both', which='major', labelsize='x-small')
    ax7.set_xlabel('X coordinate', fontsize='x-small')
    ax7.set_ylabel('Y coordinate', fontsize='x-small')
    ax7.set_xticklabels(myxticks)
    ax7.set_yticklabels(myyticks)
    im7 = ax7.imshow(np.flipud(keg_variance.reshape(160, -1)), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax7.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm7 = mpl.colors.Normalize(vmin=0.01, vmax=varMax)
    im7.set_norm(norm7)
    cb7 = fi7.colorbar(im7, orientation='horizontal', fraction=0.08, pad=0.1)
    cb7.set_label('Variance of ln(K) KEG', fontsize='x-small')

    #######################
    fi6 = plt.figure(10)
    ax6 = fi6.add_subplot(111)
    ax6.set_title('Variance of ln(K) EnKF transport')
    ax6.tick_params(axis='both', which='major', labelsize='x-small')
    ax6.set_xlabel('X coordinate', fontsize='x-small')
    ax6.set_ylabel('Y coordinate', fontsize='x-small')
    ax6.set_xticklabels(myxticks)
    ax6.set_yticklabels(myyticks)
    im6 = ax6.imshow(np.flipud(enkf_variance_tr.reshape(160, -1)), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax6.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm6 = mpl.colors.Normalize(vmin=0.01, vmax=varMax)
    im6.set_norm(norm6)
    cb6 = fi6.colorbar(im6, orientation='horizontal', fraction=0.08, pad=0.1)
    cb6.set_label('Variance of ln(K) EnKF', fontsize='x-small')

    fi7 = plt.figure(11)
    ax7 = fi7.add_subplot(111)
    ax7.set_title('Variance of ln(K) KEG transport')
    ax7.tick_params(axis='both', which='major', labelsize='x-small')
    ax7.set_xlabel('X coordinate', fontsize='x-small')
    ax7.set_ylabel('Y coordinate', fontsize='x-small')
    ax7.set_xticklabels(myxticks)
    ax7.set_yticklabels(myyticks)
    im7 = ax7.imshow(np.flipud(keg_variance_tr.reshape(160, -1)), interpolation='nearest', cmap=plt.get_cmap('jet'))
    ax7.plot(well_points[:, 0], well_points[:, 1], 'ko', ms=2)
    norm7 = mpl.colors.Normalize(vmin=0.01, vmax=varMax)
    im7.set_norm(norm7)
    cb7 = fi7.colorbar(im7, orientation='horizontal', fraction=0.08, pad=0.1)
    cb7.set_label('Variance of ln(K) KEG', fontsize='x-small')

    #
    #
    #
    # if storativity is True:
    #     ssPath = os.path.join(realizationdir, 'storativity')
    #     ssList = os.listdir(ssPath)
    #
    #     ssMean = []
    #     ssDif = []
    #
    #     for idx, curss in enumerate(ssList):
    #         cur_ss = np.load(os.path.join(ssPath, curss))
    #         ssMean.append(cur_ss.mean())
    #         fig = plt.figure(1)
    #         plt.hist(cur_ss.T, histtype='step', color='gray', alpha=0.5, bins=16)
    #         fig2 = plt.figure(2)
    #         plt.hist(np.log(cur_ss).T, histtype='step', color='gray', alpha=0.5, bins=16)
    #         ssDif.append(np.abs(cur_ss.mean() - ssTruth))
    #
    #     fig.suptitle('Storativity')
    #     plt.grid()
    #     plt.xlabel('Storativity [-]')
    #     fig.savefig(os.path.join(paramdir, 'pdf_storativity%s.png' % mymode), dpi=600, format='png')
    #
    #     fig2.suptitle('Storativity')
    #     plt.grid()
    #     plt.xlabel('Log Storativity [-]')
    #     fig2.savefig(os.path.join(paramdir, 'pdfLog_storativity%s.png' % mymode), dpi=600, format='png')
    #
    #     myplt.xyplot(np.arange(0, len(ssMean)), np.array(ssMean), title='Storativity', textbox='',
    #                  label='Ensemble Mean', xlabel='Iteration', ylabel='Storativity [-]',
    #                  savefile=os.path.join(paramdir, 'EnsembleMean_storativity%s.png' % mymode), eps=eps)
    #
    #     myplt.xyplot(np.arange(0, len(ssDif)), np.array(ssDif), title='Storativity', textbox='',
    #                  label='Average Absolute Error', xlabel='Iteration', ylabel='Error [-]',
    #                  savefile=os.path.join(paramdir, 'AAE_storativity%s.png' % mymode), eps=eps)
    #     np.savetxt(os.path.join(paramdir, 'storativity_error_%s.txt' % mymode),
    #                np.array((ssDif)).T, header='ssdif_error')

    print('Finished')

if __name__ == '__main__':
    main()
