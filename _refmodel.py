"""
Generate a reference model and synthetic output.
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon
"""
import sys
import os
import scipy.sparse as ssp
import matplotlib.pylab as plt
from matplotlib.offsetbox import AnchoredText
import numpy as np
funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import _enkf.addfunc.hgs as hg
import _enkf.addfunc.mystats as mysst
import _enkf.addfunc.dirs as dm


def main():
    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #
    # Model data:
    modelname = 'ModelExample'
    curKFile = 'kkkGaus.dat'
    mymode = 'fl_'  # 'fl_' or 'tr_'
    gridformat = 'tp2018'

    runModel = True
    savedata = True
    plot = True
    saveplots = True

    # Relevant for transport models:
    calcmomentsPW = False # Calculate moments specifically for the pumping well
    pumpingRate = 2.5  # m3 to L
    injectedMass = 0.001107*1e6  # 0.001096 * 1e6  # 100000, or  e.g. 0.00206 * 1e6  # in milligrams
    pw_file = 'tr_simo.observation_well_conc.B6.fluorescein.dat'  # File name of the pumping well data

    # Relevant for model outputs:
    irregularGrid = False
    neg2zero = False
    head2dwdn = True
    cumbtc = False
    norm = False
    moments = False
    std_dev = 1.0e-3  # 1.0e-9  # As an example assumed a std error measurement, Flow:1.0e-3, tr: 1e-8
    ini_head = 10.0  # 10.0

    # ---------------------------------------------------------------------------------------------#
    # --------------------- Start with the calculations -------------------------------------------#
    # ---------- If you are not sure on what's going on, don't modify the next section ------------#
    # ---------------------------------------------------------------------------------------------#
    if 'fl' in mymode:
        mymodel = 'Flow'  # _fl_26062015_synthetic' '_tr_26062015_synthetic_b3top_fluor'
    elif 'tr' in mymode:
        mymodel = 'Transport'
    Flowdir = 'Flow'  # Used only if transport is to be run

    modesDICT = {'ow_str_fl_': 'observation_well_flow', 'ow_str_tr_': 'observation_well_conc',
                 'plotYlabel_fl_': 'Drawdown [m]', 'plotYlabel_tr_': 'Concentration [$g/l$]',
                 'plotXlabel': 'Time [s]',
                 'plot_str_fl_': 'drawdown', 'plot_str_tr_': 'btc'}
    mainPath = os.path.join(os.path.dirname(__file__), '_dataAssim', modelname)
    modeldata_dir = os.path.join(mainPath, 'model_data')
    curModel_dir = os.path.join(mainPath, 'model', mymodel)

    if cumbtc is True:
        xtrastr = 'cum'
    elif moments is True:
        xtrastr = 'moments'
    else:
        xtrastr = ''

    # 0. Run and/or extract model outputs:  ################################################################
    # --------------------------------      ################################################################
    mytimes, mymodOutput = hg.run_refmodel(mainPath, curKFile, Flowdir, modeldata_dir, mymodel, mymode=mymode,
                                           runmodel=runModel, std_dev=std_dev, ini_head=ini_head, neg2zero=neg2zero,
                                           head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments,
                                           irregularGrid=irregularGrid, ref=True, gridformat = gridformat)

    # 1. Load model observations:           ################################################################
    # --------------------------------      ################################################################
    if mymode is 'tr_':
        ow_str = 'observation_well_conc'
    elif mymode is 'fl_':
        ow_str = 'observation_well_flow'
    mylist = dm.getdirs(curModel_dir, mystr=ow_str, fullpath=True, onlylast=False)
    welldata = hg.read_save_ow_coord(mylist, os.path.join(modeldata_dir, '_obswellscoord%s.dat' % mymode))

    # 2. Plot model observations:           ################################################################
    # --------------------------------      ################################################################
    if (plot is True) and (moments is False):
        fig, ax = plt.subplots(1)
        fig.set_figheight(8.)
        fig.set_figwidth(12.)
        ax.set_title('Model output for: %s%s' % (modelname, mymodel))
        ax.set_ylabel(modesDICT['plotYlabel_%s' % mymode])
        ax.set_xlabel(modesDICT['plotXlabel'])
        for ii in range(0, mymodOutput.shape[-1]):
            ax.plot(mytimes, mymodOutput[:, ii], '-x', label=welldata[ii, 0], ms=4)

        if mymode == 'tr_':
            try:
                pumpWellConc = np.loadtxt(os.path.join(curModel_dir, pw_file), skiprows=2, usecols=(0, 1))
                mytimes = pumpWellConc[:, 0]
                pumpWellConc = pumpWellConc[:,1]
            except:
                pumpWellConc = hg.rmodout(os.path.join(curModel_dir, pw_file), mytimes, mymode, dualdomain=True, hgs='2018')
                # pumpWellConc = np.loadtxt(os.path.join(curModel_dir, pw_file), skiprows=3, usecols=(0, 1))
            if cumbtc is True:
                plt.fill_between(mytimes, np.cumsum(pumpWellConc), 0, color='.8')
            elif cumbtc is False:
                plt.fill_between(mytimes, pumpWellConc, 0, color='.8')

        textstr = '$\sigma=%.2e$\n$Initial head=%s$' % (std_dev, ini_head)
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        # place a text box in upper left in axes coordinates
        # ax.legend(loc='best', shadow=True, fontsize=6.5, numpoints=1, markerscale=0.5)
        boxtext = AnchoredText(textstr, loc=1)
        ax.add_artist(boxtext)
        # ax.text(0.8, 0.8, textstr, transform=ax.transAxes, fontsize=11, verticalalignment='top', bbox=props)

        plt.grid(True)
        if saveplots is True:
            fig.savefig(os.path.join(modeldata_dir, '%s%s.png' % (modesDICT['plot_str_%s' % mymode], xtrastr)),
                        bbox_inches='tight', dpi=700,
                        transparent=False,
                        format='png')
            fig.savefig(os.path.join(modeldata_dir, '%s%s.eps' % (modesDICT['plot_str_%s' % mymode], xtrastr)),
                        bbox_inches='tight', dpi=700,
                        transparent=True,
                        format='eps')
            print('--> Output plot stored in << %s >>' % os.path.join(modeldata_dir,
                                                                      '%s' % modesDICT['plot_str_%s' % mymode]))
        plt.show()
        plt.close()

    # 3. Calculate moments, independent from what was performed before: ####################################
    # --------------------------------      ################################################################
    if calcmomentsPW is True:
        # For the pumping well separately:
        assert mymode == 'tr_', "Transport mode not selected, no sense to compute moments with this function"
        concentration = pumpWellConc
        time = mytimes
        # time, concentration = np.loadtxt(os.path.join(curModel_dir, pw_file), skiprows=2, usecols=(0, 1), unpack=True)
        concentration *= 1e3  # from kg/m3 to mg/L
        print('Concentration was assumed to be in kg/m3 and has been changed to mg/L!')

        dataObj = mysst.Miscel(time, concentration)

        # Get the moments:
        Zero_m = dataObj.tempmoments(degree=0, central=False, full_output=False)
        First_m = dataObj.tempmoments(degree=1, central=False, full_output=False)
        Mass = Zero_m * pumpingRate  #
        First_mN = First_m / Zero_m  # Mean ArrivalTime

        print('Recovered Mass: %s milligrams' % Mass)
        print('Recovered percentage: %s' % (Mass / injectedMass * 100))
        print('Mean arrival time: %s' % First_mN)

        if plot is True:
            fig, ax = plt.subplots(1)
            fig.set_figheight(8.)
            fig.set_figwidth(12.)
            ax.plot(time / 60, concentration, 'o-')
            ax.fill_between(time / 60, concentration, 0, color='.8')
            ax.plot(np.ones((50, 1)) * First_mN / 60.,
                    np.linspace(np.min(concentration), np.max(concentration) * 1.2, 50), 'r--', lw=2)
            ax.set_ylabel('Concentration [mg/L]')
            ax.set_xlabel('Time [minutes]')
            ax.set_title('Breakthrough curve at extraction well')
            plt.grid(True)
            textstr = 'Injected Mass:      %4.2f gr\n' \
                      'Recovered Mass:   %4.2f gr\n' \
                      'Extraction Rate:    %4.2f L/s\n' \
                      'Mean arrival time: ~%d [min]' \
                      % (injectedMass / 1000., Mass / 1000., pumpingRate, First_mN / 60.)
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            # place a text box in upper left in axes coordinates
            ax.text(0.6, 0.8, textstr, transform=ax.transAxes, fontsize=10, verticalalignment='top', bbox=props)
        if saveplots is True:
            fig.savefig(os.path.join(modeldata_dir, 'btc_pw.png'), bbox_inches='tight', dpi=700, transparent=False,
                        format='png')
            fig.savefig(os.path.join(modeldata_dir, 'btc_pw.eps'), bbox_inches='tight', dpi=700, transparent=True,
                        format='eps')
            print('--> BTC plots for the pumping well stored in << %s >>' % os.path.join(modeldata_dir, 'btc_pw'))

    # 3.Save moments in a file for further use with the filter:             ##########################
    # Zero_m, First_m, First_mN, Second_mC, Second_mCN, Third_mC, Third_MCN ##########################
    # --------------------------------------------------------------------  ##########################
    if savedata is True:
        np.savetxt(os.path.join(modeldata_dir, 'Field%s%s.dat' % (mymode, xtrastr)), mymodOutput, fmt='%10.9e')
        print('--> Ref model done and outputs stored in << %s >>' % 'Field%s%s.dat' % (mymode, xtrastr))

    print('Done!')


if __name__ == '__main__':
    main()
