# -*- coding: utf-8 -*-
"""
Plot some results from the EnKF assimilation
Created on Thursday 27 October 11:50:00 2015
@author: eesl
"""
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pylab as plt
import numpy as np
import itertools
import sys
import os
import pdb
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.fieldgen as fg
import addfunc.dirs as dm
import addfunc.gridmanip as gm
import addfunc.hgs as hg
import addfunc.mystats as mysst
import addfunc.matmult.matmul_opt as mm
import addfunc.matmult.mylikelihood as mml
import addfunc.plotResults as myplt
plt.rcParams.update({'figure.max_open_warning': 0})


def main():
    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #
    modelname = '2d_test2'  # Model under investigation: e.g '07072015'
    remarks = 'h_01df_noga_from_fl2f'  # Provide remarks to identify various aspects of the assimilation
    mymode = 'tr_'  # Type of model to run: 'fl_' or 'tr_'
    synthetic = True  # Is it a synthetic example, meaning there is a synthetic true field?
    trim = True  # Trim the total model area to use only the elements from a smaller domain
    subgrid = True
    covariances = True
    # Model grid settings
    dx = 0.1
    dy = dx
    xlim = [34, 67]  # [30, 70]     # xlim = [0, 100]
    ylim = [52, 68]  # [15, 35]      # ylim = [0, 50]
    zlim = [0, 1]  # zlim = [0, 1]
    dxtick = 33/5
    dytick = 16/5
    # Output settings:
    plot = True
    eps = False
    myxticks = [34, xlim[0]+dxtick, xlim[0]+(2*dxtick), xlim[0]+(3*dxtick), xlim[0]+(3*dxtick), xlim[0]+(4*dxtick), xlim[0]+(5*dxtick)]
    myyticks = [52, ylim[0]+dytick, ylim[0]+(2*dytick), ylim[0]+(3*dytick), ylim[0]+(3*dytick), ylim[0]+(4*dytick), ylim[0]+(5*dytick)]

    # Additional parameters todo: integrate additional plotting features
    varMax = 1.0
    logkMax = -2.4
    logKMin = -8
    storativity = False  # Update storativity (homogeneous)
    ssTruth = 3.0e-3
    # porosity = False  # Update porosity (homogeneous)
    # ppTruth = 0
    # dispersivity = False  # Update dispersivity (homogeneous)
    # ddTruth = 0

    """ """
    # 0. Automatically generate necessary directories:
    # ----------------------------------------------------------------- #
    mystr = 'kkk'
    mystr = mystr + '_' + mymode
    mymaindir = os.path.join(dm.homedir(), '_dataAssim', modelname)  # Inversion directory
    # mymaindir = os.path.join(r'E:\phd\_DataAssimilation', 'models', modelname)  # Inversion directory
    str_assimilation = '%s%s' % (modelname, remarks)

    modeldatadir = os.path.join(mymaindir, 'model_data')
    resdir = os.path.join(mymaindir, 'results_%s' % str_assimilation)
    realizationdir = os.path.join(mymaindir, 'realizations_%s' % str_assimilation)

    # Figure directories:
    # figdir = os.path.join(resdir, '_figs')
    paramdir = os.path.join(resdir, 'parameters')
    covdir = os.path.join(paramdir, 'Qyy')
    covdir_2 = os.path.join(paramdir, 'Qyy_diagonal')
    kkkdir = os.path.join(paramdir, 'kkk')

    for cur_dir in [paramdir, covdir, covdir_2, kkkdir]:
        if os.path.isdir(cur_dir) is False:
            os.makedirs(cur_dir)

    # --------------------------------------------------- #
    # -------- Start with the calculations -------------- #
    # --------------------------------------------------- #

    # 0. Get the nodes and elements of the area of the model of interest:
    # --------------------------------------------------- #
    gridFile = os.path.join(modeldatadir, 'meshtecplot.dat')
    assert os.path.isfile(gridFile), 'No grid file in < model_data > folder. Get one!!'
    inNodes, inElem = gm.sel_grid(gridFile, xlim, ylim, zlim, store=False)

    elements = len(inElem)
    elY, elX = len(np.arange(ylim[0], ylim[1], dy)), len(np.arange(xlim[0], xlim[1], dx))

    # 1. Load wells and adjust its coordinates for plotting:
    # ---------------------------------------------------
    well_coord = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(1, 2, 3))
    # wells_ids = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(0,), dtype='S')
    well_coord *= 1 / dx

    # 2. Initiate relevant variables:
    # --------------------------------
    lstresdir = dm.getdirs(os.path.join(realizationdir, 'kkk'), mystr=mystr, fullpath=False, onlylast=False)
    print('Using directory << %s >> ' % os.path.join(realizationdir, 'kkk'))
    print('Files to work with: \n\t%s...' % lstresdir[0:5])
    nash, pears, rel_error, abs_error, param_bias, total_var, AAE, AAESD = [], [], [], [], [], [], [], []

    # 3. Load output times:
    # ---------------------------------------------------------------- #
    outputtimes = np.loadtxt(os.path.join(modeldatadir, '_outputtimes%s.dat' % mymode))

    # 4. Load synthetic parameter truth ( if synthetic = True)
    # ---------------------------------------------------------------- #
    if synthetic is True:
        orig_field = np.loadtxt(os.path.join(mymaindir, 'model', 'Flow', 'kkkGaus.dat'), usecols=(1,))
        mean_value = np.mean(orig_field)  # Mean log K used to generate fields
        orig_field = np.log(orig_field[inElem])

        if trim is True:
            well_points = np.array((well_coord[:, 0]-xlim[0]/dx, well_coord[:, 1]-ylim[0]/dy)).T
        elif trim is False:
            well_points = well_coord[:, 0:2]
        myplt.plotimshow(orig_field, elY, elX,
                         points=well_points,
                         title='Original synthetic field',
                         cbtitle='$ln(K)$',
                         savefile=os.path.join(kkkdir, 'Y_%ssynthetic.png' % mymode),
                         minval=logKMin, maxval=logkMax,
                         xlim=[0, elX],
                         ylim=[0, elY],
                         xticks=myxticks,
                         yticks=myyticks)

    # # 5. Load initial ensemble and plot initial Qyy
    # # ---------------------------------------------------------------- #
    ini_ensembleparam = np.load(os.path.join(realizationdir, 'kkk', lstresdir[0]))
    ini_ensembleparam = np.log(ini_ensembleparam)
    if subgrid is not True:
        ini_ensembleparam = np.log(ini_ensembleparam[inElem, :])

    if mymode == 'fl_':
        lstresdir = lstresdir[1:]
    # orig_Qyy = mml.autocov(ini_ensembleparam, diag=True, parallel=False)
    orig_Qyy = np.empty((ini_ensembleparam.shape[0],))
    for ii in range(0, orig_Qyy.shape[0]):
        cur_chunk = ini_ensembleparam[ii, :]
        cur_calc = (cur_chunk - cur_chunk.mean())
        orig_Qyy[ii] = (1. / (ini_ensembleparam.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)
    np.save(os.path.join(covdir_2, 'Qyy_diag_%siter000_timestep000' % mymode), orig_Qyy)
    myplt.plotimshow(orig_Qyy, elY, elX, points=well_points, title='Assimilation step 0: Initial Ensemble',
                     cbtitle='Variance $ln(K)$',
                     savefile=os.path.join(covdir, 'Qyy_diag_%siter000_timestep000.png' % mymode),
                     minval=0.01, maxval=varMax,
                     xlim=[0, elX],
                     ylim=[0, elY],
                     xticks=myxticks,
                     yticks=myyticks)

    # 5. Plot stuff:
    # --------------
    for curidx, ii in enumerate(lstresdir):
        params = np.load(os.path.join(realizationdir, 'kkk', ii))
        params = np.log(params)
        if subgrid is not True:
            params = params[inElem, :]
        #Qyy = mml.autocov(params, diag=True, parallel=False)
        if covariances is True:
            # orig_Qyy = mml.autocov(ini_ensembleparam, diag=True, parallel=False)
            Qyy = np.empty((params.shape[0],))
            for jj in range(0, Qyy.shape[0]):
                cur_chunk = params[jj, :]
                cur_calc = (cur_chunk - cur_chunk.mean())
                Qyy[jj] = (1. / (params.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)


        #np.save(os.path.join(covdir_2, 'Qyy_diag_%s' % ii[2:-4]), Qyy)

            hg.kkk_bin2ascii(Qyy, os.path.join(covdir_2, 'Qyy_diag_%s.txt' % ii[2:-4]), elements,
                             biggrid_elid='', smallgrid_elid='', get_mean=False, exptransf=False, logtransf=False,
                             expand=False)

        # 5.1 Statistical analysis:
        # -------------------------
        nash.append(mysst.nash_sutcliffe(params.mean(axis=1), orig_field))
        pears.append(mysst.Pearson(params.mean(axis=1), orig_field))
        rel_error.append(mysst.relative_error(params.mean(axis=1), orig_field))
        abs_error.append(mysst.absolute_error(params.mean(axis=1), orig_field))
        param_bias.append(mysst.param_bias(orig_field, params.mean(axis=1)))
        total_var.append(mysst.tot_var(params, np.log(mean_value)))
        AAE.append(mysst.mean_abs_error(params.mean(axis=1), orig_field))
        AAESD.append(mysst.mean_ensemble_std(params, params.mean(axis=1)))

        # 5.2 Plot fields and covariances:
        # ---------------------------------
        if plot is True:
            if covariances is True:
                myplt.plotimshow(Qyy, elY, elX, points=well_points,
                                 title='Assimilation step %d: %s [sec]' % (curidx + 1, outputtimes[curidx]),
                                 cbtitle='Variance $ln(K)$',
                                 savefile=os.path.join(covdir, 'Qyy_diag_%s.png' % ii[2:-4]),
                                 minval=0.01, maxval=varMax,
                                 xlim=[0, elX],
                                 ylim=[0, elY],
                                 xticks=myxticks,
                                 yticks=myyticks)

            myplt.plotimshow(params.mean(axis=1), elY, elX, points=well_points,
                             title='Assimilation step %d: %s [sec]' % (curidx + 1, outputtimes[curidx]),
                             cbtitle='$ln(K)$',
                             savefile=os.path.join(kkkdir, 'Y_%s.png' % ii[2:-4]),
                             minval=logKMin, maxval=logkMax,
                             xlim=[0, elX],
                             ylim=[0, elY],
                             xticks=myxticks,
                             yticks=myyticks)
            myplt.plotimshow(orig_field - params.mean(axis=1), elY, elX, points=well_points,
                             title='Assimilation step %d: %s [sec]' % (curidx + 1, outputtimes[curidx]),
                             cbtitle='Difference in $ln(K)$',
                             savefile=os.path.join(kkkdir, 'Diff_Y_%s.png' % ii[2:-4]),
                             minval=-2, maxval=2,
                             xlim=[0, elX],
                             ylim=[0, elY],
                             xticks=myxticks,
                             yticks=myyticks)

            # 5.3 Plot pdf´s:
            # ---------------
            myplt.plothist_ensemble(params, X_orig=orig_field,
                                    title='Time step:%d t=%s s' % (curidx + 1, outputtimes[curidx]),
                                    norm=False, cum=False,
                                    textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                                    xlabel='Ln K',
                                    savefile=os.path.join(kkkdir, 'Ensemble_pdf_%s.png' % ii[2:-4]),
                                    eps=eps)
            # 5.4 Plot cdf´s:
            # ---------------
            myplt.plothist_ensemble(params, X_orig=orig_field,
                                    title='Time step:%d t=%s s' % (curidx + 1, outputtimes[curidx]),
                                    norm=False, cum=True,
                                    textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                                    xlabel='Ln K',
                                    savefile=os.path.join(kkkdir, 'Ensemble_cdf_%s.png' % ii[2:-4]),
                                    eps=eps)

            # 5.5 Plot a calibration graphic:
            # ---------------------------
            # print('Time step %s--> NS-coeff: %s\nPearsons: %s' % (idx, nash, pears))
            plt.plot(orig_field, params.mean(axis=1), 'rx')
            plt.title('Synthetic vs modeled parameters.\nTime step:%d t=%s s' % (curidx + 1, outputtimes[curidx]))
            plt.xlabel('Original parameters')
            plt.ylabel('Modeled parameters')
            plt.grid(True)
            plt.xlim([-9, -2])
            plt.ylim([-9, -2])
            plt.savefig(os.path.join(paramdir, 'calibplot_EnsembleMean_%s.png' % ii[2:-4]), dpi=600, format='png')
            plt.close()

            # 5.6 Plot additional statistics
            # ---------------------------
    myplt.xyplot(np.arange(1, len(total_var) + 1, 1), total_var,
                 title='Total variance',
                 textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                 label='Total variance',
                 xlabel='Time step',
                 ylabel='Variance',
                 savefile=os.path.join(paramdir, 'total_variance_%s.png' % mymode), eps=eps)
    myplt.xyplot(np.arange(1, len(AAE) + 1, 1), AAE,
                 title='Average Absolute Error',
                 textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                 label='',
                 xlabel='Time step',
                 ylabel='AAE Ln K',
                 savefile=os.path.join(paramdir, 'AAE_%s.png' % mymode), eps=eps)
    myplt.xyplot(np.arange(1, len(AAESD) + 1, 1), AAESD,
                 title='Average Ensemble Standard Deviation',
                 textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                 label='',
                 xlabel='Time step',
                 ylabel='AESD Ln(K)',
                 savefile=os.path.join(paramdir, 'AAESD_%s.png' % mymode), eps=eps)

    nash.append(mysst.nash_sutcliffe(params.mean(axis=1), orig_field))
    pears.append(mysst.Pearson(params.mean(axis=1), orig_field))
    rel_error.append(mysst.relative_error(params.mean(axis=1), orig_field))
    abs_error.append(mysst.absolute_error(params.mean(axis=1), orig_field))
    param_bias.append(mysst.param_bias(orig_field, params.mean(axis=1)))
    total_var.append(mysst.tot_var(params, np.log(mean_value)))
    AAE.append(mysst.mean_abs_error(params.mean(axis=1), orig_field))
    AAESD.append(mysst.mean_ensemble_std(params, params.mean(axis=1)))

    np.savetxt(os.path.join(paramdir, 'errors_summarize_%s.txt' % mymode),
               np.array((nash, param_bias, total_var, AAE, AAESD)).T,
               header='nash, param_bias, total_var, AAE, AAESD')

    # np.savetxt(os.path.join(paramdir, 'errors_summarize_%s.txt' % mymode),
    #            np.array((nash, np.array(pears)[:,0], rel_error, abs_error, param_bias, total_var, AAE, AAESD)).T,
    #            header='nash, pears,rel_error, abs_error, param_bias, total_var, AAE, AAESD')
    if storativity is True:
        ssPath = os.path.join(realizationdir, 'storativity')
        ssList = os.listdir(ssPath)

        ssMean = []
        ssDif = []

        for idx, curss in enumerate(ssList):
            cur_ss = np.load(os.path.join(ssPath, curss))
            ssMean.append(cur_ss.mean())
            fig = plt.figure(1)
            plt.hist(cur_ss.T, histtype='step', color='gray', alpha=0.5, bins=16)
            fig2 = plt.figure(2)
            plt.hist(np.log(cur_ss).T, histtype='step', color='gray', alpha=0.5, bins=16)
            ssDif.append(np.abs(cur_ss.mean() - ssTruth))

        fig.suptitle('Storativity')
        plt.grid()
        plt.xlabel('Storativity [-]')
        fig.savefig(os.path.join(paramdir, 'pdf_storativity%s.png' % mymode), dpi=600, format='png')

        fig2.suptitle('Storativity')
        plt.grid()
        plt.xlabel('Log Storativity [-]')
        fig2.savefig(os.path.join(paramdir, 'pdfLog_storativity%s.png' % mymode), dpi=600, format='png')

        myplt.xyplot(np.arange(0, len(ssMean)), np.array(ssMean), title='Storativity', textbox='',
                     label='Ensemble Mean', xlabel='Iteration', ylabel='Storativity [-]',
                     savefile=os.path.join(paramdir, 'EnsembleMean_storativity%s.png' % mymode), eps=eps)

        myplt.xyplot(np.arange(0, len(ssDif)), np.array(ssDif), title='Storativity', textbox='',
                     label='Average Absolute Error', xlabel='Iteration', ylabel='Error [-]',
                     savefile=os.path.join(paramdir, 'AAE_storativity%s.png' % mymode), eps=eps)
        np.savetxt(os.path.join(paramdir, 'storativity_error_%s.txt' % mymode),
                   np.array((ssDif)).T, header='ssdif_error')


if __name__ == '__main__':
    main()
