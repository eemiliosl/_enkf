"""
Functions to deal with GROK and HGS
Created on Wed Mar 25 20:39:31 2015
@author: Emilio Sanchez-Leon
"""
import sys
import os
import numpy as np
import datetime
import shutil
import subprocess as sp
import platform
import multiprocessing as mp
import time
import scipy
from itertools import repeat
from statistics import mean
import scipy.sparse as ssp
import fieldgen as fg
import dirs as dm
import gridmanip as gm
import mystats as mysst
# import kalman as klm
import pdb


def rmodout(myfile, mytimes, mymode, dualdomain=True):
    """ Read HydroGeoSphere outputs. Reads from well-like and point-like type of observation. It supports reading heads
    from flow simulations and concentrations from a single- and double-domain model.
            Arguments:
            ----------
        myfile:     str, fullpath with the name of the file to be loaded
        mytimes:    np array,  float or int array with times of interest
        mymode:     str, model run mode. 'fl_' for flow model, 'tr_for transport'
        dualdomain: bool, if True, reads mob and immob concentrations from a dual domain transport model. Default = True
            Returns:
            --------
        mymodmeas:     np.array, with model outputs at specific times (nmeas x 1)
        """
    try:
        # Observation point HGS format
        if (mymode == 'tr_') and (dualdomain is True):
            hgsTime, mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=2, usecols=(0, 1, 2), unpack=True)
            mymodmeas = mymodmeas1 + mymodmeas2
        else:
            hgsTime, mymodmeas = np.loadtxt(myfile, skiprows=2, usecols=(0, 1), unpack=True)
    except:
        # Observation well HGS format:
        mymodmeas = []
        idx_line, hgsTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)

        if isinstance(idx_line, int) is True:
            hgsTime = np.asarray(hgsTime, dtype=float)
            mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0,))
            mymodmeas = np.mean(mymodmeas)

        if isinstance(idx_line, int) is False:
            with open(myfile, 'r') as f:
                mylines = f.readlines()

            for yy in range(0, len(idx_line), 1):
                if yy < len(idx_line) - 1:
                    myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                elif yy == len(idx_line) - 1:
                    myvalues = np.asarray(mylines[idx_line[yy] + 1:])

                try:
                    mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
                except:
                    mymodmeas.append(mean([float(cur_line.split(' ')[3]) for cur_line in myvalues]))

        if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
            mymodmeas = np.asarray(mymodmeas)

    # If a single output time is generated, create an extra dimension:
    if (isinstance(mytimes, int) is True) or (isinstance(mytimes, float) is True):
        mytimes = np.expand_dims(mytimes, 1)

    # %% Mask to get only the elements of interest:
    if isinstance(mymodmeas, np.ndarray) is False:
        mymodmeas = np.expand_dims(mymodmeas, axis=1)
        if len(mytimes) == 1:
            mytimes = int(round(mytimes[0] * 10)) / 10

    mymodmeas = mymodmeas[np.where(np.in1d(hgsTime, mytimes))]

    if mymodmeas.ndim > 1:
        mymodmeas = mymodmeas.squeeze()
    return mymodmeas


mypath = r'C:\Users\Emilio\Desktop\grid_comparisons'
mytimes = np.loadtxt(os.path.join(mypath, 'grid_new', 'mytimes.outtime'))
# mytimes = np.loadtxt(os.path.join(mypath, 'grid_old', 'mytimes.outtime'))
# # myfile = r'C:\Users\IRTG\Desktop\grid_test\tr_simo.observation_well_conc.1_1point.fluorescein.dat'
# myfile = r'C:\Users\IRTG\Desktop\grid_test\tr_simo.observation_well_conc.1_1well.fluorescein.dat'
mymode = 'tr_'

files = dm.getdirs(os.path.join(mypath, 'grid_new'), mystr='observation_well_conc', fullpath=True, onlylast=False)

# Start collecting them in a single 2D array
all_modout = np.array([])

for idx, cur_file in enumerate(files):
    selMeas = rmodout(cur_file, mytimes, mymode, dualdomain=True)

    all_modout = np.append(all_modout, selMeas)

all_modout = np.reshape(all_modout, (-1, len(files)), order='F')

print('Pause')




