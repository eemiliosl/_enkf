# -*- coding: utf-8 -*-
""" Functions to create socket IP connections """
import socket, ssl, sys, subprocess as sp, platform
from datetime import datetime


def getIP():
    """ Returns IP address, avoids 127.0.0.1 which is the default value in Linux
    Returns:
        address: Str, the right IP address
    """
    if platform.system() == 'Linux':
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('4.2.2.1', 0))
        address = s.getsockname()[0]
    else:
        address = socket.gethostbyname(socket.gethostname())

    return address


def server(ip, port):
    """ Build up a socket for the SERVER and waits for incoming certified connections
    Arguments:
        ip:   Str, host IP address
        port:      Int, port number to create server
    Returns:
        myserver: Socket object, ready to accept certified client connections
    """
    try:
        myserver = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        sys.exit('Error[001]: Failed to create SERVER socket')

        # Bind the server socket to the port
    address = (ip, port)
    myserver.bind(address)
    print('Server on %s(%s) port %s' % (socket.gethostname(), address[0], address[1]))

    # Listening for incoming connections:
    myserver.listen(100)

    return myserver


def client(myserver):
    """ Function no more needed?
    Arguments:
        myserver:     Str, IP and port of the server
    """
    print('Initializing connection...')
    try:
        myclient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        sys.exit('Error[001]: Failed to create socket')

    try:
        # ssl_socket_client = ssl.wrap_socket(client_sock, ca_certs='server.crt', cert_reqs=ssl.CERT_REQUIRED)
        client_ssl = ssl.wrap_socket(myclient, certfile="cert", keyfile="key")
    except socket.error:
        sys.exit('SSL connection denied. Check certificate file!')

    try:
        client_ssl.connect(myserver)
        print('Connected to %s port %s' % myserver)
    except socket.error:
        sys.exit('SSL connection with the server denied. Check Server address!')

    return client_ssl


def senddata(myserver, ncpu):
    """ Connects a client to a server to receive indices for working.
        The connection is secured using SSL with a certificate file. For generation
        of first ensemble
    Arguments:
        myserver:     Str, IP and port of the server
        ncpu:       Int, Number of CPUs available in the client
    Returns:
        idrecv:    Tuple, indices for the processes
        flag:      Str, flag to tell the process in which stage it is found
    """
    print('Initializing connection...')
    try:
        myclient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        sys.exit('Error[001]: Failed to create socket')

    try:
        # ssl_socket_client = ssl.wrap_socket(client_sock, ca_certs='server.crt', cert_reqs=ssl.CERT_REQUIRED)
        myclientssl = ssl.wrap_socket(myclient, certfile="cert", keyfile="key")
    except socket.error:
        sys.exit('SSL connection denied. Check certificate file!')

    try:
        myclientssl.connect(myserver)
        print('Connected to %s port %s' % myserver)
    except socket.error:
        sys.exit('SSL connection with the server denied. Check Server address!')

    try:
        myclientssl.send(str(ncpu).encode('utf16'))

        idrecv = myclientssl.recv(1024).decode('utf16')
        flag = myclientssl.recv(1024).decode('utf16')

    except (socket.error, IOError, OSError, EOFError, IndexError, ValueError):
        sys.exit('Error when receiving the process indices!')

    try:
        myclientssl.send(idrecv.encode('utf16'))

    except (socket.error, IOError, OSError, EOFError, IndexError, ValueError):
        sys.exit('Error when sending the process indices!')
    finally:
        print('closing socket')
        myclientssl.close()

    return idrecv.split(',')[:-1], flag


def PortScanner(remoteServer):
    """ Scan for a free Ports
    Arguments:
        remoteServer: Str, remote host to scan (Computer name)
    """
    # %%Clear the screen
    sp.call('clear', shell=True)
    remoteServerIP = socket.gethostbyname(remoteServer)

    # %% Print a nice banner with information on which host we are about to scan
    print("Please wait, scanning remote host %s ..." % remoteServerIP)
    print("- Scan started at %s: " % (datetime.now()))

    # Check what time the scan started
    t1 = datetime.now()

    # %% Error handling:
    try:
        for port in range(1, 500):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((remoteServerIP, port))
            if result == 0:
                # if a socket is listening it will print out the port number
                print("Port {}: \t Open".format(port))
            sock.close()

    except KeyboardInterrupt:
        print("You pressed Ctrl+C")
        sys.exit()

    except socket.gaierror:  # Here is my host exception, in case you typed it wrong.
        sys.exit('Hostname could not be resolved. Exiting')

    except socket.error:  # finally socket error in case python is having trouble scanning or resolving the port
        sys.exit("Couldn't connect to server")

    t2 = datetime.now()  # Checking the time again

    # %% Calculates the difference of time, to see how long it took to run the script
    total = t2 - t1

    # Printing the information to screen
    print('Scan completed: %s sec' % total)
