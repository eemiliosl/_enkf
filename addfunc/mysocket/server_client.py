"""
Functions to support Client <---> Server chitchat to make model forward runs in parallel
"""

# Define initial settings:
import sys
import os
import numpy as np
import platform  # these are routines for numerical operations


def mk_id2send(idall, iddone, num_cpu):
    # %% Create proper indices to distribute work:
    C = np.searchsorted(idall, iddone)
    a = np.ma.array(idall, mask=False)
    a.mask[C] = True
    notsent = a.compressed()
    notsent.sort()
    id2send = notsent[0:int(num_cpu)]
    return id2send


def fmt_id2send(id2send, mymessage):
    # add zeros at the beginning for a proper alphabet listing in the directory
    for ii in range(0, len(id2send), 1):
        if id2send[ii] < 10:
            mymessage[ii] = np.core.defchararray.add('0000', id2send[ii].astype(str))
        elif (id2send[ii] >= 10) and (id2send[ii] < 100):
            mymessage[ii] = np.core.defchararray.add('000', id2send[ii].astype(str))
        elif (id2send[ii] >= 100) and (id2send[ii] < 1000):
            mymessage[ii] = np.core.defchararray.add('00', id2send[ii].astype(str))
    # Further formatting of the message to have clear identifiers
    mymessage = np.core.defchararray.add(mymessage, ',')
    mymessage = ''.join(mymessage)
    return mymessage


def fmt_idrecv(idrecv, iddone):
    # Format received message with ids
    idrecv = np.array([idrecv.split(',')[:-1]]).astype(int)
    if idrecv.shape[1] > 1:
        idrecv = idrecv.squeeze()
    elif idrecv.shape[1] == 1:
        idrecv = idrecv[0]
    idrecv.sort()

    iddone = np.r_[iddone, idrecv]
    iddone.sort()
    iddone = iddone.astype(int)
    iddone = np.unique(iddone)
    return iddone


def fmt_filename2send(myfulldir):
    """ Extract the file name from the full directories """
    if platform.system() == 'Linux':
        filestr = str(myfulldir.split('/')[-1])
    if platform.system() == 'Windows':
        filestr = str(myfulldir.split('\\')[-1])
    return filestr


def storedir(myFilename, mypath_dict):
    # Select in which folder to store the data:
    if 'kkk' in myFilename:
        mypath = mypath_dict['fld_reckkk']
    elif 'final' in myFilename:
        mypath = mypath_dict['fld_recheads']
    elif 'mod' in myFilename:
        mypath = mypath_dict['fld_recmodobs']
    return mypath
