"""
Functions to generate parameter fields
Created on Wed Mar 25 20:39:31 2015
@author: Emilio Sanchez-Leon
"""

""" """
import sys
import os
import time
import numpy as np
# import matplotlib.pyplot as plt
import scipy
import scipy.sparse as ssp
import scipy.stats as sst
# import mystats
# mainPath = os.path.dirname(__file__)


def readParam(myDir):
    """
    Load the parameters for generating random gaussian fields
        Arguments:
        ----------
    myDir:  Str, full file path (including filename)
        Returns:
        --------
    Corresponding parameters: xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim,
    zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z
    """
    try:
        fieldparam = {}
        myobj = open(myDir, 'r')
        mylines = myobj.readlines()
        for cur_lin in mylines:
            if ("!" in cur_lin) or (cur_lin is '\n'):
                pass
            else:
                fieldparam[cur_lin.split(':')[0]] = cur_lin.split(':')[1].rstrip('\n')
                if ' ' in fieldparam[cur_lin.split(':')[0]]:
                    if 'exp' in fieldparam[cur_lin.split(':')[0]].strip(' '):
                        fieldparam[cur_lin.split(':')[0]] = fieldparam[cur_lin.split(':')[0]].strip(' ')
                    else:
                        fieldparam[cur_lin.split(':')[0]] = float(fieldparam[cur_lin.split(':')[0]].strip(' '))
        myobj.close()
    except:
        print('Parameters for field generation are not properly defined in the input file')
        raise

    # Boundaries of the flow model domain
    xlen, ylen, zlen = [fieldparam['xlen'], fieldparam['ylen'], fieldparam['zlen']]
    nxel, nyel, nzel = [fieldparam['xlen']/fieldparam['dx'], fieldparam['ylen']/fieldparam['dy'], fieldparam['zlen']/fieldparam['dz']]

    # elX_size,  elY_size, elZ_size  = [xlen/nxel, ylen/nyel, zlen/nzel]

    # Variogram parameters
    Y_model, Y_var, beta_Y = [fieldparam['Y_model'], fieldparam['Y_var'], fieldparam['beta_Y']]
    lambda_x, lambda_y, lambda_z = [fieldparam['lmbx'], fieldparam['lmby'], fieldparam['lmbz']]

    # Boundaries of the transport model domain
    xlim = np.array([fieldparam['x0_sub'], fieldparam['x1_sub']])
    ylim = np.array([fieldparam['y0_sub'], fieldparam['y1_sub']])
    zlim = np.array([fieldparam['z0_sub'], fieldparam['z1_sub']])

    return xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z
    #  elX_size, elY_size, elZ_size


def genFields_FFT_3d(xlen, ylen, zlen, nxel, nyel, nzel, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z,
                     plot=False, mydir=''):
    """
    Generate 3D gaussian fields using spectral methods (Detrich & Newsam). Modified from Cirpka, 2014.
    Works only for regularly spaced rectangular grids.
        Arguments:
        ----------
    xlen,ylen,zlen: Float, length width and height of domain [m], X, Y and Z axis
    nxel,nyel,nzel: Integer, number of elements in X, Y and Z
    Y_model:        Str, cov function model: 'exponential', 'gaussian', 'spherical'
    Y_var:          Float, variance of field
    beta_Y:         Float, mean value. NOT in log scale!
    lambda_X,Y,Z:   correlation length in x, y, z
    plot: 			Boolean, True: create a plot and save the figure as a png file
        Returns:
        --------
    Parameter values in original units (e.g. in m/s) and in the right order for HGS.
    If plot is true, it creates the corresponding pcolor maps of log(K) and save
    them as .png file.
        """
    nxel = int(nxel)
    nyel = int(nyel)
    nzel = int(nzel)
    # Y_model = Y_model.astype(str)
    scipy.random.seed()
    start_time = time.clock()
    # %% Define parameters:
    # beta_Y = np.array([np.log(beta_Y)])  # mean value
    # Qbb = np.array([1.])  # covariance of mean value
    # invQbb      = np.linalg.inv([Qbb])  # Inverse of Qbb

    domain_xlen = xlen  # length of domain [m], or X axis
    domain_ylen = ylen  # width of domain [m] or Y axis
    domain_zlen = zlen  # height of domain [m] or Z axis

    el_xlen = domain_xlen / nxel  # length of elements in X axis
    el_ylen = domain_ylen / nyel  # length of elements in Y axis
    el_zlen = domain_zlen / nzel  # length of elements in Z axis

    if (Y_model == 'exponential') or (Y_model == 'gaussian'):
        embfac = 10
    elif Y_model == 'spherical':
        embfac = 1
    else:
        raise ValueError('no valid geostatistical model')

    # %% Define embedded domain size:
    nxele = int(np.ceil(np.amax([2 * embfac * lambda_x, domain_xlen + (embfac * lambda_x)]) / el_xlen))
    nyele = int(np.ceil(np.amax([2 * embfac * lambda_y, domain_ylen + (embfac * lambda_y)]) / el_ylen))
    nzele = int(np.ceil(np.amax([2 * embfac * lambda_z, domain_zlen + (embfac * lambda_z)]) / el_zlen))

    nele = nxele * nyele * nzele  # Total number of elements in embedded domain

    domain_xlene = nxele * el_xlen  # length of embedded domain [m], or X axis
    domain_ylene = nyele * el_ylen  # width of embedded domain [m], or Y axis
    domain_zlene = nzele * el_zlen  # height of embedded domain [m], or Z axis

    # %% Define original domain coordinates (distances):
    yel_vec = np.arange(0, nyel, 1, dtype='float32') * el_ylen
    xel_vec = np.arange(0, nxel, 1, dtype='float32') * el_xlen
    zel_vec = np.arange(0, nzel, 1, dtype='float32') * el_zlen
    nel = len(yel_vec) * len(xel_vec) * len(zel_vec)

    if plot is False:
        xel, yel, zel = np.meshgrid(xel_vec, yel_vec, zel_vec, sparse=True)
    else:
        xel, yel, zel = np.meshgrid(xel_vec, yel_vec, zel_vec)

    h_eff = np.float32(np.sqrt((xel / lambda_x) ** 2 + (yel / lambda_y) ** 2 + (zel / lambda_z) ** 2))

    # %% Define prior statistics:
    X1 = np.ones((nyel, nxel, nzel))  # This one is used internally in python
    X = np.ravel(X1, 1)  # This is used for exporting to HGS

    # %% Define embedded domain coordinates (distances):
    xele_vec = np.arange(0, nxele, 1, dtype='float32') * el_xlen
    yele_vec = np.arange(0, nyele, 1, dtype='float32') * el_ylen
    zele_vec = np.arange(0, nzele, 1, dtype='float32') * el_zlen

    if plot is False:
        xele, yele, zele = np.meshgrid(xele_vec, yele_vec, zele_vec, sparse=True)
    elif plot is True:
        xele, yele, zele = np.meshgrid(xele_vec, yele_vec, zele_vec)

    xele = np.minimum(xele, domain_xlene - xele)
    yele = np.minimum(yele, domain_ylene - yele)
    zele = np.minimum(zele, domain_zlene - zele)
    h_effe = np.float32(np.sqrt(np.square(xele / lambda_x) + np.square(yele / lambda_y) + np.square(zele / lambda_z)))

    # %% Define original and embedded covariance matrix and its FFT:
    if Y_model == 'exponential':
        # Qs = Y_var * np.exp(-h_eff)
        Qse = Y_var * np.exp(-h_effe)

    elif Y_model == 'gaussian':
        # Qs = Y_var * np.exp(-h_eff ** 2)
        Qse = Y_var * np.exp(-h_effe ** 2)

    elif Y_model == 'spherical':
        Qs = Y_var * (1 - 1.5 * h_eff + 0.5 * h_eff ** 3)
        Qs[np.where(Qs > 1)] = 0
        Qse = Y_var * (1 - 1.5 * h_effe + 0.5 * h_effe ** 3)
        Qse[np.where(Qse > 1)] = 0

    FFTQse = np.fft.fftn(np.reshape(Qse, (nyele, nxele, nzele))).real
    lamb = np.float32(FFTQse / nele)

    epsilonreal = np.float64(np.random.standard_normal((FFTQse.shape[0], FFTQse.shape[1], FFTQse.shape[2])))
    epsilon = epsilonreal.astype(complex)
    epsilon.imag = np.float64(np.random.standard_normal((FFTQse.shape[0], FFTQse.shape[1], FFTQse.shape[2])))
    # epsilon = np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1],FFTQse.shape[2])) +
    # np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1],FFTQse.shape[2]))*1j
    Y_bar = epsilon * np.sqrt(lamb)
    Ye = np.fft.ifftn(Y_bar).real * nele

    # Arrays needed for tecplot order
    Ytemp = np.empty([nyel, nxel])
    Ybasket = np.zeros([1])
    print('Time to generate 3d field with %d elements: %s seconds' % (nel, time.clock() - start_time))
    # For plotting in python in the right order
    if plot is True:
        Y = np.empty([nyel, nxel, nzel])  # First create the desired matrix

    for ii in range(0, nzel):
        # For Tecplot
        Ytemp[0:nyel, 0:nxel] = Ye[0:nyel, 0:nxel, ii]
        Ytempshaped = np.reshape(Ytemp, nyel * nxel, order='A')
        Ybasket = np.r_[(Ybasket, Ytempshaped)]  # This array has the order needed in HGS

        # For plotting in python
        if plot is True:
            Y[0:nyel, 0:nxel, ii] = np.exp(Ye[0:nyel, 0:nxel, ii]) * X1[0:nyel, 0:nxel, ii] * beta_Y

    # Ybasket = Ybasket[1:] + X * beta_Y
    Ybasket = np.exp(Ybasket[1:]) * X * beta_Y
    # if plot is True:
    #     for ii in range(0, nzel):
    #         plt.figure(ii)
    #         plt.pcolormesh(xel[:, :, ii], yel[:, :, ii], Y[:, :, ii], shading='flat')
    #         plt.xlabel('X [m]')
    #         plt.ylabel('Y [m]')
    #         plt.colorbar()
    #         plt.title('Gaussian K [m/s] field %d' % ii)
    #         plt.savefig(os.path.join(mydir, '3D_K_Figure%d.png' % ii), bbox_inches='tight')
    #     plt.show()
    #     plt.close()
    #     for ii in range(0, nzel):
    #         plt.figure(ii)
    #         plt.pcolormesh(xel[:, :, ii], yel[:, :, ii], np.log(Y[:, :, ii]), shading='flat')
    #         plt.colorbar()
    #         plt.title('Gaussian ln(K)  field %d' % ii)
    #         plt.savefig(os.path.join(mydir, '3D_lnK_Figure%d.png' % ii), bbox_inches='tight')
    #     plt.show()
    #     plt.close()
    #     for ii in range(0, nzel):
    #         plt.figure(ii)
    #         plt.pcolormesh(xel[:, :, ii], yel[:, :, ii], np.log10(Y[:, :, ii]), shading='flat')
    #         plt.colorbar()
    #         plt.title('Gaussian $log_{10}$K  field %d' % ii)
    #         plt.savefig(os.path.join(mydir, '3D_log10K_Figure%d.png' % ii), bbox_inches='tight')
    #     plt.show()
    #     plt.close()

    # return np.exp(Ybasket)  # return K values in meter per seconds
    return Ybasket  # return K values in meter per seconds


def genFields_FFT_2d(xlen, ylen, nxel, nyel, Y_model, Y_var, beta_Y, lambda_x, lambda_y, plot=False):
    """
    Generate 2D gaussian fields using spectral method (Detrich & Newsam). Works only for regularly spaced
    rectangular grids. Modified from Cirpka, 2014.
        Arguments:
        ----------
    xlen,ylen:      Float, length width (or height) of domain [m], X, Y (or Z) axis
    nxel,nyel:      Integer, number of elements in X, Y
    Y_model:        Str, cov function: 'exponential', 'gaussian', 'spherical'
    Y_var:          Float, variance of field (give it in log scale)
    beta_Y:         Float, mean parameter value. NOT in log scale!
    lambda_X,Y:     correlation length in x, y
    plot: 			Boolean, True: create a plot and save the figure as a png file
        Returns:
        --------
    K field (m/s) in the right order for HGS. If plot is true, it creates
    the corresponding pcolor maps of log(K) and save them as a .png file.
    """
    Y_model = Y_model.astype(str)
    scipy.random.seed()
    start_time = time.clock()

    # %% Define parameters:
    nxel = int(nxel)
    nyel = int(nyel)
    domain_xlen = xlen  # length of domain [m], or X axis
    domain_ylen = ylen  # width of domain [m] or Y axis
    el_xlen = domain_xlen / nxel  # length of elements in X axis
    el_ylen = domain_ylen / nyel  # length of elements in Y axis

    if (Y_model == 'exponential') or (Y_model == 'gaussian'):
        embfac = 10
    elif Y_model == 'spherical':
        embfac = 1
    else:
        raise ValueError('no valid geostatistical model')

    # %% Define embedded domain size:
    nxele = int(np.ceil(np.amax([2 * embfac * lambda_x, domain_xlen + (embfac * lambda_x)]) / el_xlen))
    nyele = int(np.ceil(np.amax([2 * embfac * lambda_y, domain_ylen + (embfac * lambda_y)]) / el_ylen))
    nele = nxele * nyele
    domain_xlene = nxele * el_xlen  # length of embedded domain [m], or X axis
    domain_ylene = nyele * el_ylen  # width of embedded domain [m], or Y axis

    # %% Define original domain coordinates (distances):
    yel_vec = np.arange(0, nyel, 1, dtype='float') * el_ylen
    xel_vec = np.arange(0, nxel, 1, dtype='float') * el_xlen
    nel = len(yel_vec) * len(xel_vec)
    xel, yel = np.meshgrid(xel_vec, yel_vec)
    h_eff = np.sqrt((xel / lambda_x) ** 2 + (yel / lambda_y) ** 2)

    # %% Define prior statistics:
    X = np.ones((nyel, nxel))
    X = np.ravel(X, 1)

    # %% Define embedded domain coordinates (distances):
    xele_vec = np.arange(0, nxele, 1, dtype='float') * el_xlen
    yele_vec = np.arange(0, nyele, 1, dtype='float') * el_ylen
    xele, yele = np.meshgrid(xele_vec, yele_vec)
    xele = np.minimum(xele, domain_xlene - xele)
    yele = np.minimum(yele, domain_ylene - yele)
    h_effe = np.sqrt((xele / lambda_x) ** 2 + (yele / lambda_y) ** 2)

    # %% Define original and embedded covariance matrix and its FFT2:
    if Y_model == 'exponential':
        # Qs = Y_var * np.exp(-h_eff)
        Qse = Y_var * np.exp(-h_effe)

    elif Y_model == 'gaussian':
        # Qs = Y_var * np.exp(-h_eff ** 2)
        Qse = Y_var * np.exp(-h_effe ** 2)

    elif Y_model == 'spherical':
        Qs = Y_var * (1 - 1.5 * h_eff + 0.5 * h_eff ** 3)
        Qs[np.where(Qs > 1)] = 0
        Qse = Y_var * (1 - 1.5 * h_effe + 0.5 * h_effe ** 3)
        Qse[np.where(Qse > 1)] = 0

    FFTQse = np.fft.fftn(np.reshape(Qse, (nyele, nxele))).real
    lamb = FFTQse / nele

    epsilon = np.random.standard_normal((FFTQse.shape[0], FFTQse.shape[1])) + np.random.standard_normal(
        (FFTQse.shape[0], FFTQse.shape[1])) * 1j
    Y_bar = epsilon * np.sqrt(lamb)

    Ye = np.fft.ifftn(Y_bar).real * nele
    Y = Ye[0:nyel, 0:nxel]
    Y_tecplot = np.exp(np.reshape(Y, nel)) * X * beta_Y
    # Y_true = np.reshape(Y_tecplot, (nyel, nxel))  # todo: to comment all matplotlib for the cluster
    print('Time to generate 2d field with %d elements:: %s seconds' % (nel, time.clock() - start_time))
    # if plot is True:
    #     plt.figure(1)
    #     plt.pcolormesh(xel, yel, Y_true, shading='flat')
    #     plt.xlabel('X [m]')
    #     plt.ylabel('Y [m]')
    #     plt.colorbar()
    #     plt.title('Gaussian K field')
    #     plt.savefig('2D_K_Figure.png', bbox_inches='tight')
    #
    #     plt.figure(2)
    #     plt.pcolormesh(xel, yel, np.log(Y_true), shading='flat')
    #     plt.xlabel('X [m]')
    #     plt.ylabel('Y [m]')
    #     plt.colorbar()
    #     plt.title('Gaussian lnK field')
    #     plt.savefig('2D_lnK_Figure.png', bbox_inches='tight')
    #
    #     plt.figure(3)
    #     plt.pcolormesh(xel, yel, np.log10(Y_true), shading='flat')
    #     plt.xlabel('X [m]')
    #     plt.ylabel('Y [m]')
    #     plt.colorbar()
    #     plt.title('Gaussian $log_{10}$K field')
    #     plt.savefig('2D_log10K_Figure.png', bbox_inches='tight')
    #
    #     plt.show()
    #     plt.close()

    return Y_tecplot    # K in m/s


def genGaussParams(model_dir, filename, noParam):

    stats_values = np.loadtxt(os.path.join(model_dir, filename))
    if stats_values.ndim == 1:
        stats_values = stats_values[np.newaxis, ...]
    params = np.empty((stats_values.shape[0], noParam))

    for ii in range(0, stats_values.shape[0]):
        mymean = stats_values[ii, 0]
        myvar = stats_values[ii, 1]
        mean = np.ones((noParam,)) * np.log(mymean)
        R = ssp.dia_matrix((np.multiply(np.eye(noParam, noParam), myvar)))  # sparse Matrix
        params[ii, :] = np.random.multivariate_normal(mean, R.toarray())
        # params[ii, :] = np.log(stats_values[ii, 1]) * np.random.randn(noParam) + np.log(stats_values[ii, 0])

    return np.exp(params)


def gstat2realk(inputdata, coord, dist='lognormal', mymean=-6.2, mystd=0.5, plothist=True):
    """ Transform the simulation of Gstat into "real" K values. It should be possible use
        also in a parallel process.
    Arguments:
        inputdata:     array with (X,Y,Z value) input for Gstat (eg gstatGaus_XXXXX.dat)
        coord:         array with coordinates used for Gstat (X,Y,Z) (eg 'centroids_gstat.dat')
        dist:           str, 'lognormal' or 'Weibulinv' (only for a mean value of 3.0e-3m/s)
        mymean:         float, mean value for the chosen distribution. Default value for lognormal: log_K_Mean = -6.2
        mystd:          float, std value for the chosen distribution. Default value for lognormal: log_K_Std = 0.5
        plothist:           bool, activate plotting options or not
    Returns:
        outputdata:     transformed values. The values are ordered according to HGS structure.
    """
    print('Scaling GSTAT output to parameter space...')

    # %% Sort whole coordinates: first X, then Y,then Z keeping precision of Gstat simulation:
    coord_idx = np.arange(1, len(coord) + 1, 1)

    for i in [0, 1, 2]:
        sorted_idx = np.argsort(coord[:, i], kind='mergsort')

    # coord = coord[sorted_idx]
    coord_idx = coord_idx[sorted_idx]

    # %% Sort gstat values according to last column (element value)
    indices_gaus = np.argsort(inputdata[:, 3])
    inputdata = inputdata[indices_gaus]  # Sort table according to gaussian values

    # %% Scale the Normal Gaussian values from Gstat to values in the K parameter space:
    if dist == 'Weibulinv':
        print('Parameter values are mapped according to InvWeibul...\n')

        U = np.random.rand(inputdata.shape[0])
        K = mystats.invVertWeib(44320127996.4, 4.2896256, 0, U)

        # if plothist is True:
        #     plt.hist(K, bins=20, normed=True)
        #     plt.show()

    elif dist == 'lognormal':
        print('Parameter values are being mapped according to Log Normal ...\n')

        # Generate normal random numbers using mean and variance form Lessoff and calculate standard ranks:
        normdist = np.random.lognormal(mymean, mystd, len(inputdata[:, -1]))
        quantiles_normdist = 1. / (len(normdist))
        ranks_normdist = np.arange(0, 1.0 + quantiles_normdist, quantiles_normdist)
        stdranks_normdist = ranks_normdist[1:, ] - (0.5 * quantiles_normdist)
        stdranks_normdist = np.concatenate((np.array([0.0]), stdranks_normdist, np.array([1.])))

        # if plothist is True:
        #     plt.plot(normdist, stdranks_normdist[1:-1], 'b.')
        #     plt.hist(stdranks_normdist, bins=20, cumulative=True, color='green', alpha=0.5, normed=True)
        #     plt.show()

        # Calculate standard ranks of the gstat output
        quantilerange = 1.0 / (len(inputdata))
        ranks = np.arange(0, 1.0 + quantilerange, quantilerange)
        stdranks_gstat = ranks[1:, ] - (0.5 * quantilerange)
        stdranks_gstat = np.concatenate((np.array([0.0]), stdranks_gstat, np.array([1.])))
        K = normdist

        # if plothist is True:
        #     plt.hist(stdranks_gstat, bins=20, cumulative=True, color='green', alpha=0.5, normed=True)
        #     plt.show()
        #     plt.close()

    # %% Assign to the gstat norm values the values on K-space obtained with the random generator:
    inputdata[:, -1] = K  # Substitute the last column for K-values

    # %% Back sort with respect the number of elements
    for i in [0, 1, 2]:
        sort_ind_gstat = np.argsort(inputdata[:, i], kind='mergesort')

    inputdata = inputdata[sort_ind_gstat]
    back_sort_elements = np.argsort(coord_idx, kind='mergesort')
    # coord_idx = coord_idx[back_sort_elements]
    outputdata = inputdata[back_sort_elements]

    # # %% Saving the KKK-file for HGS:
    # np.savetxt(output_file,
    #            np.transpose((coord_idx, inputdata[:, -1], inputdata[:, -1], inputdata[:, -1])),
    #            fmt='%d %.10e %.10e %.10e')

    return outputdata

# The rest not really useful anymore...
# def gstatControlFile(inputFile, outputFile, instructionFile, Nugget, NumberModels, Models, c, a, rand_seed):
#     """ Generate the control file for Gstat. Contains variogram parameters.
#         (equivalent to the 00_generate_GstatFile.py script)
#     Arguments:
#         inputFile:      Str, name of file that contains grid information (X,Y,Z) (full path!!!)
#         outputFile:     Str, name of file to be generated by Gstat after each simulation (full path)
#         instructionFile: Str, txt file name for gstat, will contain the simulated values (full path!!!))
#         Nugget:         Float, variogram nugget (eg 0.050)
#         NumberModels:   Int, if a combination of two or more variograms is wanted (eg 2)
#         Models:         Str numpy array with the variogram models (gstat syntax, ie np.array(['Sph', 'Gau']))
#         c:              Float numpy array, vertical(variance) scaling factor or partial sill in transitive models
#                         ( c(1-Model(a)) ) (np.array([1.50, 1.50]))
#         a:              Float numpy array, range (horizontal, distance scaling factor) of the simple model
#                         (e.g. np.array([10.0, 1.0]))
#         rand_seed:      Int, random seed number to simulate independant fields
#                         (e.g. rand_seed = np.round(np.random.rand() * 1000 + 1))
#     Returns:
#         Generates the 'Gaussian_sim.txt' file that will be used by Gstat to simulate a field.
#     """
#     # %% Defined Gstat parameters:
#     sk_mean = 0.0  # simple kriging mean
#     radius = 150.0  # select obs in a local neighbourhood when they are within a distance of radius
#     maxPoints = 100  # maximum number of observations in a local neighbourhood selection
#     minPoints = 10  # minimum number of observations in a local neighbourhood selection
#     fileName = 'Gaussian_sim.txt'  # File name to be executed by Gstat
#     method = np.array(['gs'])  # gs = gaussian simulation
#     n_sim = 1  # Perform only one simulation
#
#     # %% Start writing the file:
#     fobj = open(instructionFile, 'w')
#     fobj.write('# Gaussian simulation, conditional upon data            #\n')
#     fobj.write('# (local neighbourhoods, simple and ordinary kriging)   #\n')
#     fobj.write('#########################################################\n\n')
#
#     fobj.write(
#         'data(alpha): dummy, sk_mean=%1.5f,radius=%6.3f,max=%d,min=%d;\n' % (sk_mean, radius, maxPoints, minPoints))
#
#     fobj.write('variogram(alpha): %5.3f Nug(0.00) ' % (Nugget))
#
#     for ii in range(0, NumberModels, 1):
#         fobj.write('+ ')
#         fobj.write('%s %s(%6.3f)' % (c[ii], Models[ii], a[ii]))
#     fobj.write(';\n')
#
#     fobj.write('\n # gaussian simulation = gs\n')
#     fobj.write('method: %s;\n' % (method[0]))
#
#     fobj.write('data(): \'' + inputFile + '\', x=1, y=2, z=3;\n\n')
#     fobj.write('set format=\'%.10g\';\n')
#     fobj.write('set output: \'' + outputFile + '\';\n\n')
#
#     n_uk = 20  # use ordinary kriging when > 20 data in neighbouhood
#     fobj.write('# use ordinary kriging when > 20 data in neighbourhood\n')
#     fobj.write('set n_uk = %d;\n\n' % (n_uk))
#
#     fobj.write('# missing values are written as blank space = \' \' \n')
#     fobj.write('set mv= \' \';\n\n')
#
#     fobj.write('## Number of simulations(are written in one output file [x y z k k k k.....]\n')
#     fobj.write('set nsim=%d;\n\n' % (n_sim))
#     fobj.write('set seed=%d;\n' % (rand_seed))
#
#     fobj.close()
#
#
# def gstatBatch(nFieldsToSimulate, myBatchFileGstat, myCodeDir, myBatchDir, k_file, MonteCarlo, InternalLoop,
#                gstat2real):
#     """ Create a batch file to run Gstat X number of times to generate X number of diferent
#         Gaussian parameter fields, sequentially, not in Parallel.
#     Arguments:
#         nFieldsToSimulate:      Int, number of realizations
#         myBatchFileGstat:       Str, name of the batch file to be generated (full path)
#         k_file:                 Str, name of the file with the current parameter values, give full path(eg kkk_synthetic_Gaus.dat)
#         MonteCarlo:             Boolean, True: MonteCarlo approach, False: single realization
#         InternalLoop:           Boolean, True:loop defined in batch file, False: loop defined in python
#         gstat2real:             Str, full path of the python script to convert Gstat output to real K values
#
#             Note: The gstat2real string should call a python script that calls the function "gstat2realK"
#
#     Return: A batch file to generate X number of realizations, sequentially.
#     """
#
#     # %% Generate the strings for each KKK file to be created
#     # and create necessary variables:
#     k_fileStrip = np.char.strip(k_file, '.dat')
#     gstatfile = 'Gaussian_sim.txt'
#     time_between_sim = 12
#
#     # %% Start writting the file:
#     if all(MonteCarlo == False, InternalLoop == False):
#         nFieldsToSimulate = 1
#         fobj = open(myBatchFileGstat, 'w')
#         fobj.write('@echo off')
#         fobj.write('\n\ngstat.exe %s\n' % gstatfile)
#         fobj.write('python %s' % (gstat2real))
#         fobj.close()
#
#     if all(InternalLoop == True, MonteCarlo == True):
#         fobj = open(myBatchFileGstat, 'w')
#         fobj.write('@echo off')
#         fobj.write('\n\nfor /L %s in (1,1,%d) do (' % ('%%i', nFieldsToSimulate))
#         fobj.write('\n\ngstat.exe %s\n' % (gstatfile))
#         fobj.write('python %s\n\n' % (gstat2real))
#         fobj.write('copy %s %s_%s.dat > nul' % (k_file, k_fileStrip, '%%%i'))
#         fobj.write('\n\n')
#         fobj.write('del %s > nul\n\n' % (k_file))
#         fobj.write('timeout /t %d)' % time_between_sim)
#         fobj.close()
#
#     else:
#         ValueError('Check MonteCarlo and InternalLoop variables')
#         raise
#
#
# def HGSBatchFile(nFieldsToSimulate):  # Needs to be rewritten!!!
#     # Defining working directories:
#     workingDir = os.path.dirname(__file__)  # Path where the code is located
#     myCodesDirectory = os.path.abspath(os.path.join(workingDir, '..', '_codes'))
#     myGstatDirectory = os.path.abspath(os.path.join(workingDir, '..', 'Gstat'))
#     myHGSDirectory = os.path.abspath(os.path.join(workingDir, '..', 'HGS'))
#     MonteCarloFolder = 'MonteCarloHGS'
#
#     # code_gstat2real = '04_Gstat_2_actualk_DEV.py'
#     GstatBatch = "gstat_multrealiz.bat"
#     k_file = 'kkk_synthetic_Gaus.dat'
#     k_fileStrip = np.char.strip(k_file, '.dat')
#
#     gstatfile = 'Gaussian_sim.txt'
#
#     # nFieldsToSimulate = 500
#     startAtNumber = 0
#     time_between_sim = 12
#
#     # Create the batch file for running HGS with all the different realizations:
#
#     HGSbatchFile = 'letmerunPrecalSim.bat'
#
#     fobj_HGS = open(os.path.join(myHGSDirectory, HGSbatchFile), 'w')
#     fobj_HGS.write('@echo off')
#     fobj_HGS.write("\n")
#     fobj_HGS.write("\n")
#
#     fobj_HGS.write('for /L %s in (1,1,%d) do (' % ('%%i', nFieldsToSimulate))
#     fobj_HGS.write('\n')
#     fobj_HGS.write('del kkk_file.dat > nul')
#     fobj_HGS.write('\n')
#     fobj_HGS.write(
#         'copy %s\%s_%s.dat kkk_file.dat' % (os.path.join(myHGSDirectory, MonteCarloFolder), k_fileStrip, '%%i'))
#     fobj_HGS.write('\n')
#
#     fobj_HGS.write('grok')
#     fobj_HGS.write('\n')
#     fobj_HGS.write('phgs')
#     fobj_HGS.write('\n')
#
#     fobj_HGS.write('python  %s' % (workingDir + '\8_headsout_file_cal_transient.py'))
#     fobj_HGS.write('\n')
#     fobj_HGS.write('copy modeledheads.dat modeledheads_%s.dat' % ('%%i'))
#     fobj_HGS.write('\n')
#     fobj_HGS.write('del modeledheads.dat > nul)')
#     fobj_HGS.close()
#
#     print('\nHello! I am ALSO done with the batch file for running %d HGS simulations!' % (nFieldsToSimulate))
