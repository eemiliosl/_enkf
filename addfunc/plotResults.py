""" Functions to Manipulate directories """
import shutil
import os
import numpy as np
import re
import time
import pdb
import matplotlib as mpl
import matplotlib.pylab as plt
mpl.use('Agg')


def plotimshow(X, elY, elX, points=np.array([]), title='', cbtitle='', savefile='', minval=0., maxval=1., xlim=None,
               ylim=None, xticks=None, yticks=None):
    if xticks is None:
        xticks = []
    fig = plt.figure(1)
    ax1 = fig.add_subplot(111)
    ax1.set_title(title)
    ax1.tick_params(axis='both', which='major', labelsize='x-small')
    ax1.set_xlabel('X coordinate', fontsize='x-small')
    ax1.set_ylabel('Y coordinate', fontsize='x-small')
    if xlim:
        ax1.set_xlim(xlim)
    if ylim:
        ax1.set_ylim(ylim)
    if xticks:
        ax1.set_xticklabels(xticks)
    if yticks:
        ax1.set_yticklabels(yticks)

    im1 = ax1.imshow(X.reshape(elY, elX), interpolation='nearest', cmap=plt.get_cmap('jet'))
    # im1 = ax1.imshow(np.flipud(X.reshape(elY, elX)), interpolation='nearest')
    if len(points) > 0:
        ax1.plot(points[:, 0], points[:, 1], 'ko', ms=2)

    norm = mpl.colors.Normalize(vmin=minval, vmax=maxval)
    im1.set_norm(norm)
    cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
    cb1.set_label(cbtitle, fontsize='x-small')
    if savefile:
        plt.savefig(savefile, transparent=False, dpi=600)
    plt.close()


def plotimshow_slices(X, elY, elX, points=np.array([]), title='', cbtitle='', savefile='', minval=0., maxval=1., xlim=None,
               ylim=None, xticks=None, yticks=None, shape=111):
    # from mpl_toolkits.axes_grid1 import ImageGrid
    if xticks is None:
        xticks = []
    if len(str(shape)) > 3:
        nolayers = str(shape)[-2:]
    else:
        nolayers = str(shape)[-1]
    fig = plt.figure(1)
    # grid = ImageGrid(fig, shape,  # as in plt.subplot(111)
    #                  nrows_ncols=(int(str(shape)[0]), int(str(shape)[1])),
    #                  axes_pad=0.15,
    #                  share_all=True,
    #                  cbar_location="bottom",
    #                  cbar_mode="single",
    #                  cbar_size="7%",
    #                  cbar_pad=0.15,
    #                  )
    fig.suptitle(title, fontsize='x-small')
    for ii in range(0, int(nolayers)):
        X_new = X[:, :, ii]
        str_shape = str(shape)
        str_shape = '%s%s%s' % (str_shape[0], str_shape[1], str(ii+1))

        ax1 = fig.add_subplot(int(str_shape[0]), int(str_shape[1]), (ii+1))
        # ax1 = grid[ii]
        ax1.set_title('Elevation: %s [m]' % ii, fontsize=6)
        ax1.tick_params(axis='both', which='major', labelsize=4)
        # ax1.set_xlabel('X coordinate', fontsize=5)
        # ax1.set_ylabel('Y coordinate', fontsize=5)
        if xlim:
            ax1.set_xlim(xlim)
        if ylim:
            ax1.set_ylim(ylim)
        if xticks:
            ax1.set_xticklabels(xticks)
        if yticks:
            ax1.set_yticklabels(yticks)

        im1 = ax1.imshow(X_new.reshape(elY, elX), interpolation='nearest', cmap=plt.get_cmap('jet'))
        # im1 = ax1.imshow(np.flipud(X.reshape(elY, elX)), interpolation='nearest')
        if len(points) > 0:
            ax1.plot(points[:, 0], points[:, 1], 'ko', ms=2)

        norm = mpl.colors.Normalize(vmin=minval, vmax=maxval)
        im1.set_norm(norm)

    fig.text(0.5, 0.04, 'X coordinate', fontsize='small', ha='center')
    fig.text(0.04, 0.5, 'Y coordinate', fontsize='small', va='center', rotation='vertical')
    fig.subplots_adjust(right=0.8, wspace=0.2, hspace=0.4)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    # cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
    cb1 = fig.colorbar(im1, orientation='vertical', cax=cbar_ax)
    cb1.set_label(cbtitle, fontsize='small')
    cb1.ax.tick_params(labelsize=5)
    # plt.tight_layout()
    if savefile:
        plt.savefig(savefile, transparent=False, dpi=600)
        plt.savefig('%s.eps' % savefile[:-3], transparent=False, dpi=600, format='eps')
    plt.close()


def plothist(X, title='', label='', color='gray', norm=False, cum=False, textbox='', xlabel='', savefile='', eps=False):
    fig = plt.figure()
    plt.title(title)
    plt.hist(X, histtype='step', normed=norm, color=color, alpha=0.5, bins=16, cumulative=cum, label=label)
    if textbox:
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        fig.text(0.2, 0.8, textbox, fontsize=11, verticalalignment='top', bbox=props)
    plt.grid()
    plt.legend(loc='best', shadow=True)
    if xlabel:
        plt.xlabel(xlabel)
    if savefile:
        plt.savefig(savefile, dpi=600, transparent=True, format='png')
    if eps is True:
        plt.savefig('%s.eps' % savefile[:-4], dpi=600, format='eps')
    plt.close()


def plothist_ensemble(X, X_orig=np.array([]), title='', norm=False, cum=False, textbox='', xlabel='', savefile='',
                      eps=False):
    fig = plt.figure()
    plt.title(title)
    for zz in np.arange(0, X.shape[-1]):
        plt.hist(X[:, zz], histtype='step', normed=norm, color='gray', alpha=0.5, bins=16, cumulative=cum)
    plt.hist(X.mean(axis=1), histtype='step', color='k', label='Ensemble mean', normed=False, bins=16,
             cumulative=cum)
    if len(X_orig) > 0:
        plt.hist(X_orig, histtype='step', normed=norm, color='r', label='Synthetic', bins=16, cumulative=cum)
    if textbox:
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        fig.text(0.2, 0.8, textbox, fontsize=11, verticalalignment='top', bbox=props)
    plt.grid()
    plt.legend(loc='best', shadow=True)
    plt.xlabel(xlabel)
    if savefile:
        plt.savefig(savefile, dpi=600, transparent=True, format='png')
    if eps is True:
        plt.savefig('%s.eps' % savefile[:-4], dpi=600, format='eps')
    plt.close()


def xyplot(X, Y, title='', textbox='', label='', xlabel='', ylabel='', savefile='', eps=False):
    fig = plt.figure()
    plt.title(title)
    plt.plot(X, Y, 'k8-', label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()
    plt.legend(loc='best', shadow=True)
    if textbox:
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        fig.text(0.2, 0.8, textbox, fontsize=11, verticalalignment='top', bbox=props)
    if savefile:
        plt.savefig(savefile, dpi=600, format='png')
    if eps is True:
        plt.savefig('%s.eps' % savefile[:-4], dpi=600, format='eps')
    plt.close()


def xy_percentileplot(X, Y_ens, ci=None, Y_real=None, median=True, title='', xlabel='', ylabel='', savefile='', eps=False):
    """ Plot uncertainty of ensemble outputs, using the Percentile 50 (median) and the two ci percentiles given. 
        Arguments:
    X:          np.array, X values of the XY plot
    Y_ens:      np.array, Y values of th ensemble. Dims= Length of X x Length of ensemble
    ci:         tuple or list, lower and upper percentile defining confidence intervals
    Y_real:     np.array, reference Y values. Optional, Default = None
    median:     bool, plot also the median of the ensemble or not. Default is True
    title:      str, title of the plot. Optional    
    xlabel:     str, x axis label. Optional
    ylabel:     str, y axis label. Optional
    savefile:   str,twin if provided it stores the plot in the given path/name. Optional
    eps:        bool, stores additionally an eps file. Default is False
        Return:
    If given, the plot file in png and (if True) eps format(s).
    """

    assert ci is not None, 'Bounds of confidence intervals not provided!'

    # Estimate percentiles from ensemble data:
    P_1 = np.percentile(Y_ens, axis=0, q=ci[0])
    P_2 = np.percentile(Y_ens, axis=0, q=ci[1])

    fig, axs = plt.subplots()
    axs.set_facecolor('gainsboro')
    plt.title(title)
    # plt.title(cur_well.astype(np.str))
    # axs.plot(X, fielddata[:, well_id], 'k8-', label='Synthetic Data')
    if Y_real is not None:
        axs.plot(X, Y_real, 'k8-', label='Synthetic Data')
    # axs.plot(outputtimes, meanheads, c='b', ls='dashed', marker='s', label='Ensemble median')
    if median is True:
        P50_median = np.percentile(Y_ens, axis=0, q=50)
        axs.plot(X, P50_median, c='b', ls='dashed', marker='x', label='Ensemble Median')

    axs.fill_between(X, P_1, P_2, facecolor='gray', alpha=0.8, linestyle='dashdot', antialiased=True,
                     label='%s%% conf.interval' % (ci[1]-ci[0]))
    axs.grid(True)
    # axs.set_ylabel(ylabel)
    # axs.set_xlabel(xlabel)
    # myleg = axs.legend(loc='best', shadow=True, numpoints=1, fontsize=14)
    # get the individual lines inside legend and set line width
    # for line in myleg.get_lines():
    #     line.set_linewidth(2)
    #myleg = axs.legend(loc='best', shadow=True, numpoints=1, fontsize=14)
    # get the individual lines inside legend and set line width
    #    line.set_linewidth(2)

    if savefile:
        plt.savefig(savefile, dpi=600, format='png')
    if eps is True:
        plt.savefig('%s.eps' % savefile[:-4], dpi=600, format='eps')
    plt.close()