"""
Functions to deal with GROK and HGS
Created on Wed Mar 25 20:39:31 2015
@author: Emilio Sanchez-Leon
"""
import sys
import os
import numpy as np
import datetime
import shutil
import subprocess as sp
import platform
import multiprocessing as mp
import time
import scipy
from itertools import repeat
from statistics import mean
import scipy.sparse as ssp
import _enkf.addfunc.fieldgen as fg
import _enkf.addfunc.dirs as dm
import _enkf.addfunc.gridmanip as gm
import _enkf.addfunc.mystats as mysst
import _enkf.addfunc.kalman as kf


def GenReal(str2pass):
    """ Run realizations with the possibility of doing it in parallel """
    # Create variables with the proper part of the string
    import re
    Str2Assign, homedir, mode, Ini_ensemble, genfields, grid_filedir, \
    initial_head, mytype, str_assim, \
    neg2zero, head2dwdn, cumbtc, norm, moments, porosity, storativity,\
    dispersivity, subgrid, spinup, hgs = list2var(str2pass)

    if subgrid is True:
        xlen, ylen, zlen, nxel, nyel, nzel, \
        xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, \
        lambda_y, lambda_z = fg.readParam(os.path.join(homedir, 'model_data', '_RandFieldsParameters.dat'))
        if os.path.isfile(os.path.join(os.path.split(grid_filedir)[0], 'subGrid.elements.npy')) is False:
            print('Gathering subgrid elements...')
            if re.match('[0-9]{4}',hgs):       # eg. hgs = 2014 --> gridformat = tp2014
                gridformat = 'tp' + hgs
            else:                              # what else: maybe hgs=gms? Since gridformat = gms is allowed
                gridformat = hgs
            subgridnodes, subgridelem = gm.sel_grid(grid_filedir, xlim, ylim, zlim, store=True, gridformat=gridformat)
            subgridnodes = None
        else:
            subgridelem = np.load(os.path.join(os.path.split(grid_filedir)[0], 'subGrid.elements.npy'))
            subgridnodes = None
    # %%
    try:
        NoReal = np.asarray(Str2Assign).astype('int') - 1
    except:
        NoReal = np.asarray(Str2Assign).astype('str')

    # %% Some headers for multiprocessing module:
    scipy.random.seed()
    time.sleep(np.random.uniform(low=1.0, high=1.5, size=1))  # Process_created = mp.Process()
    Process_current = mp.current_process()
    print('Process No. %s started by %s...' % (Str2Assign, Process_current.name))

    # %%  mesh data:
    meastimes = '_outputtimes%s.dat' % mode
    modeltimes = np.loadtxt(os.path.join(homedir, 'model_data', meastimes))

    # Generate subgridData if decided to work with a subset:
    # if subgrid is True:
    #     subgridNodes, subgridElements = gm.sel_grid(grid_filedir, xlim, ylim, zlim, store=True)

    nnodes = int(dm.str_infile(grid_filedir, 'N='))

    # %% Run function to get states:
    modmeas, Y = getstates(homedir, NoReal, str_assim, nnodes, mode, modeltimes, Y_i_upd='', curtimestep=0,
                                      genfield=genfields, plot=False, mytype=mytype,
                                      initial_head=initial_head,
                                      neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments,
                                      Ini_ensemble=Ini_ensemble, porosity=porosity, storativity=storativity,
                                      dispersivity=dispersivity, subgrid=subgrid, subgridnodes=subgridnodes,
                                      subgridelem=subgridelem, spinup=spinup, hgs=hgs)

    print('Exiting:', Process_current.name)
    # If initialization of ensemble, return only the index
    if Ini_ensemble is True:
        return Str2Assign
    # If not initialization of ensemble, return also model outputs and parameters
    elif Ini_ensemble is False:
        return Str2Assign, modmeas, Y


def list2var(str2pass):
    """
    Change a composite list to several variables using '-' as a symbol to
    separate the strings...
    """
    Str2Assign = str2pass.split('-')[0]
    homedir = str2pass.split('-')[1]
    mode = str2pass.split('-')[2]
    Ini_ensemble = str2bool(str2pass.split('-')[3])
    genfields = str2bool(str2pass.split('-')[4])
    grid_filedir = str2pass.split('-')[5]
    initial_head = str2pass.split('-')[6]
    mytype = str2pass.split('-')[7]
    str_assim = str2pass.split('-')[8]
    neg2zero = str2bool(str2pass.split('-')[9])
    head2dwdn = str2bool(str2pass.split('-')[10])
    cumbtc = str2bool(str2pass.split('-')[11])
    norm = str2bool(str2pass.split('-')[12])
    moments = str2bool(str2pass.split('-')[13])
    porosity = str2bool(str2pass.split('-')[14])
    storativity = str2bool(str2pass.split('-')[15])
    dispersivity = str2bool(str2pass.split('-')[16])
    subgrid = str2bool(str2pass.split('-')[17])
    spinup = str2bool(str2pass.split('-')[18])
    hgs = str2pass.split('-')[19]
    # subgridelem = str2pass.split('-')[18]

    if (initial_head == 'False') or (initial_head == 'None'):
        initial_head = str2bool(initial_head)
    else:
        initial_head = float(initial_head)

    return Str2Assign, homedir, mode, Ini_ensemble, genfields, grid_filedir, initial_head, mytype, \
           str_assim, neg2zero, head2dwdn, cumbtc, norm, moments, porosity, storativity, dispersivity, \
           subgrid, spinup, hgs


def caller_getstates(Y, homedir, str_assim, nnodes, mymode, fieldmeas_len, current_time, thetimes, type_update,
                     parallel=False, cpus='', genfield=False, plot=False, initial_head=7.0,
                     neg2zero=False, head2dwdn=False, cumbtc=False, norm=False, moments=False, OWnode='',
                     porosity=False, storativity=False, dispersivity=False,
                     subgrid=False, subgridnodes=None, subgridelem=None, hgs='2016'):
    """
    Support function to call getstates with the option of doing it in parallel
    Arguments:
        Y:              numpy 2D array, parameter array for all realizations
        homedir:        str, home directory (e.g. kalmanfilter_git)
        mymode:         str, 'fl_ or 'tr_'
        fieldmeas_len:  int, number of field observations (
        str_assim:
        nnodes:
        current_time:
        thetimes:
        type_update:
        parallel:
        cpus:
        genfield:
        plot:
        initial_head:
        neg2zero:
        head2dwdn:
        cumbtc:
        norm:
        moments:
        OWnode:
        porosity:
        storativity:
        dispersivity:
        subgrid:
        subgridnodes:
        subgridelem:
    Returns:

    """
    nel, nreal = Y.shape
    modmeas = np.zeros((fieldmeas_len, nreal))
    if parallel is False:
        for ii in range(0, nreal):
            modmeas_temp, idx_dummy = getstates_parall(ii, Y, homedir, str_assim, nnodes,
                                                       mymode, current_time, thetimes, type_update,
                                                       genfield, plot, initial_head, neg2zero, head2dwdn,
                                                       cumbtc, norm, moments, OWnode, porosity, storativity,
                                                       dispersivity, subgrid, subgridnodes, subgridelem, hgs)
            # if (len(modmeas_temp) != fieldmeas_len) or (len(modmeas_temp) == 0):
            #     modmeas[:, ii] = np.zeros(fieldmeas_len, )
            # else:
            modmeas[:, ii] = modmeas_temp

    elif parallel is True:
        mypool = mp.Pool(cpus)
        modmeas_temp = mypool.starmap(getstates_parall,
                                      zip(np.arange(0, nreal, 1), repeat(Y), repeat(homedir), repeat(str_assim),
                                          repeat(nnodes),
                                          repeat(mymode), repeat(current_time), repeat(thetimes), repeat(type_update),
                                          repeat(genfield), repeat(plot), repeat(initial_head),
                                          repeat(neg2zero), repeat(head2dwdn), repeat(cumbtc), repeat(norm),
                                          repeat(moments), repeat(OWnode), repeat(porosity), repeat(storativity),
                                          repeat(dispersivity), repeat(subgrid), repeat(subgridnodes),
                                          repeat(subgridelem), repeat(hgs)))
        mypool.close()
        mypool.join()

        for ii in range(0, len(modmeas_temp)):
            # if (len(modmeas_temp[ii][0]) != fieldmeas_len) or (len(modmeas_temp[ii][0]) == 0):
            #     modmeas[:, modmeas_temp[ii][-1]] = np.zeros(fieldmeas_len, )
            # else:
            modmeas[:, modmeas_temp[ii][-1]] = np.asarray(modmeas_temp[ii][0])

    return modmeas


def getstates_parall(ii, Y, homedir, str_assim, nnodes, mymode, current_time, thetimes, type_update, genfield, plot,
                     initial_head, neg2zero, head2dwdn, cumbtc, norm, moments, OWnode, porosity, storativity,
                     dispersivity, subgrid, subgridnodes, subgridelem, hgs):
    """ Function that calls getstates for the corresponding member of the ensemble. Prepares the
        cur_process folder, and add updates the time in the grok file.
    Arguments:
        initial_head:
        storebinary:
        plot:
        genfield:
        ii:
        Y:
        homedir:
        mymode:
        current_time:
        thetimes:
        type_update:
        moments:
    Returns:
        modeled observations and index of the current member of the ensemble
    """
    mypath_dict = dm.gendirtree(homedir, str_assim=str_assim, proc_idx=ii + 1, mode=mymode)
    concFile = '%ssim_%.5do.cen_prev' % (mymode, (ii + 1))

    if not os.listdir(mypath_dict['fld_mode']):
        prepfwd(ii, homedir, mymode, current_time, thetimes, str_assim=str_assim,
                type_update=type_update, concFile=concFile)

    # addt2grok(mypath_dict, current_time, thetimes, mode=mymode, mytype=type_update)
    if not isinstance(initial_head, (float, str)):
        initial_head = None  # initial_head[:, ii]

    modmeas, Y = getstates(homedir, ii, str_assim, nnodes, mymode, thetimes, Y_i_upd=np.exp(Y[:, ii]),
                                      curtimestep=current_time,
                                      genfield=genfield, plot=plot, mytype=type_update,
                                      initial_head=initial_head, neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc,
                                      norm=norm,
                                      moments=moments, OWnode=OWnode, porosity=porosity, storativity=storativity,
                                      dispersivity=dispersivity,
                                      subgrid=subgrid, subgridnodes=subgridnodes,
                                      subgridelem=subgridelem, hgs=hgs)
    # If there are no heads file, store them first individually:
    # if mymode != 'tr_':
    #     if (type_update == 'std') and (os.path.isfile(
    #             os.path.join(mypath_dict['fld_heads'], 'hydheads_%siter001_timestep_001.npy' % mymode)) is False):
    #         np.save(os.path.join(mypath_dict['fld_heads'], headsFile_str), headfield)

    return modmeas, ii


def getstates(homedir, ii, str_assim, nnodes, mode, modeltimes, Y_i_upd='', curtimestep=0, genfield=False, plot=False,
              mytype='restart', initial_head=7.0, neg2zero=False, head2dwdn=False,
              cumbtc=False, norm=False, moments=False, Ini_ensemble=False, OWnode='', porosity=False,
              storativity=False, dispersivity=False, subgrid=False, meshfile='meshtecplot.dat',
              subgridnodes=None, subgridelem=None, spinup=False, hgs='2016'):
    """
    Update states(model measurements) with updated parameters using a forward model
    run for the corresponding realization. Give the parameter values NOT in LOG SCALE!!!
        Arguments:
        ----------
    homedir:
    ii:                 int, process index
    mode:               str, type of model run. 'fl_' or 'tr_'
    Y_i_upd:            np.array, parameter values of the model. Not in log scale!
    genfield:           bool, generate a gaussian field or not
    plot:               bool, plot the generated fields or not
    moments:
        Returns:
        --------
    modmeas:      np.array of updated modeled measurements (or states). Fortran order ([no_meas*no_obsPoints] x 1 )
    """
    # %% Initialize necessary variables and directories:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mode, homedir, str_assim=str_assim, porosity=porosity,
                                                         storativity=storativity, dispersivity=dispersivity)
    randparam = '_RandFieldsParameters.dat'
    Model_data_dir = dir_dict['fld_modeldata']
    cur_processpath = dir_dict['fld_curproc']
    randfile = os.path.join(Model_data_dir, randparam)
    ModObsPath = dir_dict['fld_modobs']
    dest_kkkFolderBinary = dir_dict['fld_kkkfiles']
    curDestDir = dir_dict['fld_mode']
    concFile = '%ssim_%so.cen_prev' % (mode, fmt_string)
    headFile = '%ssim_%so.hen_prev' % (mode, fmt_string)

    if not os.listdir(curDestDir):
        prepfwd(ii, homedir, mode, curtimestep, modeltimes, str_assim=str_assim,
                type_update=mytype, concFile=concFile, porosity=porosity,
                storativity=storativity, dispersivity=dispersivity)

    files2keep = np.array(
        ['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'iniheads', 'parallelindx', 'meshwell', '.png',
         '.lic', '.cen', '.hen'])
    assert os.path.exists(curDestDir), 'Something is wrong in the definition of the tree directory!'
    assert os.path.exists(Model_data_dir), 'Model data directory does not exist or not in proper location!'

    if (subgrid is True) and (subgridelem is None):
        subgridelem = np.load(os.path.join(homedir, mode + 'ToCopy', 'subGrid.elements.npy'))
        subgridnodes = None

    # %% First Step: Generate new kkk files to be used by GROK and HGS in the proper folder
    # ---------------------#
    # Decide whether to create random fields or not:
    # Load random field parameter data from the main file:
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(
        randfile)
    if genfield is True:

        # Generate the field: returns K in m/s.
        Y = fg.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel, Y_model,
                                Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot, mydir=curDestDir)
        if subgrid is True:
            nelements = int(dm.str_infile(os.path.join(homedir, mode + 'ToCopy', meshfile), 'E='))
            # Treat as a single mean value to the elements outside the subdomain
            if (nxel * nyel * nzel) < nelements:
                Y_temp = np.ones((nelements,)) * Y.mean()
                Y_temp[subgridelem] = Y
                np.savetxt(os.path.join(curDestDir, curKFile),
                           np.transpose((np.arange(1, len(Y_temp) + 1, 1), Y_temp.squeeze(), Y_temp.squeeze(),
                                         Y_temp.squeeze())), fmt='%d %.6e %.6e %.6e')
        if subgrid is False:
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(Y) + 1, 1), Y.squeeze(), Y.squeeze(),
                                     Y.squeeze())), fmt='%d %.6e %.6e %.6e')

    elif genfield is False:
        assert len(Y_i_upd) > 0, 'No parameters were provided!'
        # Save KKK file in the proper process folder:
        if subgrid is False:
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(Y_i_upd) + 1, 1), Y_i_upd.squeeze(), Y_i_upd.squeeze(),
                                     Y_i_upd.squeeze())), fmt='%d %.6e %.6e %.6e')
        elif subgrid is True:
            nelements = int(dm.str_infile(os.path.join(homedir, mode + 'ToCopy', meshfile), 'E='))
            # Treat as a single mean value to the elements outside the subdomain
            if (nxel * nyel * nzel) < nelements:
                Y_temp = np.ones((nelements,)) * np.exp(np.log(Y_i_upd).mean())
                Y_temp[subgridelem] = Y_i_upd.squeeze()
                np.savetxt(os.path.join(curDestDir, curKFile),
                           np.transpose((np.arange(1, len(Y_temp) + 1, 1), Y_temp.squeeze(), Y_temp.squeeze(),
                                         Y_temp.squeeze())), fmt='%d %.6e %.6e %.6e')

    # Read last porosity or storage or dispersivity file and update mprops if necessary:
    if Ini_ensemble is True:
        dummy1, dummy2 = read_extraparam(ii, dir_dict, mode, initialize=True, porosity=porosity,
                                         storativity=storativity,
                                         dispersivity=dispersivity, update_mprops=True)

    if (curtimestep == 0) and (mode == 'tr_'):
        dummy1, dummy2 = read_extraparam(ii, dir_dict, mode, initialize=True, porosity=porosity,
                                         storativity=storativity,
                                         dispersivity=dispersivity, update_mprops=True)

    # %% Second Step: Run the model: GROK and HGS :
    # ---------------------#
    # If spin-up first:
    if (mode == 'fl_') and (spinup is True):
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!IniTransSim\nTransient flow': '!IniTransSim\n!Transient flow',
                       '!IniNoWells\n!skip on': '!IniNoWells\nskip on'})
        print(time.strftime("%d-%m %H:%M:%S",time.localtime(time.time())) + ': Running spin-up for steady-state flow')
        fwdHGS(curDestDir, fmt_string, mytype=mode)
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!IniTransSim\n!Transient flow': '!IniTransSim\nTransient flow',
                       '!IniNoWells\nskip on': '!IniNoWells\n!skip on'})
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!Inihead\nInitial head\n10.0':'!Inihead\n!Initial head\n!10.0'})
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!restart file for heads\n!fl_simo.hen': 'restart file for heads\n%s' %headFile})

        dm.rmv_rename(os.path.join(dir_dict['fld_mode'], headFile),
                      os.path.join(dir_dict['fld_mode'], 'fl_sim_%so.hen' % fmt_string))
        print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Finish spin-up for steady-state flow')

    if (mode == 'fl_') and (isinstance(initial_head, (float, str)) is False):
        headfield, dummytime = readHGSbin(os.path.join(dir_dict['fld_mode'], headFile), nnodes, endfile=True)
        OWnode = np.loadtxt(os.path.join(Model_data_dir, '_obswellscoord%s.dat' % mode), usecols=(-1,))
        initial_head = headfield[OWnode.astype('int')-1]

    fwdHGS(curDestDir, fmt_string, mytype=mode)

    # Remove previous .hen_prev file if exists and rename the current .hen file:
    if mode == 'fl_':
        dm.rmv_rename(os.path.join(dir_dict['fld_mode'], headFile),
                      os.path.join(dir_dict['fld_mode'], 'fl_sim_%so.hen' % fmt_string))

    if (mytype == 'std') and (mode == 'tr_'):
        dm.rmv_rename(os.path.join(dir_dict['fld_mode'], concFile),
                      os.path.join(dir_dict['fld_mode'], 'tr_sim_%so.hen' % fmt_string))

    # %% Third step: Read new model outputs at specified output times, store them as bin files.
    # If running flow, extract in addition final heads:
    # ---------------------#
    if 'std' in mytype:
        mytimes = modeltimes[curtimestep][np.newaxis]
    elif 'restart' in mytype:
        mytimes = modeltimes[0:curtimestep + 1]
    elif 'full' in mytype:
        mytimes = modeltimes
    all_modout = getmodout(curDestDir, mode=mode, outputtime=mytimes, initial_head=initial_head,
                           neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments,
                           curtimestep=curtimestep, curreal=Noreal, dirtree=dir_dict, hgs=hgs)

    # %% Fourth: save stuff in the corresponding directories
    # Just if Ini_ensemble is True, if not return the values for concatenating in a single array and binary file:
    # ---------------------#
    if Ini_ensemble is True:
        np.save(os.path.join(ModObsPath, 'ini_mod%s%s' % (mode, fmt_string)), all_modout.transpose())
        np.save(os.path.abspath(os.path.join(dest_kkkFolderBinary, 'ini_%s' % curKFile.split('.')[0])), np.transpose(Y))

    # Fifth: Remove unnecessary files:
    # ---------------------#
    dummypaths = dm.rmfiles(files2keep, mode + fmt_string, cur_processpath)
    del dummypaths

    # Sixth: Reorder model outputs (states) prior to return them, using Fortran convention:
    # ---------------------#
    try:
        modmeas = all_modout.flatten('F')  # Fortran convention is always used in this step
    except:
        raise Exception('Something is wrong with the model outputs, check e.g. cur time step in grok!')

    return modmeas, Y_i_upd


def prepfwd(ii, homedir, mymode, cur_timestep, thetimes, headsFile='', str_assim='', type_update='', concFile='',
            porosity=False, storativity=False, dispersivity=False):
    """
    Prepare folder for forward model run. Meaning: copy the source folder with
    all its files, into a folder that includes process number id (number of realization)
        Arguments:
        ----------
    ii:             int, process id
    homedir:        str, home directory of the whole project
    mymode:         str, fl_ or tr_, defines flow or transport
    headsFile:      str, name of the final(from heads) or initial(for transport) heads
        Returns:
        --------
    Creates corresponding directories for a forward model run
    """

    # %% Create process folder if does not exist:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mymode, homedir, str_assim=str_assim,
                                                         porosity=porosity,
                                                         storativity=storativity, dispersivity=dispersivity)
    SourceDir = os.path.abspath(os.path.join(homedir, mymode + 'ToCopy'))
    # Copy the necessary files from ...ToCopy folder with updated filename:
    grokfile = cpSrcfiles(SourceDir, dir_dict['fld_mode'], mymode, fmt_string)

    # Replace necessary stuff in grok file:
    if type_update != 'std':  # Dictionary of files which name is to be updated:
        StrFiles2Replace = {'kkkGaus.dat': curKFile,
                            'Mesh to tecplot': '!Mesh to no tecplot',
                            'meshtecplot.dat': '!meshtecplot no .dat'}
    else:
        StrFiles2Replace = {'kkkGaus.dat': curKFile,
                            'tr_simo.cen': concFile,
                            'Mesh to tecplot': '!Mesh to no tecplot',
                            'meshtecplot.dat': '!meshtecplot no .dat'}

    changeStrGrok(os.path.join(dir_dict['fld_mode']), grokfile, StrFiles2Replace)

    if (type_update == 'std') and (cur_timestep != 0) and (mymode == 'tr_'):
        addstr2grok(os.path.join(dir_dict['fld_mode'], grokfile), startstr='!---Injection point',
                    endstr='!---End injection point', str2add=[''])
        changeStrGrok(os.path.join(dir_dict['fld_mode']), grokfile,
                      {'!restart file for concentrations': 'restart file for concentrations',
                       '!%s' % concFile: concFile})

    if (type_update == 'std') and (cur_timestep != 0) and (mymode == 'fl_'):
        add_head2grok(os.path.join(dir_dict['fld_mode'], grokfile), headsFile)
        addstr2grok(os.path.join(dir_dict['fld_mode'], grokfile), startstr='!Inihead',
                endstr='!End Inihead', str2add=['!Initial head\n!10.0\n'])

    addt2grok(dir_dict, cur_timestep, thetimes, mode=mymode, mytype=type_update)

    return Noreal, dir_dict, fmt_string, curKFile


def read_extraparam(ii, dir_dict, mymode, initialize=False, porosity=False, storativity=False, dispersivity=False,
                    update_mprops=False):
    mpropsFile = os.path.join(dir_dict['fld_mode'], 'lauswiesen.mprops')
    Allparameters = []
    porosity_idx = 0
    storage_idx = 0
    dispersivity_idx = 0

    if initialize is True:
        if porosity is True:
            porosityFile = os.path.join(dir_dict['fld_porosity'], 'porosity_%siter000_timestep_000.npy' % mymode)
        if storativity is True:
            storativityFile = os.path.join(dir_dict['fld_storativity'],
                                           'storativity_%siter000_timestep_000.npy' % mymode)
        if dispersivity is True:
            dispersivityFile = os.path.join(dir_dict['fld_dispersivity'],
                                            'dispersivity_%siter000_timestep_000.npy' % mymode)

    elif initialize is False:
        if porosity is True:
            porosityFile = dm.getdirs(dir_dict['fld_porosity'], mystr='porosity_%s' % mymode, fullpath=True,
                                      onlylast=True)
        if storativity is True:
            storativityFile = dm.getdirs(dir_dict['fld_storativity'], mystr='storativity_%s' % mymode, fullpath=True,
                                         onlylast=True)
        if dispersivity is True:
            dispersivityFile = dm.getdirs(dir_dict['fld_dispersivity'], mystr='dispersivity_%s' % mymode,
                                          fullpath=True, onlylast=True)

    if porosity is True:
        porosity_idx = 3
        porosity_param = np.load(porosityFile)
        Allparameters.append(porosity_param)
        if update_mprops is True:
            update_mpropsFile('porosity', porosity_param[:, ii], mpropsFile)

    if storativity is True:
        storage_idx = 1
        storativity_param = np.load(storativityFile)
        Allparameters.append(storativity_param)
        if update_mprops is True:
            update_mpropsFile('storage', storativity_param[0, ii], mpropsFile)

    if dispersivity is True:
        dispersivity_idx = 1
        dispersivity_param = np.load(dispersivityFile)
        Allparameters.append(dispersivity_param)
        if update_mprops is True:
            update_mpropsFile('dispersivity', dispersivity_param[0, ii], mpropsFile)

            # Tot parameters,
    return Allparameters, [porosity_idx + storage_idx + dispersivity_idx, porosity_idx, storage_idx, dispersivity_idx]


def update_mpropsFile(param_name, param_values, mpropsFile):
    try:  # Check if it a single value, if not then do not transform to float
        param_values = float(param_values)
    except TypeError:
        pass

    if param_name == 'porosity':
        addstr2grok(mpropsFile, startstr='!mobile', endstr='!end mobpor',
                    str2add=['porosity', '%s' % float(param_values[0])])
        addstr2grok(mpropsFile, startstr='immobile zone porosity', endstr='!end immobpor',
                    str2add=['%s' % float(param_values[1])])
        addstr2grok(mpropsFile, startstr='immobile zone mass transfer coefficient ', endstr='!end transfer',
                    str2add=['%s' % float(param_values[2])])
    if param_name == 'storage':
        addstr2grok(mpropsFile, startstr='specific storage', endstr='!end storativity',
                    str2add=['%s' % param_values])
    if param_name == 'dispersivity':
        addstr2grok(mpropsFile, startstr='longitudinal dispersivity', endstr='!end long disp',
                    str2add=['%s' % param_values])
        addstr2grok(mpropsFile, startstr='!transverse', endstr='!end trans disp',
                    str2add=['transverse dispersivity', '%s' % (param_values / 10.0)])
        addstr2grok(mpropsFile, startstr='vertical transverse dispersivity', endstr='!end vtrans disp',
                    str2add=['%s' % (param_values / 100.0)])


def packXtra_params(param_name, mydir, nreal, cur_iter=0, cur_time=0, mymode='fl_'):
    """ Pack single parameter values previously stored in individual files, into a single file
        Args:
    param_name:
    mydir:
    nreal:
    cur_iter:
    cur_time:
    mymode:
        Returns:
    """
    if param_name == 'porosity':
        temp_string = 'porositytemp'
        filename = 'porosity_%siter%.3d_timestep_%.3d.npy' % (mymode, cur_iter, cur_time)
        upd_params = np.zeros((3, nreal))
    elif param_name == 'storativity':
        temp_string = 'storativitytemp'
        filename = 'storativity_%siter%.3d_timestep_%.3d.npy' % (mymode, cur_iter, cur_time)
        upd_params = np.zeros((1, nreal))
    elif param_name == 'dispersivity':
        temp_string = 'dispersivitytemp'
        filename = 'dispersivity_%siter%.3d_timestep_%.3d.npy' % (mymode, cur_iter, cur_time)
        upd_params = np.zeros((1, nreal))
    st_file = dm.getdirs(mydir, mystr=temp_string, fullpath=True, onlylast=False)

    for ww in range(0, len(st_file)):
        try:
            upd_params[:, ww] = np.load(st_file[ww])
        except ValueError:
            upd_params[:, ww] = np.load(st_file[ww]).squeeze()
        os.remove(st_file[ww])

    np.save(os.path.join(mydir, filename), upd_params)

    print('File < %s > created' % filename)


def cpSrcfiles(SourceDir, DestDir, mymode, fmt_string, hsplot=False):
    """
    Support for the <prepfwd>, this is the function that does copy the folder. It copies all
    dependencies, adds an identifier to each file, creates a batch file with proper prefix
    to make GROK run automatically, and replace corresponding file-names within GROK file
        Arguments:
        ----------
    SourceDir:          str, folder to be copied (full path)
    DestDir:            str, destination directory
    mymode:             str, fl_ or tr_, defines flow or transport
    fmt_string:         str, Identifier to assign to each new copied folder
    StrFiles2Replace:   dict, files to be replaced within GROK (eg.
                        {'kkk_file.dat': cur_kkkFile,'InitialHeads.head_pm.001': 'iniHeads_%s.head_pm'}) Not full path
    hsplot:             bool, default is False: not include hsplot in the batch file sequence
        Returns:
        --------
    Creates a brand new folder with the necessary files to make a forward HGS run using a
    current KKK field
    """
    # %% Copy directories and files:
    Ignorefiles = ('mesh*', '*.npy')
    try:
        shutil.copytree(SourceDir, DestDir, ignore=shutil.ignore_patterns(Ignorefiles))
    except:
        myfiles = os.listdir(SourceDir)
        if 'meshtecplot.dat' in myfiles: myfiles.remove('meshtecplot.dat')
        myfiles = [x for x in myfiles if '.npy' not in x]
        for item in myfiles:
            if 'groks' in os.path.join(DestDir, item):
                pass
            else:
                try:
                    shutil.copy2(os.path.join(SourceDir, item), os.path.join(DestDir, item))
                except:
                    pass

    # %% Rename grok file if necessary:
    new_str = mymode + 'sim' + ('_%s.grok' % fmt_string)
    oldGrok = '%ssim.grok' % mymode

    newFileslist = os.listdir(DestDir)
    if new_str in newFileslist:
        os.remove(os.path.join(DestDir, oldGrok))
        newgrok = next(x for x in newFileslist if x.endswith('.grok'))
        print('No need to rename << %s >>, nor corresponding batch files' % newgrok)

    if (oldGrok in newFileslist) and (new_str not in newFileslist):
        # grok_loc = newFileslist.index(oldGrok)
        os.rename(os.path.join(DestDir, oldGrok), os.path.join(DestDir, new_str))
        # os.remove(os.path.join(DestDir, oldGrok))

        # %% Create the prefix and "let me run " batch files with the proper string:
    CreatePrefixBatch(DestDir, new_str.split('.')[0])
    Createletmerun(DestDir, new_str.split('.')[0], hsplot=hsplot)

    return new_str


def changeStrGrok(DestDir, grokfile, StrFiles2Replace):
    In_grokFile = os.path.join(DestDir, grokfile)
    tempGrok = '%s_temp' % In_grokFile

    InFileObj = open(In_grokFile)
    InFile = InFileObj.read()
    OutFile = open(tempGrok, 'w')
    for i in list(StrFiles2Replace.keys()):
        InFile = InFile.replace(i, StrFiles2Replace[i])

    OutFile.write(InFile)
    OutFile.close()
    InFileObj.close()
    os.remove(In_grokFile)
    os.rename(tempGrok, In_grokFile)


def addstr2grok(grokfile, startstr='', endstr='', str2add=[]):
    """ Add a string or a list of strings to an existent grok file
        Arguments:
            startstr:
            endstr:
            str2add:
        :return:
        """

    tempgrokfile = '%s_temp' % grokfile
    oldstr_idx = dm.str_infile(grokfile, startstr, index=True)  # todo: reference properly this function

    if oldstr_idx is None:
        print('String < %s > not found in <%s>' % (startstr, os.path.split(grokfile)[-1]))
    elif oldstr_idx is not None:
        writing = True
        with open(grokfile) as f:
            with open(tempgrokfile, 'w') as out:
                for line in f:
                    if writing:
                        if startstr in line:
                            writing = False
                            out.write(startstr)
                            for ii in range(0, len(str2add)):
                                out.write('\n%s' % (str2add[ii]))
                            buffer = [line]
                            out.write("\n%s\n" % endstr)
                        else:
                            out.write(line)
                    elif endstr in line:
                        writing = True
                    else:
                        buffer.append(line)
                else:
                    if not writing:
                        # There wasn't a closing "event", so write buffer contents
                        out.writelines(buffer)
                out.close()
                f.close()
        os.remove(grokfile)
        os.rename(tempgrokfile, grokfile)
        # print('Given strings added to < %s > ' % grokfile)
        str_grokfile = os.path.join(os.path.split(os.path.split(grokfile)[0])[-1], os.path.split(grokfile)[-1])
        print('Strings: %s added to < %s >' % (str2add, str_grokfile))


def prepareGROK(SourceDir, DestDir, mymode, fmt_string, StrFiles2Replace, hsplot=False):
    """
    Support for the <prepfwd>, this is the function that does copy the folder. It copies all
    dependencies, adds an identifier to each file, creates a batch file with proper prefix
    to make GROK run automatically, and replace corresponding file-names within GROK file
        Arguments:
        ----------
    SourceDir:          str, folder to be copied (full path)
    DestDir:            str, destination directory
    mymode:             str, fl_ or tr_, defines flow or transport
    fmt_string:         str, Identifier to assign to each new copied folder
    StrFiles2Replace:   dict, files to be replaced within GROK (eg.
                        {'kkk_file.dat': cur_kkkFile,'InitialHeads.head_pm.001': 'iniHeads_%s.head_pm'}) Not full path
    hsplot:             bool, default is False: not include hsplot in the batch file sequence
        Returns:
        --------
    Creates a brand new folder with the necessary files to make a forward HGS run using a
    current KKK field
    """
    # %% Copy directories and files:
    Ignorefiles = 'mesh*'
    try:
        shutil.copytree(SourceDir, DestDir, ignore=shutil.ignore_patterns(Ignorefiles))
    except:
        myfiles = os.listdir(SourceDir)
        if 'meshtecplot.dat' in myfiles: myfiles.remove('meshtecplot.dat')
        for item in myfiles:
            if 'groks' in os.path.join(DestDir, item):
                pass
            else:
                try:
                    shutil.copy2(os.path.join(SourceDir, item), os.path.join(DestDir, item))
                except:
                    pass

    # %% Create a new GROk file with the KKK file updated:
    new_str = mymode + 'sim' + ('_%s.grok' % fmt_string)
    Out_grokFile = os.path.join(DestDir, new_str)

    if not os.path.isfile(Out_grokFile):

        In_grokFile = os.path.join(DestDir, mymode + 'sim' + '.grok')

        # %% Copy heads file if working with transport:
        # No!!! This will be done in the get states function, here just the name
        # is replaced in the GROK file

        InFileObj = open(In_grokFile)
        InFile = InFileObj.read()
        OutFile = open(Out_grokFile, 'w')
        for i in list(StrFiles2Replace.keys()):
            InFile = InFile.replace(i, StrFiles2Replace[i])

        OutFile.write(InFile)
        OutFile.close()
        InFileObj.close()
        os.remove(In_grokFile)

        # %% Create the prefix and "let me run " batch files with the proper string:
        CreatePrefixBatch(DestDir, new_str.split('.')[0])
        Createletmerun(DestDir, new_str.split('.')[0], hsplot=hsplot)


def fwdHGS(mydir, mystr, mytype='', hsplot=False):
    """
    Run GROK and HGS and if stated, run also hsplot
        Arguments:
        ----------
    mydir:          str, full path directory where GROK and HGS should be run
    mystr:          str, flag to print no. of realization in the cmd
    mytype:         str, flag to print flow or transport identifier in the cmd
    hsplot:         bool, (optional) if false, hspot is not executed
    """
    print('Process No. %s: running GROK (%s)... ' % (mystr, mytype))
    if platform.system() == 'Windows':
        qq = sp.Popen('grok', shell=True, cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    else:
        qq = sp.Popen('grok.x', shell=True, cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq, error_qq = qq.communicate()
    if 'Normal exit' not in str(output_qq):
        myfile = open(os.path.join(mydir, 'grokOutput.txt'), 'wb')
        myfile.write(output_qq)
        myfile.close()
        sys.exit(
            'Something went wrong when executing GROK in process %s(%s)... Check grokOutput.txt file' % (mystr, mytype))

    print('Process No. %s: running PHGS (%s)... ' % (mystr, mytype))
    if platform.system() == 'Windows':
        qq_2 = sp.Popen('phgs', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    else:
        qq_2 = sp.Popen('hgs.x', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq_2, error_qq_2 = qq_2.communicate()
    if 'NORMAL EXIT' not in str(output_qq_2):
        myfile = open(os.path.join(mydir, 'hgsOutput.txt'), 'wb')
        myfile.write(output_qq_2)
        myfile.close()
        if ('No more mass' in str(output_qq_2)) or ('Warning -1' in str(output_qq_2)):
            print('No more mass stored in the system: Process No. << %s: %s >> ... ' % (mystr, mytype))
            print('Check hgsOutput.txt file')
        else:
            sys.exit('Something went wrong when executing HGS in process %s(%s)... Check hgsOutput.txt file' % (
                mystr, mytype))

    if hsplot is True:
        print('Process No. %s: running HSPLOT(%s)... ' % (mystr, mytype))
        if platform.system() == 'Windows':
            qq_3 = sp.Popen('hsplot', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
        else:
            qq_3 = sp.Popen('hsplot.x', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
        output_qq_3, error_qq_3 = qq_3.communicate()
        if platform.system() == 'Windows':
            qq_3.kill()
    if platform.system() == 'Windows':
        qq.kill()
        qq_2.kill()
    print('Successful fwd model run, Process No. %s (%s)' % (mystr, mytype))


def fwdHGS_batch_caller(mydir, batchfile, mymode='fl_', updateGrok=False, newtimes='', parallel=False, cpus=0,
                        remove=False, files2keep='', linux=False, runbatch=False):
    """ Run batch load of HGS models. e.g. run all models within the ensemble: allproc
    Args:
        mydir:          str, directory containing all folders with models
        batchfile:      str, name of the common batchfile that runs GROK,PHGS and HSPLOT
        mymode:         str, identifier to discriminate between type of models
        updateGrok:     bool, update times in grok or not
        newtimes:       array, new times to add to grok if updateGrok is True
        files2keep:
        remove:
    Returns:
        Exit banner if all runs were successfully
    """

    procfolderlist = os.listdir(mydir)

    if parallel is True:
        # Create the pool if in parallel:
        mypool = mp.Pool(cpus)
        mypool.starmap(fwdHGS_batch, zip(procfolderlist, repeat(mymode), repeat(mydir), repeat(updateGrok),
                                         repeat(newtimes), repeat(batchfile), repeat(remove),
                                         repeat(files2keep), repeat(linux), repeat(runbatch)))
        mypool.close()
        mypool.join()

    if parallel is False:
        for idx, cur_procfolder in enumerate(procfolderlist):
            fwdHGS_batch(cur_procfolder, mymode, mydir, updateGrok, newtimes, batchfile, remove, files2keep, linux,
                         runbatch)


def fwdHGS_batch(procfolder, mymode, mydir, updateGrok, newtimes, batchfile, remove, files2keep, linux, runbatch):
    """
    Function designed to run HGS from a batchfile (e.g. "letmerun"). It is designed to be able to run in parallel and
    the directory structure for the EnKF...
    Args:
        procfolder:     str, folder containing all processes of the ensemble
        mymode:         str, 'fl_' for flow model or 'tr_' for transport
        mydir:          str, main directory (one level above procfolder)
        updateGrok:     bool, if it is wanted to update output times in each grok file
        newtimes:       np.array, with the output times
        batchfile:      str, name of the batch file to run the HGS model
        files2keep:
        remove:
    Returns:

    """

    modelfolder = os.listdir(os.path.join(mydir, procfolder))

    for idy, cur_modelfolder in enumerate(modelfolder):

        if mymode in cur_modelfolder:
            cur_dir = os.path.join(mydir, procfolder, cur_modelfolder)

            if updateGrok is True:
                mydict = {'fld_mode': cur_dir}
                addt2grok(mydict, None, newtimes, mode=mymode, mytype='full')
                print('Working with directory << %s >>' % os.path.split(mydir)[0])
            if runbatch is True:
                print('Running model %s. Executing batchfile %s' % (cur_modelfolder, batchfile))

            if linux is False:
                changeStrGrok(cur_dir, batchfile, {'grok.x': 'grok',
                                                   'hgs.x': 'phgs'})
            elif linux is True:
                changeStrGrok(cur_dir, batchfile, {'grok': 'grok.x', 'phgs': 'hgs.x'})

            if runbatch is False:
                fwdHGS(cur_dir, cur_modelfolder, mytype=mymode, hsplot=False)
            elif runbatch is True:
                qq = sp.Popen(batchfile, shell=True, cwd=cur_dir, stdout=sp.PIPE, stderr=sp.PIPE)
                output_qq, error_qq = qq.communicate()
                if 'Normal exit' not in str(output_qq):
                    myfile = open(os.path.join(cur_dir, '%s.txt' % batchfile), 'wb')
                    myfile.write(output_qq)
                    myfile.close()
                    sys.exit('Something went wrong when executing %s in %s... Check %s.txt file' % (
                        batchfile, cur_modelfolder, batchfile))

        if remove is True:
            dummypaths = dm.rmfiles(files2keep, cur_modelfolder, os.path.join(mydir, procfolder))
            # dummypaths = dm.rmfiles(files2keep, '%s%.5d' % (mymode, zz + 1), os.path.join(allprocDir, curprocess))
            print('Removing files from folder < %s >' % cur_modelfolder)
            del dummypaths


def getmodout(mypath, mode='', outputtime='', initial_head=7.0, neg2zero=False, head2dwdn=False, cumbtc=False,
              norm=False, moments=False, curtimestep=None, curreal=None, dirtree=None, ref=False, hgs='2018'):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in transport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    # assert outputtime != '', 'No output times provided'
    if not mode:
        print('Model outputs not extracted. No running mode provided')
    elif mode == 'fl_':
        mystrs = 'well_flow'
    elif mode == 'tr_':
        mystrs = 'well_conc'

    # Get a list of the files of all observation points:
    files = dm.getdirs(mypath, mystr=mystrs, fullpath=True, onlylast=False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for idx, cur_file in enumerate(files):
        selMeas = rmodout(cur_file, outputtime, mode, hgs=hgs)

        if not isinstance(initial_head, (float, str)):
            local_inihead = initial_head[idx]
        else:
            local_inihead = initial_head

        transfMeas = procmodout(selMeas, outputtime, ini_head=local_inihead, neg2zero=neg2zero,
                                head2dwdn=head2dwdn, cum=cumbtc, norm=norm, moments=moments,
                                curtimestep=curtimestep, curreal=curreal, mymode=mode, obsid=idx, dirtree=dirtree,
                                ref=ref)
        # selMeas = rmodout(ii, outputtime.astype(np.int))
        all_modout = np.append(all_modout, transfMeas)  # All selected heads

    all_modout = np.reshape(all_modout, (-1, len(files)), order='F')

    return all_modout


def read_save_ow_coord(listfiles, savefile):
    """ Read and save observation file coordinates and node number (all related to the model grid)
    Args:
        listfiles:     list, all files to be used to extract X,Y,Z, node. Provide full path for each
        savefile:       str, full path directory to store the observation well coordinates
    Returns:
        creates an ascii "savefile" with X,Y,Z, NoNode columns
    """
    ow_all = np.empty((len(listfiles), 4), dtype=float)
    ow_name_all = np.empty((len(listfiles),), dtype='|S15')

    for ow_id, curfile in enumerate(listfiles):

        assert os.path.isfile(curfile) is True, '<< %s >> is not a file. Check the list'

        if len(os.path.split(curfile)[-1].split('.')) == 5:  # for the extra name when running transport
            ow_name_all[ow_id] = os.path.split(curfile)[-1].split('.')[-3]
        else:
            ow_name_all[ow_id] = os.path.split(curfile)[-1].split('.')[-2]

        try:
            ow_all[ow_id, :] = np.genfromtxt(curfile, skip_header=3, usecols=(4,5,6,7), max_rows=1)
        except ValueError:
            try:
                ow_all[ow_id, :] = np.genfromtxt(curfile, skip_header=3, usecols=(3,4,5,6), max_rows=1)
            except:
                ow_all[ow_id, :] = np.genfromtxt(curfile, skip_header=3, usecols=(2,3,4,5), max_rows=1)

    joined_set = np.c_[ow_name_all.astype('str'), ow_all]
    np.savetxt(savefile, joined_set, fmt='%s', header='name, Xcoo, Ycoo, Zcoo, Node')

    return joined_set


def rmodout(myfile, mytimes, mymode, dualdomain=True, hgs='2016'):
    """ Read HydroGeoSphere outputs. Reads from well-like and point-like type of observation. It supports reading heads
    from flow simulations and concentrations from a single- and double-domain model. If the model outputs are from a
    well-type of observation, it returns the averaged value over all
            Arguments:
            ----------
        myfile:     str, fullpath with the name of the file to be loaded
        mytimes:    np array,  float or int array with times of interest
        mymode:     str, model run mode. 'fl_' for flow model, 'tr_for transport'
        dualdomain: bool, if True, reads mob and immob concentrations from a dual domain transport model. Default = True
            Returns:
            --------
        mymodmeas:     np.array, with model outputs at specific times (nmeas x 1)
        """

    if (mymode == 'tr_') and (dualdomain is True):
        try:
            # Observation well HGS format:
            mymodmeas = []
            # mymodmeas1, mymodmeas2 = [], []
            idx_line, hgsTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)
            if isinstance(idx_line, int) is True:
                hgsTime = np.asarray(hgsTime, dtype=float)
                mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=3, usecols=(0,1), unpack=True)
                mymodmeas = np.mean(mymodmeas1) + np.mean(mymodmeas2)
            if isinstance(idx_line, int) is False:
                with open(myfile, 'r') as f:
                    mylines = f.readlines()
                for yy in range(0, len(idx_line), 1):
                    mymodemeas1_temp, mymodemeas2_temp = [], []
                    if yy < len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                    elif yy == len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1:])
                    for cur_line in myvalues:
                        cur_values = list(filter(None, np.asarray(cur_line.split(' ')).tolist()))
                        mymodemeas1_temp.append(float(cur_values[0]))
                        mymodemeas2_temp.append(float(cur_values[1]))
                    mymodmeas1, mymodmeas2 = mean(mymodemeas1_temp), mean(mymodemeas2_temp)
                    mymodmeas.append(mymodmeas1 + mymodmeas2)
                    #
                    # try:
                    #     mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
                    # except:
                    #     mymodmeas.append(mean([float(cur_line.split(' ')[3]) for cur_line in myvalues]))

            if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
                mymodmeas = np.asarray(mymodmeas)
                mymodmeas = np.expand_dims(mymodmeas, axis=1)
        except:
            try:
                hgsTime, mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=2, usecols=(0, 1, 2), unpack=True)
            except:
                hgsTime, mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=3, usecols=(0, 1, 2), unpack=True)

            mymodmeas = mymodmeas1 + mymodmeas2

    else:
        try:
            # Observation well HGS format:
            mymodmeas = []
            idx_line, hgsTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)
            if isinstance(idx_line, int) is True:
                hgsTime = np.asarray(hgsTime, dtype=float)
                mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0,))
                mymodmeas = np.mean(mymodmeas)
            if isinstance(idx_line, int) is False:
                with open(myfile, 'r') as f:
                    mylines = f.readlines()
                for yy in range(0, len(idx_line), 1):
                    if yy < len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                    elif yy == len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1:])
                    try:
                        mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
                    except:
                        mymodmeas.append(mean([float(cur_line.split(' ')[3]) for cur_line in myvalues]))
            if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
                mymodmeas = np.asarray(mymodmeas)
                mymodmeas = np.expand_dims(mymodmeas, axis=1)
        except:
            if hgs == '2018':
                hgsTime, mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0, 1), unpack=True)
            else:
                hgsTime, mymodmeas = np.loadtxt(myfile, skiprows=2, usecols=(0, 1), unpack=True)

    # If a single output time is generated, create an extra dimension:
    if (isinstance(mytimes, int) is True) or (isinstance(mytimes, float) is True):
        mytimes = np.expand_dims(mytimes, 1)

    # %% Mask to get only the elements of interest:
    if isinstance(mymodmeas, np.ndarray) is False:
        mymodmeas = np.expand_dims(mymodmeas, axis=1)
        if len(mytimes) == 1:
            mytimes = int(round(mytimes[0] * 10)) / 10

    mymodmeas = mymodmeas[np.where(np.in1d(hgsTime, mytimes))]

    if mymodmeas.ndim > 1:
        mymodmeas = mymodmeas.squeeze()
    return mymodmeas


def procmodout(modmeas, mytimes, ini_head=0.0, neg2zero=False, head2dwdn=False, cum=False, norm=False, moments=False,
               curtimestep=None, curreal=None, mymode=None, obsid=None, dirtree=None, ref=False):
    """
    Process the loaded model outputs, according to the settings provided.
    Args:
        modmeas:
        mytimes:
        ini_head:
        neg2zero:
        head2dwdn:
        cum:
        norm:
        moments:
    Returns:
    """
    copy_modmeas = np.copy(modmeas)

    if (neg2zero is True) or (neg2zero == 'True'):
        zero_id = np.where(copy_modmeas < 0)
        try:
            copy_modmeas[zero_id[0]] = 0
        except IndexError:
            copy_modmeas[np.newaxis][zero_id[0]] = 0

    if (head2dwdn is True) or (head2dwdn == 'True'):
        copy_modmeas = np.subtract(ini_head, copy_modmeas)

    if (cum is True) or (cum == 'True'):

        if ref is False:
            copy_modmeas = np.cumsum(copy_modmeas)[-1]  # Not yet normalized!
        elif ref is True:
            copy_modmeas = np.cumsum(copy_modmeas)  # Not yet normalized!
    if ((norm is True) and (cum is True)) or ((norm == 'True') and (cum == 'True')):
        copy_modmeas /= np.sum(copy_modmeas)
    elif ((norm is True) and (cum is False)) or ((norm == 'True') and (cum == 'False')):
        copy_modmeas = (copy_modmeas - min(copy_modmeas)) / (max(copy_modmeas) - min(copy_modmeas))

    if (moments is True) or (moments == 'True'):
        copy_modmeas = mysst.calc_tempMoments(mytimes, copy_modmeas)

    return copy_modmeas


def readHGSbin(myfile, nnodes, endfile=False):
    """
    Read a HGS output bin file. The function has been tested with
    subsurface hydraulic heads and overland heads.
        Arguments:
        ----------
    myfile:     str, filename *full path(
    nnodes:     int, number of nodes in the mesh
        Returns:
        --------
    n_param:    np.array with the nodal parameter values
    time:       float, model time that corresponds to the param field values
    """
    with open(myfile, "rb") as f:
        if endfile is False:
            pad = np.fromfile(f, dtype='uint8', count=4)
            mytime = np.fromfile(f, dtype='uint8', count=80)

            tt = [''.join(chr(t)) for t in mytime]
            mytime = float(''.join(tt))

            pad = np.fromfile(f, dtype='uint8', count=4)
            pad = np.fromfile(f, dtype='uint8', count=4)

            param = np.fromfile(f, dtype='float64', count=nnodes)
            pad = np.fromfile(f, dtype='uint8', count=4)

        if endfile is True:
            pad = np.fromfile(f, dtype='uint8', count=4)
            param = np.fromfile(f, dtype='float64', count=nnodes)
            mytime=None

    return param, mytime


def CreatePrefixBatch(mydir, myString):
    """
    Create a new Prefix file for running GROK and HGS
        Arguments:
        ----------
    mydir:      str, directory where the file should be located
    myString:   str, string to add to the prefix file
        Returns:
        --------
    The batch file batch.pfx with problem prefix
    """
    prefFile = 'batch.pfx'
    if prefFile in os.listdir(mydir):
        os.remove(os.path.join(mydir, prefFile))

    prefix = open(os.path.join(mydir, prefFile), 'w')
    prefix.write(myString)
    prefix.close()


def Createletmerun(mydir, myString, hsplot=False):
    """
    Create a letmerun batch file for running GROK, HGS and HSPLOT
        Argument:
        ---------
    mydir:      str, directory where the file should be located
    myString:   str, name of the GROK file to be executed
    hsplot:     bool, True: include instruction to run hsplot
                             False: just include grok and phgs
        Returns:
        --------
    The batch file letmerun.bat
    """
    batchfile = 'letmerun.bat'
    if batchfile in os.listdir(mydir):
        os.remove(os.path.join(mydir, batchfile))

    letmerun = open(os.path.join(mydir, batchfile), 'w')
    letmerun.write('@echo off\n\ndel *' + myString + 'o* > nul\n\n')
    if platform.system() == 'Linux':
        letmerun.write('grok.x\nhgs.x\n')
    else:
        letmerun.write('grok\nphgs\n')

    if hsplot is True:
        if platform.system() == 'Linux':
            letmerun.write('hsplot.x\n')
        else:
            letmerun.write('hsplot\n')

    letmerun.close()


def rdparam_bin(myPath, cmstr=''):
    """ Read parameter fields from binary files. Binary file names should contain a specific string identifier
    to constrain the file list.
    Arguments:
        myPath:     Str, directory where the information is stored
        cmstr:      Str, identifier for the files to work with
    Returns:
        nel:        Integer, number of elements of the model grid
        nreal:      Integer, number of realizations in the ensemble
        Y:          Numpy array, 2D Array with the proper data
    """
    # myFiles = os.listdir(myPath)
    myFiles = [x for x in os.listdir(myPath) if cmstr in x]
    myFiles.sort()

    for idx, curkkk in enumerate(myFiles):
        if idx == 0:
            kkkmat = np.load(os.path.join(myPath, curkkk))

        elif idx > 0:
            kkk = np.load(os.path.join(myPath, curkkk))
            kkkmat = np.c_[kkkmat, kkk]  # Parameter field matrix (nel,nreal)

    nel, nreal = kkkmat.shape  # Number of elements in the domain
    return nel, nreal, kkkmat


def rdmodelout_bin(myPath, mymode='fl_'):
    """ Read Modeled outputs from binary files. Either heads or concentrations.
    Arguments:
        myPath:             Str, directory where the information is stored
        mymode:             str, type of data to read
    Returns:
        num_modout:              Integer, number of model outputs
        modoutput:       Numpy array, modeled measurement values (ndata x nrealizations)

    Note: The matrices of measurements are flattened following Fortran
        convention (column-wise or column major). If work with both types of
        measurements, heads are listed first and then concentrations
    """
    myFiles = os.listdir(myPath)
    myFiles.sort()
    basket = 0
    for curid, kk in enumerate(myFiles):

        if mymode in kk:
            cur_file = os.path.join(myPath, myFiles[curid])

            if basket == 0:
                modoutput = np.load(cur_file).flatten('F')

            elif basket > 0:
                cur_modout = np.load(cur_file).flatten('F')
                modoutput = np.c_[modoutput, cur_modout]
            basket += 1

    num_modout = modoutput.shape[0]  # Number of observations

    return num_modout, modoutput


def addt2grok(mypath_dict, cur_timestep, modeltimes, mode='fl_', mytype='restart'):
    """ Add/subtract new output times to GROK. The times are taken from the
    '_outputtimesFlowUpdate.dat' and/or '_outputtimesTransUpdate.dat'
    Arguments:
        mypath_dict:
        cur_timestep:
        modeltimes:
        mode:               'fl_'
        mytype:             'restart' cumulative
    Returns:
        GROK file with updated time step
    """
    # %% Directories and file names:

    mygrokfile = dm.getdirs(mypath_dict['fld_mode'], mystr='%ssim_' % mode, fullpath=True)
    mygrokfile = mygrokfile[0]
    tempgrokfile = mygrokfile + 'temp'

    # %% Working with the time steps to modify in grok:
    if 'std' in mytype:
        mytimes = modeltimes[int(cur_timestep)][np.newaxis]
    elif 'restart' in mytype:
        mytimes = modeltimes[0:int(cur_timestep + 1)]
    elif 'full' in mytype:
        mytimes = modeltimes
    # if ('restart' in mytype) or ('std' in mytype):
    #     mytimes = modeltimes[int(cur_timestep)][np.newaxis]
    # elif 'cum' in mytype:
    #     mytimes = modeltimes[0:int(cur_timestep + 1)]
    # elif 'full' in mytype:
    #     mytimes = modeltimes

    writing = True
    with open(mygrokfile) as f:

        with open(tempgrokfile, 'w') as out:
            for line in f:
                if writing:
                    if "output times" in line:
                        writing = False
                        out.write("output times")
                        for ii in range(0, len(mytimes)):
                            out.write('\n%s' % (mytimes[ii]))
                        buffer = [line]
                        out.write("\nend\n")
                    else:
                        out.write(line)
                elif "end" in line:
                    writing = True
                else:
                    buffer.append(line)
            else:
                if not writing:
                    # There wasn't a closing "event", so write buffer contents
                    out.writelines(buffer)
            out.close()
            f.close()
    os.remove(mygrokfile)
    os.rename(tempgrokfile, mygrokfile)

    print('grok file < %s > updated...' % (os.path.split(str(mygrokfile))[-1]))


def add_head2grok(grokfile, headsFile1):
    """

    Args:
        grokfile: full path to the grok file of interest

    Returns:

    """
    newgrok = os.path.join('%stemp' % grokfile)

    h_str = dm.str_infile(grokfile, '!restart file for heads', index=True)

    if h_str:
        replace = {'!restart file for heads': 'restart file for heads'}
        # replace = {'Initial head from file': 'Initial head from file'}
        InFileObj = open(grokfile)
        InFile = InFileObj.read()
        for i in list(replace.keys()):
            InFile = InFile.replace(i, replace[i])
        OutFile = open(newgrok, 'w')
        OutFile.write(InFile)
        OutFile.close()
        InFileObj.close()
        os.remove(grokfile)
        OutFile = open(newgrok)
        strfound = dm.str_infile(newgrok, list([replace.values()][0])[0], index=True)
        fp = open(grokfile, 'w')
        for i, line in enumerate(OutFile):
            if (i - 1) == strfound[0]:
                fp.write(headsFile1 + '\n')
            else:
                fp.write(line)
        fp.close()
        OutFile.close()
        os.remove(newgrok)
    else:
        print('< Initial head > command was already updated...')  # todo: improve this print statement


def run_refmodel(mainPath, curKFile, Flowdir, modelDataFolder, mymodel, mymode='fl_', runmodel=True, std_dev=0,
                 ini_head=0, neg2zero=False, head2dwdn=False, cumbtc=False, norm=False, moments=False,
                 irregularGrid=False,
                 ref=False, gridformat = 'tp2018'):
    """
    Perform all sequence to obtain a synthetic model with heterogeneous fields and extracting model outputs. It is
    possible to add measurement noise to model outputs.
        Args:
    mainPath:   str, directory where the model sub-directory is located, eg ../myModels
    curKFile:   str, filename containing K parameters, eg kkkGaus.dat
    Flowdir:    str, folder name where flow synthetic model is stored, eg _fl_26062015_synthetic
    modelDataFolder: str, folder name containing model input data, eg ModelData_26062015_synthetic
    mymodel:    str, folder of the model that is wished to run eg _fl_26062015_synthetic or tr_...
    mymode:     str, 'fl_':flow , 'tr_':transport. Default fl_
    runmodel:   bool, whether it is required to run the HGS model. Default is True
    std_dev:    float, measurement noise std deviation added to model outputs. Default 0 (no noise)
    ini_head:   float, initial head value in case of running flow model. Default 0

        Returns:    All output generated from GROK, PHGS and HSPLOT, and two arrays with model outputs and model times
    Input example:
        >>> mainPath = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\myModels'
        >>> curKFile = 'kkkGaus.dat'
        >>> mymode = 'fl_'  # 'tr_'
        >>> mymodel = '_fl_26062015_synthetic'
        >>> Flowdir = '_fl_26062015_synthetic'
        >>> modelDataFolder = 'ModelData_26062015_synthetic'
        >>> std_dev = 3.0e-2  # As an example assumed a std error measurement of 1.0 mm on each measurement 1.5e-3, tr = 2.5e-6
        >>> runmodel = True
        >>> ini_head = 20.0
    """
    import re
    print('You have to have at least the following files ready:')
    print('Grok file, model parameters file, outputtimes, meshtecplot')

    # Create additional directories
    mainPath = os.path.join(mainPath, 'model')
    curDestDir = os.path.join(mainPath, mymodel)
    grid_file = os.path.join(curDestDir, 'meshtecplot.dat')
    myParameterDir = os.path.join(modelDataFolder, '_RandFieldsParameters.dat')
    curFlowdir = os.path.join(mainPath, Flowdir)
    # Read parameters
    xlen, ylen, zlen, nxel, nyel, nzel,\
    xlim, ylim, zlim, Y_model, Y_var,\
    beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(myParameterDir)

    # Read model mesh components
    # Big and small grid:
    # nnodes_big, elements_big = gm.readmesh(grid_file, fulloutput=False)
    nnodes_big, elements_big, nodes_coord, element_nodes, smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_file, xlim,
                                                                                                        ylim, zlim,
                                                                                                        store=True,
                                                                                                          return_val=True,
                                                                                                        fulloutput=True,
                                                                                                        gridformat=gridformat)
    del nodes_coord, element_nodes
    biggrid_elem_ids = np.arange(1, elements_big + 1, 1)
    # smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_file, xlim, ylim, zlim)
    # smallgrid_elem_ids = np.arange(1, nnodes + 1, 1)

    # If K field does not exists, make a random realizationRead parameter field setup:
    if not os.path.isfile(os.path.join(curFlowdir, 'kkkGaus.dat')):
        kkk = fg.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel,
                                  Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot=True,
                                  mydir=curDestDir)
        if irregularGrid is True:
            from scipy.interpolate import griddata
            reggrid_oordinatesSorted, element_centroid = gm.interpToirregGrid(grid_file, nxel, nyel, nzel, xlim, ylim,
                                                                              zlim)
            kkk = griddata(reggrid_oordinatesSorted, kkk, element_centroid, method='nearest')
        elif irregularGrid is False:
            kkk = gm.expshr_param(kkk, biggrid_elem_ids, smallgrid_elem, what='expand')

        np.savetxt(os.path.join(curFlowdir, curKFile), np.transpose((np.arange(1, len(kkk) + 1, 1), kkk, kkk, kkk)),
                   fmt='%d %.6e %.6e %.6e')

    if not os.path.isfile(os.path.join(curDestDir, 'kkkGaus.dat')):
        if mymode == 'tr_':
            kkk = np.loadtxt(os.path.join(curFlowdir, 'kkkGaus.dat'), usecols=(1,))
            kkk = kkk[smallgrid_elem - 1]
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(kkk) + 1, 1), kkk, kkk, kkk)),
                       fmt='%d %.6e %.6e %.6e')

    # Run the model if required
    if runmodel is True:
        fwdHGS(curDestDir, 'reference', mytype=mymode, hsplot=True)

    # Get the synthetic observations:
    # -----------------------------
    hgs =  re.search('[0-9]{4}',gridformat).group()    # HGS version, eg. 2014
    myouttimes = np.loadtxt(os.path.join(modelDataFolder, '_outputtimes%s.dat' % mymode))
    selObs_All = getmodout(curDestDir, mode=mymode, outputtime=myouttimes, initial_head=ini_head, neg2zero=neg2zero,
                           head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments, ref=ref, hgs=hgs)

    # Add gaussian measurement noise if std_dev is not zero:
    if std_dev != 0:
        num_modmeas = selObs_All.shape[0]
        meanNoise = np.zeros((num_modmeas,))  # Mean value of white noise equal to zero (num_modmeas x 1)
        R = ssp.dia_matrix(
            (np.multiply(np.eye(num_modmeas, num_modmeas), std_dev ** 2)))  # sparse Matrix (nmeas x nmeas)
        E = np.empty((num_modmeas, selObs_All.shape[1]))
        for ii in range(0, selObs_All.shape[1], 1):
            # E[:,ii] = np.expand_dims(np.random.multivariate_normal(mean,R.toarray()),axis = 1)
            E[:, ii] = np.random.multivariate_normal(meanNoise, R.toarray())

        noisy_data = selObs_All + E
    else:
        noisy_data = selObs_All

    # # For concentrations
    # if mymode is 'tr_':
    #     zero_id = np.where(noisy_data < 0)
    #     noisy_data[zero_id[0], zero_id[1]] = 0

    return myouttimes, noisy_data


def str2bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        raise ValueError


def kkk_bin2ascii(infile, outfile, orig_shape, biggrid_elid='', smallgrid_elid='', get_mean=True, exptransf=True,
                  logtransf=False, expand=True):
    """ Read binary files generated during inversion and creates a parameter file (kkk) that HGS can read
    Args:
        infile:
        outfile:
        orig_shape:
        biggrid_elid
        smallgrid_elid
        get_mean:
        exptransf:
        logtransf:
        expand:
    Returns:
    """
    if type(infile) == str:
        myparam = np.load(infile)
    else:
        myparam = infile

    if get_mean is True:
        myparam_mn = myparam.mean(axis=1)  # todo: check if parameters are in log scale or not. I suppose not
        # myparam_mn = np.exp(np.log(myparam).mean(axis=1))
    elif get_mean is False:
        myparam_mn = myparam

    if exptransf is True:
        myparam_mn = np.exp(myparam_mn)
    if logtransf is True:
        myparam_mn = np.log(myparam_mn)

    if expand is True:
        myparam_mn = gm.expshr_param(myparam_mn, biggrid_elid, smallgrid_elid, what='expand')

    assert myparam_mn.shape[0] == orig_shape, 'shape of original and modified parameter arrays do NOT match!'
    np.savetxt(outfile, np.transpose((np.arange(1, len(myparam_mn) + 1, 1), myparam_mn, myparam_mn, myparam_mn)),
               fmt='%d %.18e %.18e %.18e')

    print('File <%s> successfully stored' % outfile)


# Hasta aqui! 09 June 2016


def syncModel2Data(heads, concentrations):
    """ Synchronize the number of model outputs with the number of
    time-steps-updates to perform
    Arguments:
        heads:          Boolean, include head data or not
        concentrations: Boolean, include conc. data or not
    Returns:
        len(cur_heads_time):    Zero if heads == False
        len(cur_conc_time):     Zero if concentration == False
        curTimes:               Number of CURRENT output times dealing with
    Dependencies:
        dm.myMainDir()
    """
    myDir = dm.myMainDir()
    KalmanFilterPath = 'kalmanfilter'

    if heads == True:
        cur_heads_time = np.loadtxt(
            os.path.join(myDir, '..', KalmanFilterPath, 'Model_data', '_outputtimesFlowUpdate.dat'))
        try:
            len(cur_heads_time)
        except:
            cur_heads_time = np.expand_dims(cur_heads_time, 1)

    if concentrations == True:
        cur_conc_time = np.loadtxt(
            os.path.join(myDir, '..', KalmanFilterPath, 'Model_data', '_outputtimesTransUpdate.dat'))
        try:
            len(cur_conc_time)
        except:
            cur_conc_time = np.expand_dims(cur_conc_time, 1)

    # %% Synchronize the number of model outputs with the number
    #  of time-steps-updates to perform:
    try:
        curTimes = len(cur_heads_time) + len(cur_conc_time)
    except:
        if heads == True:
            curTimes = len(cur_heads_time)
        if concentrations == True:
            curTimes = len(cur_conc_time)

    try:
        return len(cur_heads_time), len(cur_conc_time), curTimes
    except:
        if heads == True:
            return len(cur_heads_time), 0, curTimes
        if concentrations == True:
            return 0, len(cur_conc_time), curTimes


def writeinGrok(myInDir, myOutDir, myoutfile, probdescFile, grid_coordFile, gridmethod,
                Transient, Transport, Echo2Output, FD, CV, porousfile, kfile, heads_init, transport_init, heads_bc,
                TimeStepCtrl, well_propsFile, pumpwellsFile, tracer_injFile, outputimesFile, obswellsFile):
    """ Type in the main parts of a grok file
    Arguments:
        myInDir:        Str, directory where input files are located
        myOutDir:       Str, directory where GROK file is/will be located
        myoutfile:      Str, name assigned to the GROK file
        probdescFile:   Str, filename containing description of the project
        grid_coordFile: Str, filename containing grid coordinates
        gridmethod:     Str, method to generate grid-'HGS_uniform' 'HGS_grade' 'GB_file'
        Transient:      Boolean, run transient mode or not
        Transport:      Boolean, run transport mode or not
        Echo2Output:    Boolean, create echo output file or not
        FD:             Boolean, finite difference mode or not
        CV:             Boolean, control volume mode or not
        porousfile:     Str, porous media properties file
        kfile:          Str, KKK filename for homogeneous fields, if homogeneous type ''
        heads_init:     Tuple [mode, arg]:
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number
                        mode: 'raster', arg: string file name
                        mode:'txt', arg: string file name
        transport_init: Tuple [mode, arg]
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number.
                        If transport is False, just type ''
        heads_bc:       tuple [X1, X2], X corrdinate of the X plane to be selected to assign constant head equals to initial value.
                        tuple ['all', 'head equals initial']: all heads stay the same
                        (In the future should support Y and Z planes and possibility
                        to assign boundary conditions from GB files)
        TimeStepCtrl:   Boolean, et the default time step controlers or not
        well_propsFile: Str, filename containing well construction details
        pumpwellsFile:  Str, filename containing well coordinates, rates, etc.
        tracer_injFile: Str, filename containing tracr injection data, if Transport is False, type ''
        outputimesFile: Str, filename containing defined output times
        obswellsFile:   Str, filename containing observation well information

    Returns:
        A new grok file
    """
    # %% PROBLEM DESCRIPTION:
    # Read problem description content from file:
    with open(os.path.join(myInDir, probdescFile), "r") as myfile:
        banner = myfile.read()  # .replace('\n', '')
    # Generate the grok file:
    Ofile = open(os.path.join(myOutDir, myoutfile), 'w')  # Generate the file
    Ofile.write(banner % (datetime.datetime.now()))  # %(time.strftime("%d/%m/%Y"))
    Ofile.flush()
    del banner
    print('<Problem description> written ok...')

    # %% GRID GENERATION: gb or directly in HGS
    banner = '\n\n!---  Grid generation\n'
    Ofile.write('%s' % banner)
    del banner
    if gridmethod == 'HGS_grade':
        Ofile.write('generate blocks interactive\n')
        dim = ['x', 'y', 'z']

        try:
            coordinates = np.loadtxt(os.path.join(myInDir, grid_coordFile))
        except IOError('Unable to load instructions'):
            raise

        if len(dim) != len(coordinates):
            raise IndexError('number of lines in coordinate file must be equal to length of dim vector')

        for cur_dim in range(0, len(dim), 1):
            Ofile.write('grade %s\n' % dim[cur_dim])

            for cur_val in range(0, coordinates.shape[1], 1):
                Ofile.write('%.8e\t' % coordinates[cur_dim, cur_val])
            Ofile.write('\n')

        Ofile.write('end generate blocks interactive\nend\n')
        Ofile.write('Mesh to tecplot\nmeshtecplot.dat\n')

    Ofile.flush()
    print('<Grid generation> written ok...')

    # %% SIMULATION PARAMETERS GROK:
    units = 'units: kilogram-metre-second'
    banner = '\n\n!---  General simulation parameters\n'

    Ofile.write('%s' % banner)
    Ofile.write('%s\n' % units)

    if FD == True: Ofile.write('Finite difference mode\n')
    if CV == True: Ofile.write('Control volume\n')
    if Transient == True: Ofile.write('Transient flow\n')
    if Transport == True: Ofile.write('do Transport\n')
    if Echo2Output == True: Ofile.write('Echo to output\n')

    # %% POROUS MEDIA PROPERTIES:
    Ofile.write('\n!--- Porous media properties\n')
    Ofile.write('use domain type\nporous media\nproperties file\n')
    Ofile.write('./%s\n\nclear chosen zones\nchoose zones all\nread properties\nporous medium\n' % (porousfile))

    if len(kfile) > 0:
        Ofile.write('\nclear chosen elements\nchoose elements all\nRead elemental k from file\n./%s\n' % (kfile))

    if Transport == True:
        name = 'fluorescein'
        difCoef = 1.000e-10
        Ofile.write('\n!--- Solute properties\n')
        Ofile.write('solute\n name\n %s\n free-solution diffusion coefficient\n %.3e\nend solute\n' % (name, difCoef))

    Ofile.flush()
    print('<Simulation param and porous media written ok...')

    # %% INITIAL CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nchoose nodes all\n'
    Ofile.write('\n!--- IC\n!--- Flow:\n%s' % banner)

    if heads_init[0] == 'output':
        try:
            Ofile.write('initial head from output file\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'onevalue':
        Ofile.write('Initial head\n')
        try:
            Ofile.write('%.3e\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'raster':
        try:
            Ofile.write('Map initial head from raster\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'txt':
        try:
            Ofile.write('Initial head from file\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise

    if Transport == True:
        if transport_init[0] == 'onevalue':
            Ofile.write('\n!--- Transport:\n%s' % banner)
            Ofile.write('Initial concentration\n%.3e' % transport_init[1])

        elif transport_init[0] == 'output':
            try:
                Ofile.write('Initial concentration from output file\n')
                Ofile.write('%s\n' % (transport_init[1]))
            except TypeError('Innapropiate argument type in transport_init'):
                raise

    Ofile.flush()
    print('<Initial conditions written ok...')

    # %% BOUNDARY CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nuse domain type\nporous media\n'
    Ofile.write('\n\n!--- BC\n!--- Flow:\n%s' % banner)

    if heads_bc[0] == 'all':
        Ofile.write('\nchoose nodes all\n')
    else:
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n' % (heads_bc[0]))
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n' % (heads_bc[1]))

    Ofile.write('\ncreate node set\nconstant_head_boundary\n')
    Ofile.write('\nBoundary condition\n type\n head equals initial\n node set\n constant_head_boundary\nEnd\n')

    Ofile.flush()
    print('<Boundary conditions written ok...')

    # %% TIME STEP CONTROLS:
    if TimeStepCtrl == True:
        flowsolver_conv_ = 1.0e-6
        flowsolver_max_iter = 500
        # head_control = 0.01
        initial_timestep = 1.0e-5
        min_timestep = 1.0e-9
        max_timestep = 100.0
        max_timestep_mult = 2.0
        min_timestep_mult = 1.0e-2
        underrelax_factor = 0.5

        Ofile.write('\n!--- Timestep controls\n')
        Ofile.write('flow solver convergence criteria\n%.2e\n' % (flowsolver_conv_))
        Ofile.write('flow solver maximum iterations\n%d\n' % (flowsolver_max_iter))
        # Ofile.write('head control\n%.2e\n'%(head_control))
        Ofile.write('initial timestep\n%.2e\n' % (initial_timestep))
        Ofile.write('minimum timestep\n%.2e\n' % (min_timestep))
        Ofile.write('maximum timestep\n%.2e\n' % (max_timestep))
        Ofile.write('maximum timestep multiplier\n%.2e\n' % (max_timestep_mult))
        Ofile.write('minimum timestep multiplier\n%.2e\n' % (min_timestep_mult))
        Ofile.write('underrelaxation factor\n%.2f\n' % (underrelax_factor))
        Ofile.write('No runtime debug\n\n')
        Ofile.flush()
        print('<Default time step controllers written ok...')

    # %% WELL DEFINITION:

    if pumpwellsFile:
        Ofile.write('\n!--- Pumping wells\n\n')
        try:
            wells_data = np.genfromtxt(os.path.join(myInDir, pumpwellsFile), comments="#", delimiter='',
                                       dtype='|S12, f8, f8, f4, f4, f4, f8, f8, f8, f8')
            len(wells_data)
        except (IOError, TypeError):
            print('There was an error with the <wells> file')
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            Ofile.flush()
            Ofile.close()
            raise

        if wells_data[0] == wells_data[1]:
            length_wellsdata = 1
        else:
            length_wellsdata = len(wells_data)

        for ii in range(0, len(wells_data)):
            wellID = wells_data[ii][0]
            X = wells_data[ii][1]
            Y = wells_data[ii][2]
            top = wells_data[ii][3]
            bot = wells_data[ii][4]
            ext_point = wells_data[ii][5]
            pumprate_ini = wells_data[ii][6]
            pumprate_end = wells_data[ii][7]
            time_ini = wells_data[ii][8]
            time_end = wells_data[ii][9]

            Ofile.write('!-Well: %s\nuse domain type\nwell\nproperties file\n%s\n\n' % (wellID, well_propsFile))
            Ofile.write('clear chosen zones\nclear chosen segments\nchoose segments polyline\n2\n')
            Ofile.write('%s, %s, %s ! xyz of top of well\n' % (X, Y, top))
            Ofile.write('%s, %s, %s ! xyz of bottom of well\n' % (X, Y, bot))
            Ofile.write('new zone\n%d\n\n' % (ii + 1))
            Ofile.write('clear chosen zones\nchoose zone number\n%d\n' % (ii + 1))
            Ofile.write(
                'read properties\ntheis well\nclear chosen nodes\nchoose node\n%s, %s, %s\n' % (X, Y, ext_point))
            Ofile.write(
                'create node set\n %s\n\nboundary condition\n  type\n  flux nodal\n\n  name\n  %s\n  node set\n  %s\n' % (
                    wellID, wellID, wellID))
            Ofile.write('\n  time value table\n  %f   %f\n  %f   %f\n end\nend\n\n' % (
                time_ini, pumprate_ini, time_end, pumprate_end))

        Ofile.flush()
        print('<Pumping wells written ok...')
    if not pumpwellsFile:
        print('<Pumping wells not included in grok>')

    # %% TRACER INJECTION:
    if Transport == True:
        Ofile.write('\n!--- Tracer injection\n\n')

        try:
            tracer_data = np.genfromtxt(os.path.join(myInDir, tracer_injFile), comments="#", delimiter='',
                                        dtype='f8, f8, f4, f4, f4, f4')
            len(tracer_data)
        except:
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            print('There was an error with the Tracer Injection File')
            Ofile.flush()
            Ofile.close()
            raise
        if tracer_data[0] == tracer_data[1]:
            length_injdata = 1
        else:
            length_injdata = len(tracer_data)

        for ii in range(0, length_injdata):
            X = tracer_data[ii][0]
            Y = tracer_data[ii][1]
            Z = tracer_data[ii][2]
            time_ini = tracer_data[ii][3]
            time_end = tracer_data[ii][4]
            massflux = tracer_data[ii][5]

            Ofile.write(
                'clear chosen zones\nclear chosen segments\nclear chosen nodes\n\nuse domain type\nporous media\n')
            Ofile.write('\nchoose node\n')
            Ofile.write('%s, %s, %s\n' % (X, Y, Z))
            Ofile.write('\nspecified mass flux\n1\n%s,%s,%s\n\n' % (time_ini, time_end, massflux))

        Ofile.flush()
        print('<Tracer injection written ok...')

    # %% OUTPUT TIMES:
    if outputimesFile:
        try:
            times = np.loadtxt(os.path.join(myInDir, outputimesFile))
        except IOError:
            print('Check the directories of the output-times file')
            raise
        Ofile.write('!--- Outputs\noutput times\n')
        for ii in range(0, len(times)):
            Ofile.write('%s\n' % times[ii])

        Ofile.write('end\n' % times[ii])
        Ofile.flush()
        print('<Output times written ok...')
    if not outputimesFile:
        print('<No output times written in grok...>')

        # %% OBSERVATION WELLS:
    if obswellsFile:
        try:
            obs_wellsData = np.genfromtxt(os.path.join(myInDir, obswellsFile), comments="#", delimiter='',
                                          dtype='|S12, f8, f8, f4')
            len(obs_wellsData)
        except (IOError, TypeError, ValueError, OSError):
            print('Check the directories of the grok and output-times files')
            print('observation wells file must contain at least two lines, if only one entrance, just duplicate it')
            raise

        Ofile.write('\n!--- Observation wells\nuse domain type\nporous media\n\n')
        if obs_wellsData[0] == obs_wellsData[1]:
            length_obsData = 1
        else:
            length_obsData = len(obs_wellsData)

        for ii in range(0, length_obsData):
            ID = obs_wellsData[ii][0]
            X = obs_wellsData[ii][1]
            Y = obs_wellsData[ii][2]
            Z = obs_wellsData[ii][3]

            Ofile.write('make observation point\n')
            Ofile.write('%s\n%f  %f  %f\n' % (ID, X, Y, Z))
        Ofile.flush()
        print('<Observation wells written ok...')

    if not obswellsFile:
        print('<No observation wells written in grok...>')

    Ofile.close()
    print('GROK file finished!')


def timeStepUpdate(heads, concentrations, update_heads, update_concentrations, subtract=False):
    """ Add/subtract a time value in the outputtimes_xxx.dat files
    Arguments:
        heads:                  Boolean, add time step to heads or not
        concentrations:         Boolean, add time step to concentrations or not
        update_heads:           Integer, how many time steps should be added to heads
        update_concentrations:  Integer, how many time steps should be added to concentrations
        subtract:               Str, if it is wanted to delete last time step from file (for restart mode). Default False
    Returns:
        Length of the current times being analyzed
    """
    KalmanFilterPath = 'kalmanfilter'

    myCodeDir = dm.myMainDir()
    myPath = os.path.join(myCodeDir, '..', KalmanFilterPath, 'Model_data')

    if heads == True:
        AllTimes = np.loadtxt(os.path.join(myPath, '_outputtimesFlow.dat'))
        len_curTimes = len(np.loadtxt(os.path.join(myPath, '_outputtimesFlowUpdate.dat')))

        if subtract is False:
            len_curTimes += update_heads
            with open(os.path.join(myPath, '_outputtimesFlowUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' % AllTimes[len_curTimes - 1])  # to correct the zero index from python
            mytext.close()
        elif subtract is True:
            len_curTimes -= update_heads
            w = open(os.path.join(myPath, '_outputtimesFlowUpdate.dat'), 'w')
            for zz in range(0, len_curTimes):
                if zz < len_curTimes - 1:  # -1 is to avoid writting an empty line
                    w.write('%s\n' % AllTimes[zz])
                else:
                    w.write('%s' % AllTimes[zz])
            w.close()

    if concentrations is True:
        AllTimes1 = np.loadtxt(os.path.join(myPath, '_outputtimesTrans.dat'))
        len_curTimes1 = len(np.loadtxt(os.path.join(myPath, '_outputtimesTransUpdate.dat')))

        if subtract is False:
            len_curTimes1 += update_concentrations
            with open(os.path.join(myPath, '_outputtimesTransUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' % AllTimes1[len_curTimes1 - 1])  # to correct the zero index from python
            mytext.close()
        if subtract is True:
            len_curTimes1 -= update_concentrations
            w = open(os.path.join(myPath, '_outputtimesTransUpdate.dat'), 'w')
            for zz in range(0, len_curTimes1):
                if zz < len_curTimes1 - 1:  # -3 is to avoid writting an empty line
                    w.write('%s\n' % AllTimes1[zz])
                else:
                    w.write('%s' % AllTimes1[zz])
            w.close()

    print('Times updated...')

    if (heads is True) and (concentrations is True):
        return len_curTimes + len_curTimes1
    elif (concentrations is False) and (heads is True):
        return len_curTimes
    elif (concentrations is True) and (heads is False):
        return len_curTimes1


def setParam2value():
    print('construction...')
