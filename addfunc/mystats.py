""" Functions to evaluate Statistics of a data set and transform data """
import numpy as np
import scipy.stats as sst
import scipy.interpolate as si
import time
import scipy


def absolute_error(x, x_true):
    """ Compute the absolute error between two arrays.
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x:       array of data
        x_true : array of true data
    Return:
        error : array of error

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> absolute_error(x = model_data, x_true = observed_data)
    array([-0.2,  0.1, -0.2, -0.3,  0.1])
    """
    # compute error
    error = x - x_true
    return np.abs(error)


# noinspection PyTypeChecker
def mean_squared_error(x, x_true):
    """ Compute the mean square error between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        mse : value of mean square error

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> mean_squared_error(x = model_data, x_true = observed_data)
    0.038000000000000228
    """
    # compute error
    error = x - x_true
    mse = np.mean(error ** 2)
    return mse


def relative_error(x, x_true):
    """ Compute the relative change between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        error : array of relative change

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> relative_error(x = model_data, x_true = observed_data)
    array([-0.00359066,  0.0016129 , -0.00305344, -0.00463679,  0.00163666])
    """
    # compute relative error
    error = (x - x_true) / x_true
    return error


def percent_error(x, x_true):
    """ Compute the percent error between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        error : array of error
    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> percent_error(x = model_data, x_true = observed_data)
    array([-0.35906643,  0.16129032, -0.30534351, -0.46367852,  0.16366612])
    """
    # compute percent error
    error = relative_error(x, x_true) * 100
    return error


def percent_difference(x, x_true):
    """ Compute the percent difference between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        percent_diff : array of error
    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> percent_difference(x = model_data, x_true = observed_data)
    array([-0.35971223,  0.16116035, -0.3058104 , -0.464756  ,  0.1635323 ])
    """
    # compute percent difference
    avg = np.average((x, x_true), axis=0)
    percent_diff = ((x - x_true) / avg) * 100
    return percent_diff


def r_squared(modeled, observed):
    """ Compute the Coefficient of Determination. Used to indicate how well
        data points fit a line or curve. Use numpy.coeff for computation.
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        modeled : array of modeled values
        observed : array of observed value
    Return:
        coefficient : model efficiency coefficient
    *Example:*
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> r_squared(modeled = model_data, observed = observed_data)
    0.99768587638100936
    """
    r = np.corrcoef(modeled, observed)[0, 1]
    coefficient = r ** 2
    return coefficient


def nash_sutcliffe(modeled, observed):
    """ Compute the Nash-Sutcliffe (model efficiency coefficient). Used to
        assess the predictive power of hydrological models.
        E = 1 - sum((observed - modeled) ** 2)) / (sum((observed - mean_observed)**2 )))
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        observed:   array of observed (field) values
        modeled:    array of modeled values
    Return:
        coefficient : model efficiency coefficient

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> nash_sutcliffe(modeled = model_data, observed = observed_data)
    0.99682486631016043
    """

    # compute mean value of the observed array
    mean_observed = np.median(observed)

    # compute numerator and denominator
    numerator = sum((observed - modeled) ** 2)
    denominator = sum((observed - mean_observed) ** 2)

    # compute coefficient
    coefficient = 1 - (numerator / denominator)

    return coefficient


def one_d_stats(a):
    """ Purpose: Define 1D statistics of the dataset. Taken from Claus Haslauer 2014.
    Arguments:
        a: Float array, numpy array containing the dataset
     Returns:
        oned_stats: List, containing [nPts, mean, var, stdd, skew,
        kurt, cv, mi, ma, median]  """
    nPts = a.shape[0]
    mean = np.mean(a)
    var = np.var(a)
    stdd = np.sqrt(np.var(a))
    skew = sst.skew(a)
    kurt = sst.kurtosis(a)
    cv = stdd / mean
    mi = a.min()
    ma = a.max()
    median = np.median(a)

    oned_stats = [nPts, mean, var, stdd, skew, kurt, cv, mi, ma, median]

    return oned_stats


def print_one_d_stats(oned_stats):
    """ Purpose: Print 1D statistics of the dataset. Taken from Claus Haslauer 2014.
    Arguments:
        oned_stats:     list, list of the stats estimated with function < one_d_stats >
    Returns:
        No value returned. Prints results in a screen"""

    print('nPts: \t   %i' % oned_stats[0])
    print('mean: \t %11.9e' % oned_stats[1])
    print('var: \t %11.9e' % oned_stats[2])
    print('stdD: \t %11.9e' % oned_stats[3])
    print('skew: \t %11.9e (gauss=0)' % oned_stats[4])
    print('curt: \t %11.9e' % oned_stats[5])
    print('CV: \t %11.9e' % oned_stats[6])
    print('min: \t %11.9e' % oned_stats[7])
    print('max: \t %11.9e' % oned_stats[8])
    print('median:  %11.9e' % oned_stats[9])


def fitDistMoments(a, moments):
    """ Purpose: Fit a normal and exponential distribution to the data, using
    the method of moments (i.e. using mean and stddev from dataset. Taken from
    Claus Haslauer 2014.
    Arguments:
        a: Float, array containing the dataset
        moments: Tuple, mean (first) and standard deviation (sencond) of the dataset
    Returns:
        norm_pdf: array, probability values for a normal distribution
        exp_pdf: array, probability values for an exponential distribution
    """
    xe = np.linspace(np.min(a), np.max(a), 500)
    norm_pdf = sst.norm.pdf(xe, loc=moments[0], scale=moments[1])
    exp_pdf = sst.expon.pdf(xe, scale=moments[0])

    return norm_pdf, exp_pdf, xe


def Pearson(X, Y):
    """ Purpose: Compute Pearson correlation coefficient.
    Arguments:
        X: np.array, array containing X data
        Y: np.array, array containing Y data
    Returns:
        Pears: Pearson?s correlation coefficient
    """
    Pears = sst.pearsonr(X, Y)

    return Pears


def KolgoSmir(X,Y):
    """ From Scipy.stats: Computes the Kolmogorov-Smirnov statistic on 2 samples. This is a two-sided test for the
    null hypothesis that 2 independent samples are drawn from the same continuous distribution.
    Args:
        X: np.array, array containing X data
        Y: np.array, array containing Y data

    Returns:
        ks_stat: KS statistic
    Notes: (from Scipy.stats
        This tests whether 2 samples are drawn from the same distribution. Note that, like in the case of the
        one-sample K-S test, the distribution is assumed to be continuous. This is the two-sided test, one-sided
        tests are not implemented. The test uses the two-sided asymptotic Kolmogorov-Smirnov distribution. If the K-S
        statistic is small or the p-value is high, then we cannot reject the hypothesis that the distributions of
        the two samples are the same.
    """
    ks_stat = sst.ks_2samp(X,Y)

    return ks_stat

def poly_fit(Xdata, Ydata, orderPol=1, Plot=False):
    """ Purpose: Perform a linear regression using polyfit (with Least squares).
    Arguments:
        Xdata:      float, array containing X data
        Ydata:      float, array containing Y data
        orderPol:   int, degree of the polynomial function to fit
        Plot:       bool, plot results or not
    Returns:
        pearson_coef: Pearsons correlation coefficient
    """
    Fit_line = np.polyfit(Xdata, Ydata, orderPol)
    p = np.poly1d(Fit_line)
    pearson_coef = Pearson(Xdata, Ydata)

    # line_XY45 = np.arange(np.min(Xdata)-0.1,np.max(Xdata)+0.1,0.001)
    line_XY45 = np.linspace(np.min(Xdata) - 0.1, np.max(Xdata), 1000)

    # if Plot is True:
    #     plt.plot(Xdata, Ydata, 'wo', markersize=4)
    #     # plt.plot(line_XY45,line_XY45,'b-',linewidth = 2, label = 'perfect fit line')
    #     # plt.plot(Xdata,Xdata*Fit_line[0]+Fit_line[1],'k-',alpha = 1, label = 'regression line')
    #     plt.plot(line_XY45, p(line_XY45), 'k-', alpha=1, label='regression line')
    #     plt.grid()
    #     plt.xlim([np.min(Xdata) - 0.01, np.max(Xdata) + 0.01])
    #     plt.ylim([np.min(Xdata) - 0.01, np.max(Xdata) + 0.01])
    #     plt.ylabel('Modeled heads [m]')
    #     plt.xlabel('Field heads[m]')
    #     # plt.legend(numpoints=1)
    #     plt.show()
    #     plt.close()

        # print("Correlation factor (Pearson's): %5.4e" % (pearson_coef[0]))
        # print Fit_line
    return pearson_coef[0]


# noinspection PyTypeChecker,PyTypeChecker,PyTypeChecker
def GausAnam(data, jj, myplot=False, oneDstats=True):
    """
    Apply Gaussian Anamorphosis (or Rank transformation) to a given
    dataset. Mean = 0, Std = 1.
        Arguments:
    data:       Array_like, array to transform. Dim = (NoMeas x 1)
    jj:         Str or Int, name or number of the dataset (just for plotting)
    myplot:     Boolean, plot or not the histograms, anamorphosis function,
                and probability plot of original and transformed data
    oneDstats:  Boolean, estimate 1d - statistics of original and transformed
                data. If true, also a fitting to the exponential and normal
                distribution is plotted using the method of moments.
        Returns:
    transfdata[backsortid_data]:    np.array, transformed data in the proper order
    Transform_function:             scipy.interpolate object, Anamorphosis function
        Example:
    >>> import numpy as np
    >>> ydata = np.load(r'..\ModMeas_tr_iter001_timestep002.npy')
    >>> jj = 13
    >>> data = mydata[jj, :]
    >>> transfdata, Transform_function = mysst.GausAnam(data, jj, myplot=True, oneDstats=True)
    """
    #    scipy.random.seed()
    # Construction of the empirical anamorphosis function:
    # ----------------------------------------------------

    # 1.) Calculation of ranks:
    # Sort and back-sort ids for the original data:
    sortid_data = np.argsort(data)
    backsortid_data = np.argsort(sortid_data)
    # data = data[sortid_data][backsortid_data]   # data back sorted, just as a test...

    # Ranks for the data:
    quantilerange = 1.0 / len(data)  # Range of a quantile is 1 point value
    ranks_norm_data = np.arange(quantilerange, 1.0 + quantilerange, quantilerange)
    stdranks_data = ranks_norm_data - (0.5 * quantilerange)

    # Get the Gaussian distribution values:
    Gaus = np.random.standard_normal(size=(len(data)) * 1000)  # (mean=0, stdev=1)
    sortid_Gaus = np.argsort(Gaus)
    # backsortid_Gaus = np.argsort(sortid_Gaus)
    Gaus = Gaus[sortid_Gaus]

    #  Ranks for Gaussian Distribution:
    quantilerange_Gauss = 1.0 / len(Gaus)
    ranks_norm_Gauss = np.arange(quantilerange_Gauss, 1.0 + quantilerange_Gauss, quantilerange_Gauss)
    stdranks_gaus = ranks_norm_Gauss - (0.5 * quantilerange_Gauss)

    # 2.) Add 0.9*min(data) and 1.1*max(data) and their corresponding 0 and 1 rank values
    #   data = np.append(1.1 * np.min(data), data)
    #   data = np.append(data, 0.9 * np.max(data) )

    #   stdranks_data = np.append(0.0, stdranks_data)
    #   stdranks_data = np.append(stdranks_data, 1.)

    stdranks_gaus = np.append(0.0, stdranks_gaus)
    stdranks_gaus = np.append(stdranks_gaus, 1.0)

    Gaus = np.append(0.9 * np.min(Gaus), Gaus)  # np.append(-4.0,Gaus)
    Gaus = np.append(Gaus, 1.1 * np.max(Gaus))  # np.append(Gaus,4.0)#

    # 3.) Interpolation of the empirical anamorphosis function:
    # ------------------------------------------------------
    InterpGaus = si.interp1d(stdranks_gaus, Gaus)
    transfdata = InterpGaus(stdranks_data)
    #   transfdata = transfdata[1:-1]

    # 4.) Get the Anamorphosis function, able to extrapolate:
    # up to here everything is calculated over sorted arrays...
    Transform_function = si.InterpolatedUnivariateSpline(data[sortid_data], transfdata, k=1)

    # 5.) Additional options:
    if oneDstats is True:
        stats_data = one_d_stats(data)
        print('\nStatistics for original data:')
        print_one_d_stats(stats_data)

        stats_transfdata = one_d_stats(transfdata)
        print('\nStatistics of transformed data:')
        print_one_d_stats(stats_transfdata)

        moments = [stats_data[1], stats_data[3]]
        norm_pdf, exp_pdf, xe = fitDistMoments(data, moments)

    if myplot is True:
        import matplotlib.pyplot as plt
        fig = plt.figure(1)
        ax1 = fig.add_subplot(221)
        ax1.hist(data, bins=11, normed=True, stacked=True)
        ax1.plot(xe, norm_pdf, '-', c='red', label='normal')
        ax1.plot(xe, exp_pdf, '-', c='black', label='exp')
        ax1.set_title('Raw data: Realization %s' % jj)
        plt.grid(True)

        ax2 = fig.add_subplot(222)
        ax2.plot(data, Transform_function(data), 'ro', ms=3)
        ax2.set_title('Anamorphosis Function')
        #            ax2.plot( data[1:-1],Gaus[1:-1],'k.')
        ax2.set_xlabel('Raw data')
        ax2.set_ylabel('Transformed data')
        plt.grid(True)

        ax3 = fig.add_subplot(223)
        ax3.hist(transfdata, bins=11, normed=True,  stacked=True)
        momentstr = [stats_transfdata[1], stats_transfdata[3]]
        norm_pdftr, exp_pdftr, xetr = fitDistMoments(transfdata, momentstr)
        ax3.plot(xetr, norm_pdftr, '-', c='red', label='normal')
        ax3.set_title('Transformed data')
        plt.grid(True)

        ax4 = fig.add_subplot(224)
        sst.probplot(transfdata, plot=plt)
        ax4.set_title('Probability plot')
        plt.grid(True)

        # Plot the fitted distributions:
        if oneDstats is True:
            fig2 = plt.figure(2)
            ax11 = fig2.add_subplot(111)
            ax11.hist(data, normed=True, bins=11)
            # ax1.hist(data, bins=11)
            ax11.set_title('Fit a normal and exp distribution')
            ax11.plot(xe, norm_pdf, '.-', c='red', label='normal')
            ax11.plot(xe, exp_pdf, '.-', c='black', label='exp')
            plt.grid(True)
            plt.legend(loc='best')
            plt.show(2)
            plt.close(2)
        plt.show(1)
        plt.close(1)

    return transfdata[backsortid_data], Transform_function


def areacurve(ydata, xdata=None, dx=1.0, func='trapz'):
    """Calculate the area under the curve
    Args:
        ydata:      array, input to integrate
        xdata:      list or np array, optional, if not evenly spaced series will be assumed. Default is None
        dx:         scalar, spacing between sample points when xdata is None. Default is 1
        func:       string, 'trapz' or 'simps', method to calculate the area. Default is trapz
    Returns:
        myarea:     float, area under the curve (Definite integral as approximated by trapezoidal rule)
    Examples:
    >>> np.trapz([1,2,3])
    >>> np.trapz([1,2,3], x=[4,6,8])
    """
    if func is 'trapz':
        myarea = np.trapz(ydata, x=xdata, dx=dx)

    elif func is 'simps':
        from scipy.integrate import simps
        myarea = simps(ydata, x=xdata, dx=dx)

    return myarea


def invVertWeib(a, b, x0, xVect):
    """ Purpose: Transform gstat output to kkk values using the inverse Weibul
    distribution. From Claus Haslauer, 2014.
    Arguments:
        a:      float, scale parameter
        b:      float, shape parameter
        x0:     float, shift parameter
        xVect:  array, function with the values to evaluate at
    Returns:
        Numpy array with transformed values
    """
    invWeib = (np.log(1.0 - (xVect - x0)) / (-a)) ** (1.0 / b)

    return invWeib


def param_bias(X, Y):
    """ Calculate the parameter/prediction bias. Quantification of the deviation between "true" and conditioned
    parameters. Definition from Schoeniger 2012
    Args:
        X:
        Y:
    Returns:
        Scalar representing the parameter bias
    """
    parambias = np.sqrt(np.sum((X - Y)**2) / X.shape[0])

    return parambias


def tot_var(X, meanVal):
    """ Calculate the total prediction variance of the conditioned parameter fields. Equals to the A criterion of
    optimal design. Measure for the absorbed information in spatial estimation
    Args:
        X:
        meanVal:
    Returns:

    """
    subt = np.sum((X - meanVal)**2) / (X.shape[0] * X.shape[1])

    return subt


def calc_tempMoments(X, Y):
    """
    Calculate seven different temporal moments: Zero_m, First_m, First_mN, Second_mC, Second_mCN, Third_mC, Third_MCN,
    Args:
        X:
        Y:
    Returns:
        numpy array with the seven different calculated temporal moments
    """
    mom_array = np.empty((7,))
    dataObj_ow = Miscel(X, Y)
    mom_array[0, ] = dataObj_ow.tempmoments(degree=0, central=False, full_output=False)  # Area under the curve
    mom_array[1, ] = dataObj_ow.tempmoments(degree=1, central=False, full_output=False)
    mom_array[2, ] = mom_array[1, ] / mom_array[0, ]  # Mean ArrivalTime
    mom_array[3, ] = dataObj_ow.tempmoments(degree=2, central=True, full_output=False)
    mom_array[4, ] = mom_array[3, ] / mom_array[0, ]  # Variance in Travel time, spread of the BTC
    mom_array[5, ] = dataObj_ow.tempmoments(degree=3, central=True, full_output=False)
    mom_array[6, ] = mom_array[5, ] / mom_array[0, ]  # Skewness of the BTC in t*2 units

    return mom_array


def mean_abs_error(meanX, origX):
    """ Mean absolute error
    Args:
        meanX:
        origX:
    Returns:
    """
    try:
        origX.shape[1]
    except IndexError:
        origX = np.expand_dims(origX, axis=1)
    try:
        meanX.shape[1]
    except IndexError:
        meanX = np.expand_dims(meanX, axis=1)

    abs_residual = np.abs(meanX - origX)
    AAE = np.sum(abs_residual) / (abs_residual.shape[0])

    return AAE


def mean_ensemble_std(X, meanX):
    """ Average ensemble standard deviation
    Args:
        X:
        meanX:
    Returns:
    """
    try:
        meanX.shape[1]
    except IndexError:
        meanX = np.expand_dims(meanX, axis=1)
    abs_residual = np.abs(X - meanX)
    AESD = np.sum(abs_residual) / (abs_residual.shape[0] * abs_residual.shape[1])

    return AESD


def plotCopulas():
    print('under construction')


def calcDistanceMatrix(pointsA, pointsB):
    """ Calculate euclidean distances between all points of two sets.
    pointsA and pointsB are (npoints x dimension) numpy arrays containing
    the cartesian coordinates of the points (one point per row)
    The number of points in pointsA and pointsB need not be the same.
    The result will be a (npointsA x npointsB)-numpy array
    Created by Claus Haslauer 2016.
    Examples:
    in 2-D
    >>> pointsA = np.array([[0.0, 0.0], [1.0, 0.0]])
    >>> pointsB = np.array([[0.0, 1.0], [1.0, 1.0]])
    >>> calcDistanceMatrix(pointsA, pointsB)
    array([[ 1.        ,  1.41421356],
           [ 1.41421356,  1.        ]])

    in 3-D
    >>> pointsA = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]])
    >>> pointsB = np.array([[0.0, 1.0, 0.0], [1.0, 1.0, 0.0]])
    >>> calcDistanceMatrix(pointsA, pointsB)
    array([[ 1.        ,  1.41421356],
           [ 1.41421356,  1.        ]])

    With different numbers of points
    >>> pointsA = np.array([[0.0, 0.0]])
    >>> pointsB = np.array([[0.0,1.0], [1.0, 1.0]])
    >>> calcDistanceMatrix(pointsA, pointsB)
    array([[ 1.        ,  1.41421356]])

    Can also be used to calculate all distances within the same set of points
    >>> points = np.array([ [1.,1.],[-1.,-1.],[0.5,-1.1],[-0.8,0.9]])
    >>> calcDistanceMatrix(points, points)
    array([[ 0.        ,  2.82842712,  2.15870331,  1.80277564],
           [ 2.82842712,  0.        ,  1.50332964,  1.91049732],
           [ 2.15870331,  1.50332964,  0.        ,  2.38537209],
           [ 1.80277564,  1.91049732,  2.38537209,  0.        ]])

    Points should be of the same dimension
    >>> pointsA = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]])
    >>> pointsB = np.array([[0.0, 1.0], [1.0, 1.0]])
    >>> calcDistanceMatrix(pointsA, pointsB)
    Traceback (most recent call last):
    ...
    AssertionError: Dimension of points has to be the same (dimA=3) != (2=dimB
    """
    # make sure the dimensions of both point sets are the same
    assert pointsA.shape[1] == pointsB.shape[1], \
        'Dimension of points has to be the same (dimA=%i) != (%i=dimB' % \
        (pointsA.shape[1], pointsB.shape[1])

    # set up some variables
    npointsA = pointsA.shape[0]
    npointsB = pointsB.shape[0]
    dimension = pointsA.shape[1]

    # initialize our result matrix and the helper vectors
    distmatrix = np.zeros((npointsA, npointsB))

    # for each dimension calculate the squared differences and add them to
    # the distance matrix
    for dim in range(dimension):
        # create numpy matrix for points A
        xxa = np.mat(np.ones((2, npointsA)))
        xxa[0, :] = pointsA[:, dim]

        # create numpy matrix for points B
        xxb = np.mat(np.ones((2, npointsB)) * -1)
        xxb[1, :] = pointsB[:, dim]

        # calculate distance matrix summand using matrix product
        # the squaring afterwards has to be done elementwise and therfore
        # the resulting matrix has to be converted to array before doing the
        # power operation
        distmatrix += np.asarray((xxa.T * xxb))**2

    # return the result
    return np.sqrt(distmatrix)


class Miscel(object):
    """ Parametric functions to fit to data
    Arguments:
        xData:       array with x values
        yData:       array with y values
        mytitles:    str, dataset identifier (for plotting)
        plotting:    bool, plot results or not. Default = False
    """

    def __init__(self, xData, yData):
        self.xData = xData
        self.yData = yData

    def tempmoments(self, degree=0, central=False, full_output=False):
        """ Calculate temporal moments of the curve
            central:    bool, estimate central moments or not
        Arguments:
            degree:     int, degree of the central moment to calculate. discarded if central is False
            full_output:    bool, return full output or not
        Returns:
            if full_output is True
                            mom: temporal (or central if True) moment
                            mom_0:zeroth temp moment
                            mom_1:first temp moment
                            t_mean: Normalized first moment, mean travel time
            if full_output is False
                            temporal moment of defined degree
        """
        dt = np.append(self.xData[0], np.diff(self.xData))
        if central is False:
            mom = np.sum((self.xData ** degree) * self.yData * dt)
            return mom
        elif central is True:
            mom_0 = np.sum((self.xData ** 0) * self.yData * dt)
            mom_1 = np.sum((self.xData ** 1) * self.yData * dt)
            t_mean = mom_1 / mom_0  # Normalized first moment: mean travel time
            mom = np.sum(((self.xData - t_mean) ** degree) * self.yData * dt)

            if full_output is False:
                return mom

            if full_output is True:
                return mom, mom_0, mom_1, t_mean

    def areacurve(self, dx=1.0, func='trapz'):
        """Calculate the area under the curve
        Args:
            ydata:      array, input to integrate
            xdata:      list or np array, optional, if not evenly spaced series will be assumed. Default is None
            dx:         scalar, spacing between sample points when xdata is None. Default is 1
            func:       string, 'trapz' or 'simps', method to calculate the area. Default is trapz
        Returns:
            myarea:     float, area under the curve (Definite integral as approximated by trapezoidal rule)
        Examples:
        >>> np.trapz([1,2,3])
        >>> np.trapz([1,2,3], x=[4,6,8])
        """
        if func is 'trapz':
            myarea = np.trapz(self.yData, x=self.xData, dx=dx)

        elif func is 'simps':
            from scipy.integrate import simps
            myarea = simps(self.yData, x=self.xData, dx=dx)

        return myarea
