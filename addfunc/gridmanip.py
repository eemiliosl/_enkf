""" Functions to manipulate a grid """
import sys
import os
import numpy as np
import time
import pdb
import platform
import dirs as dm


# def readmesh(my_meshfile, fulloutput=False, gridformat='tecplot'):
#     """ Read relevant information from model grid. It supports reading file with Tecplot and gms formats.
#     Args:
#         my_meshfile:    str, full directory of the grid source file
#         fulloutput:     bool, optional. If True, returns also the actual nodes coordinates and nodes number for each element
#         gridformat:     str, optional.'tecplot' or 'gms'. Specifies the format of the grid source file.
#
#     Returns:
#         nnodes, nelem, nodes, elements
#     """
#     assert os.path.exists(my_meshfile), 'Mesh file does not exists'
#     if gridformat == 'tecplot':
#         nnodes = int(dm.str_infile(my_meshfile, 'N='))
#         nelem = int(dm.str_infile(my_meshfile, 'E='))
#     elif gridformat == 'gms':
#         temp1 = np.genfromtxt(my_meshfile, skip_header=4, dtype=None, usecols=(0,))
#         nnodes = len(np.where(temp1 == b'ND')[0])
#         nelem = len(np.where(temp1 == b'E8H')[0])
#
#     if fulloutput is True:
#         if gridformat == 'tecplot':
#             nodes_coord = np.genfromtxt(my_meshfile, skip_header=3, skip_footer=nelem, usecols=(0, 1, 2))
#             element_nodes = np.loadtxt(my_meshfile, skiprows=nnodes + 3)
#         elif gridformat == 'gms':
#             nodes_coord = np.loadtxt(my_meshfile, skiprows=4+nelem, usecols=(2, 3, 4))
#             element_nodes = np.genfromtxt(my_meshfile, skip_header=4, skip_footer=nnodes, usecols=(2, 3, 4, 5, 6, 7, 8, 9))
#         assert element_nodes.shape[0] == nelem, 'Mismatch with element number'
#         assert nodes_coord.shape[0] == nnodes, 'Mismatch with nodes number'
#
#     if fulloutput is True:
#         return nnodes, nelem, nodes_coord, element_nodes
#     elif fulloutput is False:
#         return nnodes, nelem


def readmesh(my_meshfile, fulloutput=False, gridformat='tp2018'):
    """ Read relevant information from model grid. Supports files with Tecplot and gms formats.
    :param my_meshfile:    str, full directory of the grid source file
    :param fulloutput:     bool, opt. If True, returns also the actual nodes coordinates and nodes number for each element
    :param gridformat:     str, format of the grid source file. ['tp2014','tp2018', 'gms'], Default is 'tp2018'
    :return:    nnodes, nelem. If fulloutput is True also nodes, elements
    """
    assert os.path.exists(my_meshfile), 'Mesh file does not exists'
    print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Start reading GROK mesh')
    if 'tp' in gridformat:
        nnodes = int(dm.str_infile(my_meshfile, 'N='))
        nelem = int(dm.str_infile(my_meshfile, 'E='))

    elif gridformat == 'gms':
        temp1 = np.genfromtxt(my_meshfile, skip_header=4, dtype=None, usecols=(0,))
        nnodes = len(np.where(temp1 == b'ND')[0])
        nelem = len(np.where(temp1 == b'E8H')[0])

    if fulloutput is True:
        if gridformat == 'tp2014':
            nodes_coord = np.genfromtxt(my_meshfile, skip_header=3, skip_footer=nelem, usecols=(0, 1, 2))
            element_nodes = np.loadtxt(my_meshfile, skiprows=nnodes + 3)
        elif gridformat == 'tp2018':
            ids, next_str = dm.str_infile(my_meshfile, '#', index=True)
            temp1 = open(my_meshfile, 'r').readlines()
            nodes_coord = np.empty((nnodes, 3))

            X = temp1[ids[0] + 1:ids[1]]
            Y = temp1[ids[1] + 1:ids[2]]
            Z = temp1[ids[2] + 1:ids[3]]
            element_nodes = np.genfromtxt(my_meshfile,skip_header=(len(temp1)-nelem), dtype=int)
            for ii, cur_val in enumerate(X):
                X[ii] = np.fromstring(cur_val, sep=' ')
            for ii, cur_val in enumerate(Y):
                Y[ii] = np.fromstring(cur_val, sep=' ')
            for ii, cur_val in enumerate(Z):
                Z[ii] = np.fromstring(cur_val, sep=' ')

            nodes_coord[:, 0] = np.concatenate(X, axis=0)
            nodes_coord[:, 1] = np.concatenate(Y, axis=0)
            nodes_coord[:, 2] = np.concatenate(Z, axis=0)

        elif gridformat == 'gms':
            nodes_coord = np.loadtxt(my_meshfile, skiprows=4+nelem, usecols=(2, 3, 4))
            element_nodes = np.genfromtxt(my_meshfile, skip_header=4, skip_footer=nnodes, usecols=(2, 3, 4, 5, 6, 7, 8, 9))
        assert element_nodes.shape[0] == nelem, 'Mismatch with element number'
        assert nodes_coord.shape[0] == nnodes, 'Mismatch with nodes number'

    print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Finished reading GROK mesh')
    if fulloutput is True:
        return nnodes, nelem, nodes_coord, element_nodes
    elif fulloutput is False:
        return nnodes, nelem


def sel_grid(in_gridfile, xlim, ylim, zlim, store=False, return_val=True, fulloutput=False, gridformat = 'tp2018'):
    """ Select grid elements within area of interest (and param value) based on element
        centroids, to create a kkk-file for running the transport model using only the
        small grid. Elements selected based on X, Y, Z coord.
    Arguments:
    ---------
        in_gridfile:        str, full path/filename with grid info from GROK (e.g. meshtecplot.dat)
        xlim, ylim, zlim:   np array, ini and final X, Y, or Z coordinate of the small area (e.g. np.array([30, 70]))
        store:              bool, store subgrid information in binary files for easy access. Default is False
        return_val:         bool, return subgrid information if True. Default is False.
    Returns:
    --------
        new_param_ind:      np.array, index of the (elemental) parameter values within area of interest
        new_K:              np.array, parameter values within are of interest
        elem_ind_fromorig:  np.array, indices of the param within are of interest WITHIN original indices listdir
        new_nodes:          np.array, nodes within area of interest
    """
    # Load grid or mesh data:
    nodes, elements, nodes_coord, element_nodes = readmesh(in_gridfile, fulloutput=True, gridformat = gridformat)
    # %% Load parameter data #and heads:
    # orig_K_ids, orig_K = np.loadtxt(in_paramfile, usecols =(0,1), unpack = True)
    # orig_K_ids = np.arange(1,elements+1, 1)
    # heads, time = grokhgs.readHeadsBin(myfile, nodes)

    # %% Obtain centroids of each element:
    element_centroid = np.empty((element_nodes.shape[0], 3))
    print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Selecting inner domain of the grid...')
    for ii in range(0, element_nodes.shape[0], 1):
        idx1 = int(element_nodes[ii, 0])
        idx2 = int(element_nodes[ii, 1])
        idx3 = int(element_nodes[ii, 2])
        idx4 = int(element_nodes[ii, 3])
        idx5 = int(element_nodes[ii, 4])
        idx6 = int(element_nodes[ii, 5])
        idx7 = int(element_nodes[ii, 6])
        idx8 = int(element_nodes[ii, 7])
        node1 = nodes_coord[idx1 - 1, :]
        node2 = nodes_coord[idx2 - 1, :]
        node3 = nodes_coord[idx3 - 1, :]
        node4 = nodes_coord[idx4 - 1, :]
        node5 = nodes_coord[idx5 - 1, :]
        node6 = nodes_coord[idx6 - 1, :]
        node7 = nodes_coord[idx7 - 1, :]
        node8 = nodes_coord[idx8 - 1, :]

        face1 = np.vstack((node1, node2, node3, node4))
        # face2 = np.vstack((node5, node5, node6, node8))
        # Is this wrong?
        face2 = np.vstack((node5, node6, node7, node8))
        centroid = np.vstack((np.mean(face1, axis=0), np.mean(face2, axis=0)))
        element_centroid[ii, :] = np.mean(centroid, axis=0)

    # %% Select those elements within the X,Y,Z limits defined above:
    center_smallgrid_ids = np.zeros((1, 1))
    for ii in range(0, len(element_centroid), 1):

        if all([element_centroid[ii, 0] >= xlim[0], element_centroid[ii, 0] <= xlim[1],
                element_centroid[ii, 1] >= ylim[0], element_centroid[ii, 1] <= ylim[1],
                element_centroid[ii, 2] >= zlim[0], element_centroid[ii, 2] <= zlim[1]]):
            center_smallgrid_ids = np.vstack((center_smallgrid_ids, ii))

    center_smallgrid_ids = center_smallgrid_ids[1:].astype('int')
    # Get node number of (nodal) parameter values within area of interest
    smallgrid_nodes = np.unique(element_nodes[center_smallgrid_ids, :].flatten()).astype('int')

    print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Finish selecting inner domain of the grid...')
    if store is True:
        np.save(os.path.join(os.path.split(in_gridfile)[0], 'subGrid.nodes'), smallgrid_nodes)
        np.save(os.path.join(os.path.split(in_gridfile)[0], 'subGrid.elements'), center_smallgrid_ids.flatten())
        print('Subgrid information stored in %s' % os.path.split(in_gridfile)[0])

    # new_K = orig_K[center_smallgrid_ids-1]
    # new_param_ind = orig_K_ids[0:len(new_K)]
    # This array let me select directly the parameters of the area if interest directly from KKK file
    # smallgrid_elem = orig_K_ids[center_smallgrid_ids-1]
    # pdb.set_trace()
    if return_val is True:
        if fulloutput is True:
            return nodes, elements, nodes_coord, element_nodes, smallgrid_nodes, center_smallgrid_ids.flatten()
        elif fulloutput is False:
            return smallgrid_nodes, center_smallgrid_ids.flatten()
    else:
        return
    # return new_param_ind, new_K, smallgrid_elem, smallgrid_nodes


def interpToirregGrid(gridfile, nxel, nyel, nzel, xlim, ylim, zlim):

    nnodes_big, elements_big, nodes_coord, element_nodes = readmesh(gridfile, fulloutput=True)
    element_centroid = np.empty((element_nodes.shape[0], 3))

    for ii in range(0, element_nodes.shape[0], 1):
        idx1 = int(element_nodes[ii, 0])
        idx2 = int(element_nodes[ii, 1])
        idx3 = int(element_nodes[ii, 2])
        idx4 = int(element_nodes[ii, 3])
        idx5 = int(element_nodes[ii, 4])
        idx6 = int(element_nodes[ii, 5])
        idx7 = int(element_nodes[ii, 6])
        idx8 = int(element_nodes[ii, 7])
        node1 = nodes_coord[idx1 - 1, :]
        node2 = nodes_coord[idx2 - 1, :]
        node3 = nodes_coord[idx3 - 1, :]
        node4 = nodes_coord[idx4 - 1, :]
        node5 = nodes_coord[idx5 - 1, :]
        node6 = nodes_coord[idx6 - 1, :]
        node7 = nodes_coord[idx7 - 1, :]
        node8 = nodes_coord[idx8 - 1, :]

        face1 = np.vstack((node1, node2, node3, node4))
        # face2 = np.vstack((node5, node5, node6, node8))
        # Is this wrong?
        face2 = np.vstack((node5, node6, node7, node8))
        centroid = np.vstack((np.mean(face1, axis=0), np.mean(face2, axis=0)))
        element_centroid[ii, :] = np.mean(centroid, axis=0)

    xx = np.arange(xlim[0] + (xlim[1] - xlim[0]) / nxel, xlim[1] + (xlim[1] - xlim[0]) / nxel,
                   (xlim[1] - xlim[0]) / nxel)
    yy = np.arange(ylim[0] + (ylim[1] - ylim[0]) / nyel, ylim[1] + (ylim[1] - ylim[0]) / nyel,
                   (ylim[1] - ylim[0]) / nyel)
    zz = np.arange(zlim[0] + (zlim[1] - zlim[0]) / nzel, zlim[1] + (zlim[1] - zlim[0]) / nzel,
                   (zlim[1] - zlim[0]) / nzel)

    XX, YY, ZZ = np.meshgrid(xx, yy, zz)
    reggrid_cartesian_coordinates = np.array((XX.flatten(), YY.flatten(), ZZ.flatten())).T
    for i in [0, 1, 2]:
        sorted_idx = np.argsort(reggrid_cartesian_coordinates[:, i], kind='mergsort')  # todo: problem her with the sorting

    return reggrid_cartesian_coordinates[sorted_idx, :], element_centroid


def rotmesh2d(coordinates, myplot=False, save2file=False, forward=True, interactive=True, clockwise=True,
              rotatemarks=True):
    """
    Rotate grid coordinates. Pivot point is the southernmost/westermost point. Rotated grid will be
    aligned with the X axis. Rotation is clockwise in the Cartesian coordinates
        Arguments:
        ----------
    coordinates:    2d np array (x,y)
    myplot:         boolean, True: plot original and rotated grid
    save2file:      str, full path of the file to store rotated coordinates. False if not to save into file
        Returns:
        --------
    2d numpy array of rotated coordinates
    """
    # %% Definition of the southernmost and easternmost point and estimation of rotation angle:
    # Find the pivot at the southernmost point location
    if interactive is False:
        pivot_coord = coordinates[np.where(coordinates[:, 1] == np.min(coordinates[:, 1]))][0]
    elif interactive is True:
        idx1 = pym.pick_points(coordinates, 'Select pivot point', rotatemarks=rotatemarks)
        pivot_coord = coordinates[idx1[0]]

    # Find the easternmost point location, to define the rotation angle
    if interactive is False:
        east_coord = coordinates[np.where(coordinates[:, 0] == np.max(coordinates[:, 0]))][0]
    elif interactive is True:
        print('Choose now the right end of the pivot line...')
        idx2 = pym.pick_points(coordinates, 'Now, the right end of the pivot line', rotatemarks=rotatemarks)
        east_coord = coordinates[idx2[0]]

    # Calculate the angle of rotation
    d = np.sqrt((np.subtract(east_coord[0], pivot_coord[0])) ** 2 + (np.subtract(east_coord[1], pivot_coord[1])) ** 2)
    angle = np.degrees(np.arcsin(np.abs(pivot_coord[1] - east_coord[1]) / d))
    if clockwise is False:
        angle *= -1

    # %% Three matrix operations: back translation, rotation and forward translation:
    # First: back translation
    xyback = np.zeros((len(coordinates), 2))
    for ii in range(0, 2, 1):
        for kk in range(0, len(xyback), 1):
            xyback[kk, ii] = np.subtract(coordinates[kk, ii], pivot_coord[ii])

    # Second: rotation
    xyrot = np.zeros((len(coordinates), 2))

    for ii in range(0, len(coordinates), 1):
        xyrot[ii, 0] = (xyback[ii, 0] * np.cos(-angle * np.pi / 180.) - xyback[ii, 1] * np.sin(-angle * np.pi / 180.))
        xyrot[ii, 1] = (xyback[ii, 0] * np.sin(-angle * np.pi / 180.) + xyback[ii, 1] * np.cos(-angle * np.pi / 180.))

    # Third: forward translation
    if forward is True:
        xyforward = np.zeros((len(coordinates), 2))
        for kk in range(0, 2, 1):
            for ii in range(0, len(xyforward), 1):
                xyforward[ii, kk] = xyrot[ii, kk] + pivot_coord[kk]
    if forward is False:
        xyforward = xyrot

    # if myplot:  # Use myplot module
    #     myplt.xyplot(coordinates[:, 0], coordinates[:, 1], xData_new=xyforward[:, 0], yData_new=xyforward[:, 1],
    #                  mytitle='Original vs rotated points')
    #     if platform.system() == 'Windows':
    #         plt.close()

    if save2file != False:
        try:
            assert os.path.isdir(os.path.split(save2file)[0]), 'Wrong directory'
        except:
            save2file = str(
                input('Type a valid path/filename input: '))  # Here I can use the function to constrain user input

        np.savetxt(save2file, xyforward, fmt='%.6e')

    return xyforward


def expshr_param(Y, biggrid_elem_ids, smallgrid_elem_ids, what=False):
    """
    Expand or shrink parameter vector. Used when it is wanted to work with a
    smaller parameter vector rather than with the parameters of the whole grid
        Arguments:
        ----------
    Y:              np array. Original parameter array
    biggrid_elem_ids:   np array. Indices of the parameter elements
    smallgrid_elem_ids:  np.array. Indices of the selected area taken from the original full grid
    what:           str, 'shrink' or 'expand' depending on what is wanted
                    if shrink, a single mean value will be assigned to an element
                    representing all the elements outside the area of interest
        Returns:
        --------
    np array of parameters. The size depends on what was asked to perform
    """
    if what is False:
        print('Y vector has NOT been modified')
        return Y

    elif (what == 'shrink') or (what == 'expand'):

        nonsel_indices = np.delete(np.subtract(biggrid_elem_ids, 1), smallgrid_elem_ids,
                                   0)  # Do I need the subtract to have python indices?
        # nonsel_indices = np.delete(biggrid_elem_ids-1, smallgrid_elem_ids,0)
        meanVal = np.mean(Y)

        if len(smallgrid_elem_ids) == len(biggrid_elem_ids):
            return Y
            # ind_from_origs = np.delete(biggrid_elem_ids, smallgrid_elem_ids[-1]-1) #ind_from_orig -1
        else:
            # ind_from_origs = np.subtract(smallgrid_elem_ids,1) # To get again from HGS indexing to python indexing
            ind_from_origs = smallgrid_elem_ids

        if what == 'shrink':
            # meanVal = np.mean(Y[nonsel_indices])
            if len(Y) > (len(smallgrid_elem_ids) + 1):
                # Plus one for the mean value added as first element of the array
                # This instruction works if a field is generated for the full grid:
                new_param = np.append(meanVal, Y[ind_from_origs])
            else:
                # This other one applies when the field is generated for the inner-smaller area only:
                new_param = np.append(meanVal, Y)

        elif what == 'expand':
            new_param = np.empty([len(biggrid_elem_ids), ])
            new_param[nonsel_indices] = meanVal

            if len(Y) > (
                len(smallgrid_elem_ids) + 1):  # Plus one for the mean value added as first element of the array
                # This instruction works if a field is generated for the full grid:
                new_param[ind_from_origs] = Y[ind_from_origs]
            else:
                # This other one applies when the field is generated for the inner-smaller area only:
                new_param[ind_from_origs] = Y

                # pdb.set_trace()
        return new_param


def interp2raster(x, y, nXnodes, nYnodes, n_cond, variogram_paras, controlfile='', rstfile='', parallel=False, ncpus='',
                  verbose=False):
    """
    Perform Ordinary kriging and get a raster file with interpolated values
        Arguments:
        ----------
    x:                  np.array containing Xini and Xend points of the grid
    y:                  np.array containing Yini and Yend points of the grid
    nXnodes:            int, number of nodes on X direction
    nYnodes:            int, number of nodes on Y direction
    n_cond:             int, number of conditioning points
    variogram_paras:    list of floats, # [Nug,1.0-Nug, range, Matern-Koeff.]
    controlfile:        str, full directory of the file containing control points (id, x, y, z)
    rstfile:            str, full directory of the output raster file
    parallel:           bool, perform kriging in parallel or not
    ncpus:              int, number of cpus to use in case parallel is True
    verbose:            bool, get a descriptive description of the results or not
        Returns:
        --------
    myrst:              2D numpy array with interpolated values arranged as a raster

    Additionally it stored the interpolated values as a raster file following ArcGIS conventions.
        Example:
        --------
    x = np.array([x0, x1])
    y = np.array([y0, y1])
    nXnodes = nYnodes = 20
    n_cond = 4
    variogram_paras = [0.01,   # Nug
                       0.99,   # 1.0-Nug
                       5.00,   # range
                       0.5]   # Matern-Koeff.)

    """

    # Grab input variables and put them in order:
    x0, x1 = x
    y0, y1 = y

    # Create the grid nodes with proper shape:
    x = np.linspace(x0, x1, nXnodes)
    y = np.linspace(y0, y1, nYnodes)[::-1]
    xv, yv = np.meshgrid(x, y)
    mygrid = np.empty((len(x) * len(y), 2))
    mygrid[:, 0] = xv.flatten('C')
    mygrid[:, 1] = yv.flatten('C')

    # Get control coordinates and values
    control_ids = np.loadtxt = np.loadtxt(controlfile, usecols=(0,))
    controls = np.loadtxt(controlfile, usecols=(1, 2))
    controls = controls[:, np.newaxis, :]

    controlvalues = np.loadtxt(controlfile, usecols=(-1,))

    # Perform interpolation:
    init_time = time.clock()
    myrst = gsi.caller(mygrid, variogram_paras, controls, controlvalues, nXnodes, nYnodes, n_cond, parallel=True,
                       ncpu=4, verbose=True)
    tot_time = (time.clock() - init_time)
    print(tot_time)

    # Save Raster file:
    np.savetxt(myrstfile, myrst, fmt='%6.5e', delimiter=' ',
               header='ncols\t%s\nnrows\t%s\nxllcorner\t%s\nyllcorner\t%s\ncellsize\t%6.5f\nNODATA_value\t-9999\n' % (
               nXnodes, nYnodes, x0, y0, np.abs(x[1] - x[0])), comments='')
    print('Raster file < %s > written.' % myrstfile)
    return myrst


print('nothing')