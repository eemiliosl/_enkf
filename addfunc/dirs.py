"""
Administration of directories and files
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon
"""
import shutil
import os
import numpy as np
import re
import time
import pdb


def homedir():
    """ Obtain automatically the path to the folder one level above the folder
    in which dirs.py is located.
        Arguments: No arguments required
    """
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


def file_info(myfile):
    """ Extract default metadata from any file.
    Args:
        myfile:         str, fullpath and filename of the file to extract metadata from

    Returns:
        creation_time, lastmodified, lastaccessed string formatted, filesize integer in bytes
    """
    info = os.stat(myfile)
    creation_time = time.asctime(time.localtime(info.st_ctime))
    lastmodified = time.asctime(time.localtime(info.st_mtime))
    lastaccessed = time.asctime(time.localtime(info.st_atime))
    filesize = info.st_size

    return creation_time, lastmodified, lastaccessed, filesize


def getdirs(mydir, mystr=None, fullpath=False,
            onlylast=False):
    """ Get a list of folders/files in a defined directory, with a common string
        Arguments:
    mydir:      str, dir of interest
    mystr:      str, common string of files. Default None
    fullpath:   bool, whether to attach full directory to file names or not. Default False
    onlylast:   bool, whether to return only the last value of the list or not. Default False
        Returns:
    mylist:         np.array type string with all folders or files of interest, listed in alphabetical order
    """
    if os.path.isdir(mydir):
        mylist = os.listdir(mydir)
    else:
        mydir = str(input('Dir doesnt exist. Type a valid one: '))
        mylist = os.listdir(mydir)  # Here I can use the function to constrain user input
    new_list = []
    mylist.sort()

    # %% If filter names according to a string:
    if mystr:
        for ii in mylist:
            if mystr in ii:
                if fullpath is True:
                    new_list.append(os.path.join(mydir, ii))
                else:
                    new_list.append(ii)
        mylist = np.copy(np.array([new_list]))

    elif not mystr:
        if fullpath is True:
            for ii in mylist:
                new_list.append(os.path.join(mydir, ii))

            mylist = np.copy(np.array([new_list]))
    mylist.sort()
    new_list.sort()

    mylist = mylist.flatten()
    if onlylast is True:
        return mylist[-1]
    elif not onlylast:
        return mylist


def gendirtree(homedirectory, str_assim='', proc_idx=-9999, mode='fl_', porosity=False, storativity=False,
               dispersivity=False):
    """
    Create directory tree if does not exist, and return paths as a tuple. If individual
    folders exists, they are not overwritten. Construct also dictionary for better handling the paths
        Arguments:
        ----------
    homedir:        str, host directory
    proc_idx:       float, index of the current realization. Default is -9999 no process
                    folder and its sub-folders are created
    mode:           string, either fl_ or tr_. Default 'fl_'
    poros:          bool, update porosity if True. Default is False
    stor:           bool, update storativity if True. Default is False
    disp:           bool, update dispersivity if True. Default is False
        Returns:
        --------
    Dictionary with the name and their corresponding paths.
    If directory tree was already created it can still create individual processes folders
    """
    assert os.path.isdir(homedirectory), 'Wrong home directory'
    fmt_string = '%.5d' % proc_idx

    fld_real = os.path.join(homedirectory, 'realizations_%s' % str_assim)
    fld_allproc = os.path.join(fld_real, 'allproc')
    fld_curproc = os.path.join(fld_allproc, 'proc_%s' % fmt_string)
    fld_mode = os.path.join(fld_curproc, '%s%s' % (mode, fmt_string))
    fld_heads = os.path.join(fld_real, 'hydheads')
    fld_kkkfiles = os.path.join(fld_real, 'kkk')
    fld_modObs = os.path.join(fld_real, 'modobs')
    fld_recdata = os.path.join(fld_real, 'datarecieved')
    fld_modeldata = os.path.join(homedirectory, 'model_data')
    fld_model = os.path.join(homedirectory, 'model')

    fld_poro = os.path.join(fld_real, 'porosity')
    fld_stor = os.path.join(fld_real, 'storativity')
    fld_disper = os.path.join(fld_real, 'dispersivity')

    fld_res = os.path.join(fld_real, 'residuals')
    fld_llh = os.path.join(fld_real, 'likelihood')

    mylabels = ['fld_allproc', 'fld_heads', 'fld_kkkfiles',
                'fld_modobs', 'fld_recdata', 'fld_recheads', 'fld_reckkk', 'fld_recmodobs',
                'fld_modeldata', 'fld_model', 'fld_residuals', 'fld_likelihood']

    mypaths = [fld_allproc, fld_heads, fld_kkkfiles, fld_modObs, fld_recdata,
               os.path.join(fld_recdata, 'hydheads'),
               os.path.join(fld_recdata, 'kkk'),
               os.path.join(fld_recdata, 'modobs'),
               fld_modeldata,
               fld_model, fld_res, fld_llh]
    if porosity is True:
        mylabels.append('fld_porosity')
        mypaths.append(fld_poro)
    if storativity is True:
        mylabels.append('fld_storativity')
        mypaths.append(fld_stor)
    if dispersivity is True:
        mylabels.append('fld_dispersivity')
        mypaths.append(fld_disper)

    time.sleep(np.random.uniform(low=1.0, high=2.0, size=1))
    for zz in mypaths:
        if not os.path.isdir(zz):
            os.makedirs(zz)
    # Add the cur_processes folder:
    mylabels.extend(['fld_curproc', 'fld_mode'])
    mypaths.extend([fld_curproc, fld_mode])

    for ii in mypaths:
        if proc_idx == -9999:
            break
        else:
            if not os.path.isdir(ii):
                os.makedirs(ii)

    mypath_dict = {}
    for cur_id, curpath in enumerate(mypaths):
        mypath_dict[mylabels[cur_id]] = curpath

    return mypath_dict


def init_dir(ii, mymode, homedirectory, str_assim='', porosity=False, storativity=False,
             dispersivity=False):
    """
    Initialize directories and dir variables used along the whole process
        Arguments:
        ---------
    ii:         int, process id (ii is always python index)
    mymode:     str, 'fl_' or 'tr_'
    homedir:    home directory of the whole project
        Returns:
        --------
    NoReal
    dir_dict
    fmt_string
    kfile
    """
    NoReal = ii + 1  # To correct the zero index from python    todo: check whats going on here with the kkk string
    dir_dict = gendirtree(homedirectory, str_assim=str_assim, proc_idx=NoReal, mode=mymode,
                          porosity=porosity, storativity=storativity,
                          dispersivity=dispersivity)  # Get a dict with the directories
    fmt_string = '%.5d' % NoReal  # Process ID to maintain order
    kfile = 'kkkGaus_%s.dat' % fmt_string  # Name of the current kkk filename
    # kkkFile = os.path.join(dir_dict['fld_mode'], kfile)

    return NoReal, dir_dict, fmt_string, kfile


def str_to_bool(s):
    if (s == 'True') or (s == 'true'):
        return True
    elif (s == 'False') or (s == 'false'):
        return False
    else:
        raise ValueError("Cannot convert string to boolean")


def settings_dict(mydict, modes='ini'):
    """Transform settings to proper variable type and retunr everything as a dictionary
    :param      mydict   dict with all settings for running the EnKF
    :return:    mydict with the values transformed to proper variable types
    """
    if modes == 'ini':
        required_input = {'Model name': str, 'Comments': str, 'Model mode': str, 'Update scheme': str,
                          'No realizations': int, 'Subgrid': bool,
                          'Server': bool, 'No cpu': int, 'Cluster': bool, 'Parallel run': bool,
                          'Gen logk fields': bool, 'Porosity': bool, 'Storativity': bool, 'Dispersivity': bool,
                          'Gen porosity': bool, 'Gen storativity': bool, 'Gen dispersivity': bool, 'Ini head': float,
                          'neg2zero': bool, 'head2dwdn': bool, 'cumbtc': bool, 'norm': bool,
                          'moments': bool, 'Spinup': bool, 'hgs': str}

    if modes == 'assim':
        required_input = {'Model name': str, 'Comments': str, 'Model mode': str, 'Subgrid': bool, 'Update scheme': str,
                          'Gaussian anam': bool, 'Damping factor': float, 'Initialize': bool, 'No realizations': int,
                          'Restart': bool, 'Total iters': int, 'Cur iter': int, 'Cur time': int,
                          'Server': bool, 'No cpu': int, 'Cluster': bool, 'Parallel run': bool,
                          'Parallel matmult': bool,
                          'Gen logk fields': bool, 'Porosity': bool, 'Storativity': bool, 'Dispersivity': bool,
                          'Gen porosity': bool, 'Gen storativity': bool, 'Gen dispersivity': bool,
                          'Ini head': float,
                          'Exist modoutput': bool,
                          'Meas std': float,
                          'Relative meas std': bool,
                          'Percentage': float,
                          'Max meas std': float,
                          'Min meas std': float,
                          'neg2zero': bool, 'head2dwdn': bool, 'cumbtc': bool, 'norm': bool,
                          'moments': bool,'Spinup': bool, 'hgs': str}

    # Transform input to proper variable type
    for cur_key, cur_val in mydict.items():
        if cur_key in required_input:
            if required_input[cur_key] is bool:
                mydict[cur_key] = required_input[cur_key](str_to_bool(cur_val))
            else:
                if cur_key == 'Ini head':
                    try:
                        mydict[cur_key] = required_input[cur_key](cur_val)
                    except:
                        mydict[cur_key] = str_to_bool(cur_val)
                else:
                    mydict[cur_key] = required_input[cur_key](cur_val)
    return mydict


def str_inWELLfile(myfile, mystr):
    """
    Find the string that follows a defined string. The string of interest should
    be able to be converted to "FLOAT" number(s). It returns also the
    line id (s) where the string was found. useful for reading observation
    well HGS output files.
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
        Returns:
        --------
    String after the one given in the arguments
    """
    next_str = []
    # ids = []
    with open(myfile, 'r') as fp:
        # counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr + '([^,]+)', line)
            if match:
                next_str.append(match.group(1))
    if len(next_str) == 1:
        next_str = next_str[0]
    else:
        next_str = np.asarray(next_str, dtype='float')

    if len(next_str) > 0:
        return next_str
    elif 'observation_well' not in myfile:
        print('Given string not found in file')


def str_infile(myfile, mystr, index=False):
    """
    Find the string that follows a defined string. The string of interest should
    be able to be converted to "FLOAT" number(s). File format is any.
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
    index:          bool, whether to retunr the line index where string was found
        Returns:
        --------
    String after the one given in the arguments. If index is True returns also
    the line indices where there was a match, as an additional array
    """
    next_str = []
    ids = []
    with open(myfile, 'r') as fp:
        # counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr + '([^,]+)', line)
            if match:
                ids.append(idx)
                next_str.append(match.group(1))
    if len(next_str) == 1:
        next_str = next_str[0]
        ids = ids[0]
    else:
        try:
            next_str = np.asarray(next_str, dtype='float')
            ids = np.asarray(ids, dtype='int')
        except:
            next_str = np.asarray(next_str, dtype='str')
            ids = np.asarray(ids, dtype='int')

    if len(next_str) > 0:
        if index is False:
            return next_str
        elif index is True:
            return ids, next_str
    elif 'observation_well' not in myfile:
        print('Given string not found in file')


def numberfiles(mydir):
    """
    Count the number of files within a directory. If the folder does not exists
    It will be created automatically.
        Arguments:
        ----------
    mydir:      str, full directory location
        Returns:
        --------
    numfiles:   float, number of existing files
    """
    try:
        numfiles = len(os.listdir(mydir))
    except:
        os.makedirs(mydir)
        numfiles = 0
        print('The folder < %s > does not exist and will be created.' % mydir)

    return numfiles


def cpfolder(sourcedir, destdir):
    """ Copy a file or directory"""
    shutil.copytree(sourcedir, destdir)


def join_data_files(mydir, dim=2, mystr=False):
    """
    Join datasets from different files to perform any kind of operation
    on all of them (e.g. rotate points)
        Arguments:
        ----------
    mydir:      str, directory where files are stored
    dim:        int, number of dimensions (columns) in the files
    mystr:      str, identifier of files of interest in case the folder contains additional files
                Default = False if all files
        Returns:
    jointdata:              np.array with joint datasets
    [mylist, lengths]:      np.array with the list of files the number of entries of each one
    """
    # %% Hands on the files to work with:
    mylist = getdirs(mydir, mystr=mystr, fullpath=False, onlylast=False)

    # %% Start loading each file and track the num of entries of each file:
    alldata = np.zeros((1, dim))
    lengths = np.array([])

    for curFile in mylist:
        curdata = np.loadtxt(os.path.join(mydir, curFile))
        alldata = np.concatenate((alldata, curdata))
        lengths = np.append(lengths, curdata.shape[0])

    return np.delete(alldata, 0, axis=0), [mylist, lengths]


def rmfiles(myfiles, fmt_string, process_path):
    """
    Remove unnecessary files to save disk space
        Arguments:
        ----------
    myfiles:        List, strings to identify all files to be KEPT
    fmt_string:     str, process ID
    process_path:   str, directory to perform the deletion
        Returns:
        --------
    mypaths:        str, full path of file(s) of the process in execution, not deleted ????
    """
    # Read all folders within proper process number:
    mypaths = getdirs(process_path, mystr=fmt_string, fullpath=True, onlylast=False)
    idx = []
    for jj in range(0, len(mypaths)):
        mylist = os.listdir(mypaths[jj])
        mylist.sort()
        full_indices = np.arange(0, len(mylist), 1)

        for curname in myfiles:
            mycurfile = getdirs(mypaths[jj], mystr=curname, fullpath=False, onlylast=False)

            if len(mycurfile) == 1:
                idx.append(np.where(mycurfile == mylist)[0])

            elif len(mycurfile) > 1:
                for ii in mycurfile:
                    idx.append(np.where(np.array(ii) == mylist)[0])

        toremove_indices = np.delete(full_indices, idx)

        try:
            for zz in toremove_indices:
                os.remove(os.path.join(mypaths[jj], mylist[zz]))
        except (IOError, PermissionError, AssertionError):
            print('Not all unnecessary files deleted, remove manually...')
            pass
    return mypaths


def fmtobspoints(points, outputfile):
    """
    Create a file with the strings "make observation point" integrated in the list
    it handles only three dimensional coordinates
    """
    f = open(outputfile, 'w')

    for ii in range(0, len(points)):
        f.write(
            'make observation point\nwell%.6d\n%s\t%s\t%s\n' % (ii + 1, points[ii, 0], points[ii, 1], points[ii, 2]))
    f.close()


def renameFiles(mydir, newstr, str2trim):
    """
    Rename the files in a directory, keeping the initial part of the string of each file
        Arguments:
    mydir:          str, directory containing the files to be renamed. Add r'mydir' if using Windows
    newstr:         str, new string to be added to all files
        Returns:
    Renamed files in defined directory
    """
    orig_list = os.listdir(mydir)
    orig_list.sort()
    str2trim = len(str2trim)

    for ii in orig_list:
        newname = ii[0:-str2trim] + newstr
        os.rename(os.path.join(mydir, ii), os.path.join(mydir, newname))


def rmv_rename(inputfile, newfile):
    """Remove input file and assign input filename to another file"""
    if os.path.isfile(inputfile):
        os.remove(inputfile)
    os.rename(newfile, inputfile)
    print('File << %s >> renamed to << %s >>' %(os.path.split(newfile)[-1], os.path.split(inputfile)[-1]))


def saveSettings(mydir, str_assimilation, mymodel, mymode, type_update, porosity, storativity, dispersivity,
                 GausAnam, damp_fc, dY_max, total_iter, ini_head, meas_stddev, relative_meas_stddev,
                 errorPercent, max_meas_stddev, min_meas_stddev, neg2zero, head2dwdn, cumbtc, norm, moments):
    """ (Deprecated) Save the settings of the filter in an ascii file """
    myfile = os.path.join(mydir, '_settings_kf_%s%s.dat' % (mymode, type_update))

    mystr = 'Folder name: %s\nModel name: %s\nmode: %s\nfilter: %s\n' % (str_assimilation, mymodel, mymode, type_update)
    mystr1 = 'Porosity: %s\nStorativity: %s\nDispersivity: %s\n' % (porosity, storativity, dispersivity)
    mystr2 = 'GA: %s\nDF: %s\nDY: %s\nIters: %s' % (GausAnam, damp_fc, dY_max, total_iter)
    mystr3 = 'Ini_head: %s\nMeas_stddev: %s\n' % (ini_head, meas_stddev)
    mystr5 = 'Relative_meas_stddev: %s\nPercentage Error: %s\nMaxMeas_std: %s\nMinMeas_std: %s\n' % (
        relative_meas_stddev, errorPercent, max_meas_stddev, min_meas_stddev)
    mystr4 = 'neg2zero: %s\nhead2dwdn: %s\ncumbtc: %s\nnorm: %s\nmoments: %s' % (
        neg2zero, head2dwdn, cumbtc, norm, moments)

    with open(myfile, 'w') as fp:
        fp.write(mystr)
        fp.write('\n')
        fp.write(mystr1)
        fp.write('\n')
        fp.write(mystr2)
        fp.write('\n')
        fp.write(mystr3)
        fp.write('\n')
        fp.write(mystr5)
        fp.write('\n')
        fp.write(mystr4)
        fp.write('\n')
    print('Settings stored in < %s >' % myfile)


def readFieldData():
    print('Under construction')


def check_userinput(question, myoptions=None):
    """ Checks the user input before sending the input to the function call. In this way,
    the answers of the user are constrained and an error is not returned due to wrong
    user input
        Arguments:
        ----------
    question:       str, question that wants to be asked to the user
    myoptions:      list of str, available options to be consider in the answer
        Returns:
        --------
    answer:         str, with the right answer provided by the user
    """
    if myoptions is None:
        myoptions = []
    is_valid = 0
    while not is_valid:
        answer = input(question)
        if answer == '':
            print('Empty answer, try again!')
            pass
        elif any(answer in s for s in myoptions):
            is_valid = 1  # set it to 1 to validate input and to terminate the while..not loop
        else:
            print('%s is not a valid answer!' % answer)
    return answer


def mk_tpl_controlfile(mypath):
    """Create a Template of the control file requiered to run the EnKF
    """
    mystr = \
        "! Model id\n!---------\nModel name: ModelExample\nComments: newcodeversion\nModel mode: flow\nSubgrid: False\n\n! Filter settings\n!---------\nFilter mode: ini\nNo realizations: 200\nGen logk fields: False\nUpdate scheme: std\nGaussian anam: True\nDamping factor: 0.3\n\n! Model parameters settings\n!---------\nPorosity: False\nStorativity: True\nDispersivity: False\nGen porosity: False\nGen storativity: True\nGen dispersivity: False\n\n! Model states and field observation settings\n!---------\nneg2zero: False\nhead2dwdn: True\ncumbtc: False\nnorm: False\nmoments: False  \n\n! Measurement settings\n!---------\nIni head: 10.0 \nExist modoutput: True\nMeas std: 5e-2\nRelative meas std: False\nPercentage: 0.10\nMax meas std: 5e-3 \nMin meas std: 5e-5\n\n! Numerical and computing settings\n!---------\nInitialize: True\nRestart: False \nTotal iters: 1 \nCur iter: 0 \nCur time: 0  \n\nServer: False\nServer name: None\nPort: 4001\nCluster: True\nParallel run: False\nParallel matmult: False  \nNo cpu: 16"
    myfile = os.path.join(mypath, 'setting_modelName.cfenkf')
    with open(myfile, 'w') as fp:
        fp.write(mystr)
    print('Template of the control file < %s > created.' % myfile)
