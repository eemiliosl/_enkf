import numpy as np
from numpy.linalg import inv, solve
import matmult.matmul_opt as mm


def likelihood(mata, matb, method='solve', parall=False, ncpus=2):
    """
    Perform matrix multiplications of the form [a.T * inv(B) * a]. Two methods are
    available. Via the inverse, or the numerically more stable solve(a,b)
        Arguments:
        ----------
    mata:   np.array, matrix ans2
    matb:   np.array, matrix b
    method: Str, how to solve the matrix multiplication problem
    parall: Bool, use or not parallel multiplications. Default False
    ncpus:  Int, number of CPUs to use. Default 2
        Returns:
        --------
    likelihood matrix
    """

    assert method == 'solve' or 'inv', 'Wrong method for computing the likelihood'
    if method == 'inv':
        if parall is True:
            return mm.matmul_locopt(mm.matmul_locopt(mata.T, inv(matb), ncpu=ncpus), mata, ncpu=ncpus)
        elif parall is False:
            return mata.T.dot(inv(matb)).dot(mata)

    elif method == 'solve':
        if parall is True:
            return mm.matmul_locopt(mata.T, solve(matb, mata), ncpu=ncpus)
        elif parall is False:
            return mata.T.dot(solve(matb, mata))


def autocov(mata, diag=True, parallel=False, ncpu=2):
    """ Compute auto-covariance matrix
    Arguments:
        mata:       2D array, first random variable matrix
        diag:       bool, return only main diagonal or not
        parallel:   bool, parallel matrix multiplication or not
        ncpu:       int, number of processors tu use if parallel=True
    Returns:
        2D array, ie. auto covariance matrix
    """
    dev_mean = mata - np.average(mata, axis=1, weights=None, returned=False)[:, np.newaxis]

    if parallel is False:
        Qyy = (1. / (mata.shape[1] - 1)) * (dev_mean.dot(dev_mean.T))
    elif parallel is True:
        Qyy = (1. / (mata.shape[1] - 1)) * (mm.matmul_locopt(dev_mean, dev_mean.T, ncpu=ncpu))

    if diag is True:
        Qyy = np.diag(Qyy)

    return Qyy


def crosscov(mata, matb, diag=True, parallel=False, ncpu=2):
    """ Compute cross-covariance matrix
    Args:
        mata:       2D array, first random variable matrix
        matb:       2D array, second random variable matrix
        diag:       bool, return only main diagonal or not
        parallel:   bool, parallel matrix multiplication or not
        ncpu:       int, number of processors tu use if parallel=True
    Returns:
        2D array, ie. cross covariance matrix
    """
    dev_mean1 = mata - np.average(mata, axis=1, weights=None, returned=False)[:, np.newaxis]
    dev_mean2 = matb - np.average(matb, axis=1, weights=None, returned=False)[:, np.newaxis]

    if parallel is False:
        Qyy = (1. / (mata.shape[1] - 1)) * (dev_mean1.dot(dev_mean2.T))
    if parallel is True:
        Qyy = (1. / (mata.shape[1] - 1)) * (mm.matmul_locopt(dev_mean1, dev_mean2.T, ncpu=ncpu))

    if diag is True:
        Qyy = np.diag(Qyy)

    return Qyy
