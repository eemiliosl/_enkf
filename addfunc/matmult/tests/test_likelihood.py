import unittest
import sys
import os
from time import time
import numpy as np
from numpy.linalg import inv, solve
mainPath = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.abspath(os.path.join(mainPath, '..')))
import matmul_opt as mmo
from functools import reduce
# Note: all tests can be ran by typing "nose" in the terminal


class Test(unittest.TestCase):

    def test_equality_inv(self):
        A = np.random.uniform(size=(1000, 1000))
        B = np.random.uniform(size=(1000, 1000))

        ans1 = mmo.matmul_locopt(mmo.matmul_locopt(A.T, inv(B), ncpu=2), A, ncpu=2)

        ans2 = A.T.dot(inv(B)).dot(A)
        self.assertTrue(np.alltrue(abs(ans1 - ans2) < 1e-11))

    def test_equality_solve(self):
        A = np.random.uniform(size=(1000, 1000))
        B = np.random.uniform(size=(1000, 1000))

        ans1 = mmo.matmul_locopt(A.T, solve(B, A), ncpu=2)

        ans2 = A.T.dot(inv(B)).dot(A)

        ans3 = reduce(np.dot, [A.T, inv(B), A])
        self.assertTrue(np.alltrue(abs(ans1 - ans2) < 1e-11))

        self.assertTrue(np.alltrue(abs(ans1 - ans3) < 1e-11))

# #t1 = time()
# #print("%d CPUs:" % ncpu, time()-t1)
if __name__ == '__main__':
    unittest.main()
