import unittest
import sys
import os
from time import time
import numpy as np
mainPath = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.abspath(os.path.join(mainPath, '..')))
import matmul_opt as mmo
# Note: all tests can be ran by typing "nose" in the terminal


class Test(unittest.TestCase):

    def test_equality(self):
        A = np.random.uniform(size=(1000, 1000))
        B = np.random.uniform(size=(1000, 1000))

        C = mmo.matmul_locopt(A, B)

        D = np.dot(A, B)
        print("Are the results the same?: ", np.alltrue(abs(C - D) < 1e-11))
        self.assertTrue(np.alltrue(abs(C - D) < 1e-11))

    def test_equality2(self):
        A = np.random.uniform(size=(10, 10))
        B = np.random.uniform(size=(10, 10))
        C = mmo.matmul_locopt(A, B)
        D = np.dot(A, B)  # Here is the fake error
        print("Same?:", np.alltrue(abs(C - D) < 1e-11))
        self.assertTrue(np.alltrue(abs(C - D) < 1e-11))


# Pendent tests:
# Check weather the entries of the matrix are all valid (e.g. not nan)
# Check that the time consumed by parallel multiplication is actually lower than in normal numpy way

# #t1 = time()
# #print("%d CPUs:" % ncpu, time()-t1)
if __name__ == '__main__':
    unittest.main()
