from multiprocessing import Pool, Process, sharedctypes
import numpy
import ctypes
from numpy import ctypeslib
# from time import time


def update_pool_global(dict):
    """
    Adds dict to the global dictionary
    """
    globals().update(dict)


def mat_rowrange_mul(a_row):
    global a, b
    # a = int(a)
    # b = int(b)
    return numpy.dot(a[int(a_row[0]):int(a_row[1]), :], b)


def mat_rowcol_mul(a_row):
    global a, b
    # a = int(a)
    # b = int(b)
    return numpy.dot(a[a_row, :], b)


def mat_rowrange_mul_shared(args):
    # a little ugly, but allows running with a Pool
    # which accept only 1 argument
    a_row_domain, a_shape, b_shape, shared_a, shared_b, shared_c = args

    # access shared memory object as numpy array, set dimensions
    nd_c = ctypeslib.as_array(shared_c).reshape((a_shape[0], b_shape[1]))
    nd_a = ctypeslib.as_array(shared_a).reshape(a_shape)
    nd_b = ctypeslib.as_array(shared_b).reshape(b_shape)

    # write answer to shared memory
    # it would be better if numpy.dot could write "in-place"
    nd_c[a_row_domain[0]:a_row_domain[1], :] = \
        numpy.dot(nd_a[a_row_domain[0]:a_row_domain[1], :], nd_b)

    return None


def matmul_locopt(mata, matb, ncpu=2):
    """
    Matrix multiplication with local optimization (several CPU's)
        Arguments:
        ----------
    mata:   np.array, matrix ans2
    matb:   np.array, matrix b
    ncpu:   int, number of cpus to use
        Returns:
        --------
    ans2:   np.array, result of matrix multiplication
    """
    # %% Create a dictionary with objects to
    # send to worker.
    assert mata.shape[1] == matb.shape[0], 'Dim mismatch!'
    worker_globals = {}
    a = worker_globals['a'] = mata
    b = worker_globals['b'] = matb

    p = Pool(ncpu, update_pool_global, (worker_globals,))  # Make the dict available for all processes

    # To define the dimensions of the new matrix
    x = mata.shape[0]
    y = matb.shape[1]

    mystep = x / ncpu
    domains = zip(numpy.arange(0, x, mystep), numpy.arange(0, x, mystep) + mystep)

    # %% Perform the multiplication:
    # t1 = time()
    tmp = p.map(mat_rowrange_mul, domains)
    p.close()
    p.join()
    # %% Collect the results and construct the new matrix
    ans2 = numpy.empty((x, y))
    count = 0
    for ii in numpy.arange(len(tmp)):
        # pdb.set_trace()
        ans2[count: count + len(tmp[ii]), :] = tmp[ii]
        count += len(tmp[ii])

    # print("%d CPUs:" % ncpu, time()-t1)
    return ans2


def matmul_shared(mata, matb, ncpu=2):
    """
    Matrix multiplication with shared memory optimization. First
    dimension of mata must be a multiple of ncpu
        Arguments:
        ----------
    mata:   np.array, matrix ans2
    matb:   np.array, matrix b
    ncpu:   int, number of cpus to use
        Returns:
        --------
    nd_c:   np.array, result of matrix multiplication
    """

    x = mata.shape[0]
    x2 = mata.shape[1]
    y = matb.shape[0]
    y2 = matb.shape[1]

    a_shape = (x, x2)
    b_shape = (y, y2)

    c = numpy.empty((x, y2))

    # allocated shared memory
    shared_a = sharedctypes.Array(ctypes.c_double, mata.flat, lock=False)
    shared_b = sharedctypes.Array(ctypes.c_double, matb.flat, lock=False)
    shared_c = sharedctypes.Array(ctypes.c_double, c.flat, lock=False)

    # access the answer as a numpy array, set dimensions
    nd_c = ctypeslib.as_array(shared_c).reshape((a_shape[0], b_shape[1]))

    # x must be a multiple of num_cpus
    assert x % ncpu == 0, 'First dimension of matrix A must be a multiple of ncpu!'

    step = x / ncpu

    # define row domains for each process
    domains = zip(numpy.arange(0, x, step), numpy.arange(0, x, step) + step)
    static_args = (a_shape, b_shape, shared_a, shared_b, shared_c)

    # allocate processes
    p = [Process(target=mat_rowrange_mul_shared, args=((x,) + static_args,)) for x in domains]

    # t1 = time()

    for x in p:
        x.start()
    for x in p:
        x.join()

    # print("%d CPUs:" % ncpu, time()-t1)
    return nd_c
