#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      IRTG
#
# Created:     14-10-2015
# Copyright:   (c) IRTG 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy as np
import matmul_opt as mmo
from numpy.linalg import inv, solve

def main():

    A = np.random.uniform(size = (10,10))
    B = np.random.uniform(size = (10,10))

    C = mmo.matmul_locopt(A,B)

    D = np.dot(A,B)

    print(C)
    print("Same?:", np.alltrue(abs(C-D)<1e-11))

if __name__ == '__main__':
    main()
