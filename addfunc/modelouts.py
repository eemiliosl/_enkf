"""
Functions to deal with model outputs of several types
Created on Tue 13 Nov 14:30:00 2016
@author: eesl
"""
import sys
import os
import numpy as np
import datetime
import shutil
import subprocess as sp
import platform
import multiprocessing as mp
import time
import scipy
from itertools import repeat
from statistics import mean
# import matplotlib.pylab as plt
import scipy.sparse as ssp
import fieldgen as fg
import dirs as dm
import gridmanip as gm
# import kalman as klm


def getstates(homedir, ii, nnodes, mode, modeltimes, Y_i_upd='', curtimestep=0, genfield=False, plot=False,
              storebinary=False, mytype='restart', initial_head=7.0, cumbtc=True):
    """
    Update states(model measurements) with updated parameters using a forward model
    run for the corresponding realization. Give the parameter values NOT in LOG SCALE!!!
        Arguments:
        ----------
    homedir:
    ii:                 int, process index
    mode:               str, type of model run. 'fl_' or 'tr_'
    Y_i_upd:            np.array, parameter values of the model
    genfield:           bool, generate a gaussian field or not
    plot:               bool, plot the generated fields or not
        Returns:
        --------
    modmeas:      np.array of updated modeled measurements (or states). Fortran order ([no_meas*no_obsPoints] x 1 )
    """
    # %% Initialize necessary variables and directories:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mode, homedir)

    processesPath = dir_dict['fld_allproc']
    mainPath = os.path.abspath(os.path.join(processesPath, '..', '..'))
    Model_data_dir = os.path.join(mainPath, 'Model_data')
    cur_processpath = dir_dict['fld_curproc']
    headsFile1 = 'finalheadsFlow_%s.dat' % fmt_string
    randparam = '_RandFieldsParameters.dat'
    randfile = os.path.join(Model_data_dir, randparam)

    headfieldsPath = dir_dict['fld_heads']
    ModObsPath = dir_dict['fld_modobs']
    dest_kkkFolderBinary = dir_dict['fld_kkkfiles']
    binkkkfile = os.path.abspath(os.path.join(dest_kkkFolderBinary, curKFile.split('.')[0]))
    curDestDir = dir_dict['fld_mode']
    if not os.listdir(curDestDir):
        prepfwd(ii, homedir, mode, headsFile=headsFile1)

    files2keep = np.array(
        ['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'finalheads', 'parallelindx', 'meshwell', '.png',
         '.lic'])
    assert os.path.exists(curDestDir), 'Something is wrong in the definition of the tree directory!'
    assert os.path.exists(Model_data_dir), 'Model data directory does not exist or not in proper location!'

    # %% First Step: Generate new kkk files to be used by GROK and HGS in the proper folder
    # ---------------------#
    # Decide whether to create random fields or not:
    if genfield is True:
        # Load random field parameter data from the main file:
        xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(
            randfile)
        # Generate the field: returns K in m/s.
        Y = fg.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel, Y_model,
                                Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot, mydir=curDestDir)
    elif genfield is False:
        assert len(Y_i_upd) > 0, 'No parameters were provided!'
        Y = np.copy(Y_i_upd)

    # Save KKK file in the proper process folder:
    np.savetxt(os.path.join(curDestDir, curKFile), np.transpose((np.arange(1, len(Y) + 1, 1), Y.squeeze(), Y.squeeze(),
                                                                 Y.squeeze())), fmt='%d %.6e %.6e %.6e')
    # %% Second Step: Run the model: GROK and HGS :
    # ---------------------#
    fwdHGS(curDestDir, fmt_string, mytype=mode)

    # %% Third step: Read new model outputs at specified output times, store them as bin files.
    # If running flow, extract in addition final heads:
    # ---------------------#
    if 'restart' in mytype:
        mytimes = modeltimes[curtimestep][np.newaxis]
    elif 'cum' in mytype:
        mytimes = modeltimes[0:curtimestep + 1]
    elif 'full' in mytype:
        mytimes = modeltimes

    all_modout = getmodout(curDestDir, mode=mode, outputtime=mytimes, initial_head=initial_head, cumbtc=cumbtc)

    # %% Fourth: save stuff in the corresponding directories:
    # ---------------------#
    if storebinary is True:
        if curtimestep == 0:
            np.save(os.path.join(ModObsPath, 'mod%s%s' % (mode, fmt_string)), all_modout)
            np.save(binkkkfile, np.transpose(Y))
        elif (curtimestep + 1) == len(modeltimes):
            headfile, dummypaths = dm.getdirs(curDestDir, mystr='head_pm', fullpath=True, onlylast=True)
            nnodes=1
            headfield, dummytime = readHGSbin(headfile, nnodes)
            np.save(os.path.join(headfieldsPath, headsFile1.split('.')[0]), headfield)

    # Fifth: Remove unnecessary files:
    # ---------------------#
    dummypaths = dm.rmfiles(files2keep, mode + fmt_string, cur_processpath)
    del dummypaths
    # Sixth: Reorder model outputs (states) prior to return them, using Fortran convention:
    # ---------------------#
    try:
        modmeas = all_modout.flatten('F')  # Fortran convention is always used in this step
    except:
        raise Exception('Something is wrong with the model outputs, check e.g. cur time step in grok!')

    return modmeas


def rmodout(myfile, mytimes):
    """
    Support for the <getmodout> function (to extract model measurements)
        Arguments:
        ----------
    myfile:     str, fullpath with the name of the file to be loaded
    mytimes:    np array,  float or int array with times of interest
    mode:     str, model run mode. Default 'fl_' for flow model
    cumbtc:     bool, weather to get a cumulative dist of the btc or not
        Returns:
        --------
    mydata:     np.array, with model outputs at specific times (nmeas x 1)
    """

    try:
        # Observation point HGS format
        myTime, mymodmeas = np.loadtxt(myfile, skiprows=2, usecols=(0, 1), unpack=True)
    except:
        # Observation well HGS format:
        mymodmeas = []
        idx_line, myTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)

        if isinstance(idx_line, int) is True:
            myTime = np.asarray(myTime, dtype=float)
            mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0,))
            mymodmeas = np.mean(mymodmeas)

        if isinstance(idx_line, int) is False:
            with open(myfile, 'r') as f:
                mylines = f.readlines()

            for yy in range(0, len(idx_line), 1):
                if yy < len(idx_line) - 1:
                    myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                elif yy == len(idx_line) - 1:
                    myvalues = np.asarray(mylines[idx_line[yy] + 1:])

                try:
                    mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
                except:
                    mymodmeas.append(mean([float(cur_line.split(' ')[3]) for cur_line in myvalues]))

        if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
            mymodmeas = np.asarray(mymodmeas)
        if isinstance(mytimes, int) is True:
            mytimes = np.expand_dims(mytimes, 1)

        # %% Mask to get only the elements of interest:
        if not isinstance(mymodmeas, np.ndarray):
            mymodmeas = np.expand_dims(mymodmeas, axis=0)
            mymodmeas = mymodmeas[np.where(np.in1d(myTime, mytimes))]

    return mymodmeas, mytimes


def getmodout(mypath, mode='', outputtime='', initial_head=7.0, cumbtc=True):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in transport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    if not mode:
        print('Model outputs not extracted. No running mode provided')
    elif mode == 'fl_':
        mystrs = '_flow'
    elif mode == 'tr_':
        mystrs = '_conc'

    # Get a list of the files of all observation points:
    files, dummydir = dm.getdirs(mypath, mystr=mystrs, fullpath=True, onlylast=False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for ii in files:
        selMeas = rmodout(ii, outputtime)   #, mymode=mode, initial_head=initial_head, cumbtc=cumbtc)
        all_modout = np.append(all_modout, selMeas)  # All selected heads

    # Protect the process of reading outputs against failure of HGS: e.g. no more mass stored => all conc = 0
    numobs = len(outputtime) * len(files)
    if (len(all_modout) != numobs) or (len(all_modout) == 0):
        all_modout = np.zeros(numobs, )

    all_modout = np.reshape(all_modout, (-1, len(files)), order='F')

    return all_modout


def transfmodelouts(mytimes, mymodmeas, todwdwn=True, iniheads='', cum=False, normed=False, no_negatives=False, moments=False, pumpRate=''):
    """
    Args:
        mytimes:
        mymodmeas:
        todwdwn:
        iniheads:
        cum:
        no_negatives:
        moments:
        normed:
        pumpRate:

    Returns:
    """
    modmeas_transf = np.copy(mymodmeas)
    if todwdwn is True:
        if not iniheads:
            sys.exit('Initial heads not provided although todwdn function was to be applied')
        modmeas_transf = np.subtract(initial_head, modmeas_transf)

    if cum is True:
        # Proper way of normalize non-cumsum data: normalized = (x-min(x))/(max(x)-min(x))
        modmeas_transf = np.cumsum(modmeas_transf)

    if normed is True:
        modmeas_transf = modmeas_transf / np.sum(modmeas_transf)

    if no_negatives is True:
        zero_id = np.where(modmeas_transf < 0)
        if len(zero_id[0]) > 0:
            try:
                mymodmeas_transf[zero_id[0]] = 0
            except IndexError:
                mymodmeas_transf[np.newaxis][zero_id[0]] = 0

    if moments is True:
        dt = np.append(mytimes[0], np.diff(mytimes))
        Zero_m = np.sum((mytimes ** 0) * mymodmeas * dt)
        First_m = np.sum((mytimes ** 1) * mymodmeas * dt)
        Second_m = np.sum((mytimes ** 1) * mymodmeas * dt)
        First_mN = First_m / Zero_m         # Mean arrival time
        Second_mN = Second_m / Zero_m
        Second_mC = Second_m - (First_m ** 2 / Zero_m)
        Second_mCN = Second_mC / Zero_m
        if pumprate:
            Mass = Zero_m * pumpRate
    if moments is False:
        return modmeas_transf
    elif moments is True:
        if not pumpRate:
            return  modmeas_transf, Zero_m, First_m, First_mN, Second_mN, Second_mCN, Mass
        elif pumpRate:
            return modmeas_transf, Zero_m, First_m, First_mN, Second_mN, Second_mCN, Mass


def readHGSbin(myfile, nnodes):
    """
    Read a HGS output bin file. The function has been tested with
    subsurface hydraulic heads and overland heads.
        Arguments:
        ----------
    myfile:     str, filename *full path(
    nnodes:     int, number of nodes in the mesh
        Returns:
        --------
    n_param:    np.array with the nodal parameter values
    time:       float, model time that corresponds to the param field values
    """
    with open(myfile, "rb") as f:
        pad = np.fromfile(f, dtype='uint8', count=4)
        mytime = np.fromfile(f, dtype='uint8', count=80)

        tt = [''.join(chr(t)) for t in mytime]
        mytime = float(''.join(tt))

        pad = np.fromfile(f, dtype='uint8', count=4)
        pad = np.fromfile(f, dtype='uint8', count=4)

        nodal_values = np.fromfile(f, dtype='float64', count=nnodes)
        pad = np.fromfile(f, dtype='uint8', count=4)

    return nodal_values


def rdparam_bin(myPath, cmstr=''):
    """ Read parameter fields from binary files. Binary file names should contain a specific string identifier
    to constrain the file list.
    Arguments:
        myPath:     Str, directory where the information is stored
        cmstr:      Str, identifier for the files to work with
    Returns:
        nel:        Integer, number of elements of the model grid
        nreal:      Integer, number of realizations in the ensemble
        Y:          Numpy array, 2D Array with the proper data
    """
    # myFiles = os.listdir(myPath)
    myFiles = [x for x in os.listdir(myPath) if cmstr in x]
    myFiles.sort()

    for idx, curkkk in enumerate(myFiles):
        if idx == 0:
            kkkmat = np.load(os.path.join(myPath, curkkk))

        elif idx > 0:
            kkk = np.load(os.path.join(myPath, curkkk))
            kkkmat = np.c_[kkkmat, kkk]  # Parameter field matrix (nel,nreal)

    nel, nreal = kkkmat.shape  # Number of elements in the domain
    return nel, nreal, kkkmat


def rdmodelout_bin(myPath, mymode='fl_'):
    """ Read Modeled outputs from binary files. Either heads or concentrations.
    Arguments:
        myPath:             Str, directory where the information is stored
        mymode:             str, type of data to read
    Returns:
        num_modout:              Integer, number of model outputs
        modoutput:       Numpy array, modeled measurement values (ndata x nrealizations)

    Note: The matrices of measurements are flattened following Fortran
        convention (column-wise or column major). If work with both types of
        measurements, heads are listed first and then concentrations
    """
    myFiles = os.listdir(myPath)
    myFiles.sort()

    for curid, kk in enumerate(myFiles):

        if mymode in kk:
            cur_file = os.path.join(myPath, myFiles[curid])

            if curid == 0:
                modoutput = np.load(cur_file).flatten('F')

            elif curid > 0:
                cur_modout = np.load(cur_file).flatten('F')
                modoutput = np.c_[modoutput, cur_modout]

    num_modout = modoutput.shape[0]  # Number of observations

    return num_modout, modoutput



def kkk_bin2ascii(infile, outfile, orig_shape, biggrid_elid='', smallgrid_elid='', get_mean=True, exptransf=True,
                  logtransf=False, expand=True):
    """ Read binary files generated during inversion and creates a parameter file (kkk) that HGS can read
    Args:
        infile:
        outfile:
        orig_shape:
        biggrid_elid
        smallgrid_elid
        get_mean:
        exptransf:
        logtransf:
        expand:
    Returns:
    """
    if type(infile) == str:
        myparam = np.load(infile)
    else:
        myparam = infile

    if get_mean is True:
        myparam_mn = myparam.mean(axis=1)
    elif get_mean is False:
        myparam_mn = myparam

    if exptransf is True:
        myparam_mn = np.exp(myparam_mn)
    if logtransf is True:
        myparam_mn = np.log(myparam_mn)

    if expand is True:
        myparam_mn = gm.expshr_param(myparam_mn, biggrid_elid, smallgrid_elid, what='expand')

    assert myparam_mn.shape[0] == orig_shape, 'shape of original and modified parameter arrays do NOT match!'
    np.savetxt(outfile, np.transpose((np.arange(1, len(myparam_mn) + 1, 1), myparam_mn, myparam_mn, myparam_mn)),
               fmt='%d %.6e %.6e %.6e')

    print('File <%s> successfully stored' % outfile)
