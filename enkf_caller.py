"""
Main caller. Reads settings from the control file
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon
"""
import sys
import os
import _enkf.addfunc.dirs as dr
import _enkf.ekfini as ekfini
import _enkf.ekf_assimfl_ as ekf_assimfl_
import _enkf.ekf_assimtr_ as ekf_assimtr_
import time


def cf_wrapper(controlfile):
    if not os.path.isfile(os.path.join(dr.homedir(), controlfile)):
        print('File < %s > does not exist!' % controlfile)
        tmpl = dr.check_userinput('Create template of the control file?(y/n): ', myoptions=['yes', 'y', 'no', 'n'])
        if 'y' in tmpl:
            dr.mk_tpl_controlfile(dr.homedir())
        sys.exit('Terminating...')

    # If control file exists:
    with open(os.path.join(dr.homedir(), controlfile), 'r') as myobj:
        mylines = myobj.readlines()

    # Store all settings in a DICT
    settings_dict = {}

    for cur_lin in mylines:
        if ("!" in cur_lin) or (cur_lin is '\n'):
            pass
        else:
            settings_dict[cur_lin.split(':')[0]] = cur_lin.split(':')[1].rstrip('\n')
            if ' ' in settings_dict[cur_lin.split(':')[0]]:
                settings_dict[cur_lin.split(':')[0]] = settings_dict[cur_lin.split(':')[0]].strip(' ')

    # Call the corresponding code:
    # First check whether it is initialize or assimilate:
    check_kw1 = ['Filter mode', 'No realizations']
    for cur_kw1 in check_kw1:
        if cur_kw1 not in settings_dict:
            sys.exit('Error (3): < %s > setting not found in < %s >' % (cur_kw1, controlfile))

    if 'Model mode' not in settings_dict:
        sys.exit('Error (4): Model mode setting not found' % controlfile)

    if 'fl' in settings_dict['Model mode']:
        settings_dict['Model mode'] = 'fl_'
    elif 'tr' in settings_dict['Model mode']:
        settings_dict['Model mode'] = 'tr_'

    if ('std' in settings_dict['Update scheme']) or ('standard' in settings_dict['Update scheme']):
        settings_dict['Update scheme'] = 'std'

    if 're' in settings_dict['Update scheme']:
        settings_dict['Update scheme'] = 'restart'

    if 'ini' in settings_dict['Filter mode']:
        print('Initialize ensemble with %s realizations...' % int(settings_dict['No realizations']))
        ekfini.iniens(settings_dict['Model name'], **settings_dict)

    elif 'assim' in settings_dict['Filter mode']:
        if 'fl' in settings_dict['Model mode']:
            ekf_assimfl_.assimfl(settings_dict['Model name'], **settings_dict)
        if 'tr' in settings_dict['Model mode']:
            ekf_assimtr_.assimtr(settings_dict['Model name'], **settings_dict)

    print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ':Process Finished and exited')


if __name__ == "__main__":
    inputfile = 'settingsFile_ModelExample.cfenkf'
    # inputfile = str(sys.argv[1])
    cf_wrapper(inputfile)
