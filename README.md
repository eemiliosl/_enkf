*project: PhD solute tracer tomography field scale*  
*@author: Emilio Sanchez Leon*  
*Date: 11.2017*  

#### This directory contains the implementation of the EnKF developed in this work.    
#### To properly run the codes, the only requisite is to keep the folder structure and naming convention. The name of the model is the only thing thsat should change.   
##### A toy model is provided to exemplify the functionality of the model.
##### Software requirements:
    1.- Python 3.x : recommend to install it through [Anaconda Distribution]:(https://www.anaconda.com/download/)
    2.- HydroGeoSphere Version 20140203 [Aquanty Inc.]:(https://www.aquanty.com/hydrogeosphere/)
#### The codes are based on a control file containing all necessary settings for the filter. The same settings file is used to run an initialization, apply the filter to a groundwater model or to a transport model.
As an example, a control file named `settingsFile_ModelExample.tmpl` is provided with the necessary settings to initialize a data assimilation exercise.
To run the filter:    
    1.- Open a terminal or command line  
    2.- `cd` to the directory where the python codes are located  
    3.- Type `>> python enkf_caller.py controlfileName`  

#### For questions regarding the codes please contact via email: eduardoemilio.sanchez-leon@student.uni-tuebingen.de