"""
Run X number of realizations to get datasets for the EnKF. It is
Support TCP/IP connections if server is True:  socket (client) is created and when the right IP
of a SERVER is provided, it will establish communication
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon
"""
import sys
import os
import numpy as np
import time
import multiprocessing as mp

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import _enkf.addfunc.dirs as dirs
import _enkf.addfunc.fieldgen as fg
from _enkf.addfunc.hgs import GenReal
import addfunc.mysocket.mysocket as sock
import addfunc.gridmanip as gm


def iniens(modelname, **mysettings):
    """ Initialize an ensemble of realizations according to the input settings provided in the
    dictionary ** my settings.
        Args:
    modelname:      str, model name
    **mysettings:   dict, containing all settings provided in the control file
        Returns:
    """
    # required_input = {'Model name': str, 'Comments': str, 'Model mode': str, 'Update scheme': str,
    #                   'No realizations': int, 'Subgrid': bool,
    #                   'Server': bool, 'No cpu': int, 'Cluster': bool, 'Parallel run': bool,
    #                   'Gen logk fields': bool, 'Porosity': bool, 'Storativity': bool, 'Dispersivity': bool,
    #                   'Gen porosity': bool, 'Gen storativity': bool, 'Gen dispersivity': bool, 'Ini head': float,
    #                   'neg2zero': bool, 'head2dwdn': bool, 'cumbtc': bool, 'norm': bool, 'moments': bool}
    #
    # # Transform input to proper variable type
    # for cur_key, cur_val in mysettings.items():
    #     if cur_key in required_input:
    #         if required_input[cur_key] is bool:
    #             mysettings[cur_key] = required_input[cur_key](dirs.str_to_bool(cur_val))
    #         else:
    #             mysettings[cur_key] = required_input[cur_key](cur_val)
    mysettings = dirs.settings_dict(mysettings)

    mainPath = os.path.join(dirs.homedir(), '_dataAssim', modelname)
    str_assimilation = '%s%s' % (modelname, mysettings['Comments'])
    grid_filedir = os.path.join(mainPath, '%sToCopy' % mysettings['Model mode'], 'meshtecplot.dat')

    print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Start generation of ensemble...')
    flag = 'stillworking'

    # If True, create arrays of Porosity, dispersivity and storativities
    mypath_dict = dirs.gendirtree(mainPath,
                                  str_assim=str_assimilation,
                                  proc_idx=-9999,
                                  mode='',
                                  porosity=mysettings['Porosity'],
                                  storativity=mysettings['Storativity'],
                                  dispersivity=mysettings['Dispersivity'])

    # todo: I think This is not needed, since nothing is returned
    # if mysettings['Subgrid'] is True:
    #    xlen, ylen, zlen, nxel, nyel, nzel, \
    #        xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, \
    #        lambda_y, lambda_z = fg.readParam(os.path.join(mypath_dict['fld_modeldata'], '_RandFieldsParameters.dat'))
    #    # gm.sel_grid(grid_filedir, xlim, ylim, zlim, store=True, return_val=False)

    mylines_torec = []
    with open(os.path.join(mypath_dict['fld_modeldata'], '_RandFieldsParameters.dat'), 'r') as sett:
        mylines_torec.append(sett.readlines())
        mylines_torec.append(['\n', '\n'])

    if mysettings['Porosity'] is True:
        porosities = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandPoros.dat', mysettings['No realizations'])
        np.save(os.path.join(mypath_dict['fld_porosity'], 'porosity_%siter000_timestep_000' % mysettings['Model mode']),
                porosities)
        with open(os.path.join(mypath_dict['fld_modeldata'], '_RandPoros.dat'), 'r') as sett:
            mylines_torec.append(sett.readlines())
            mylines_torec.append(['\n', '\n'])

    if mysettings['Storativity'] is True:
        storativities = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandStorage.dat',
                                          mysettings['No realizations'])
        np.save(os.path.join(mypath_dict['fld_storativity'],
                             'storativity_%siter000_timestep_000' % mysettings['Model mode']), storativities)
        with open(os.path.join(mypath_dict['fld_modeldata'], '_RandStorage.dat'), 'r') as sett:
            mylines_torec.append(sett.readlines())
            mylines_torec.append(['\n', '\n'])

    if mysettings['Dispersivity'] is True:
        dispersivities = fg.genGaussParams(mypath_dict['fld_modeldata'], '_RandDispers.dat',
                                           mysettings['No realizations'])
        np.save(os.path.join(mypath_dict['fld_dispersivity'],
                             'dispersivity_%siter000_timestep_000' % mysettings['Model mode']),
                dispersivities)
        with open(os.path.join(mypath_dict['fld_modeldata'], '_RandDispers.dat'), 'r') as sett:
            mylines_torec.append(sett.readlines())
            mylines_torec.append(['\n', '\n'])

    # Write necessary information in a rec file:
    recfile = os.path.join(mypath_dict['fld_allproc'], '..', '%s_ini.rec' % str_assimilation)

    with open(recfile, 'w') as recording:
        for cur_key, cur_val in mysettings.items():
            recording.write('%s :: %s\n' % (cur_key, cur_val))
        for cur_lin in mylines_torec:
            recording.writelines(cur_lin)
    print('Settings stored in < %s >' % recfile)

    while flag == 'stillworking':

        if mysettings['Server'] is True:
            pass
            # indicesDone = False
            # processes, flag = sock.senddata(server, ncpu)
            # print('working ids %s (flag:%s)' % (processes, flag))

        elif mysettings['Server'] is False:
            print('Initializing the ensemble in a local computer...')
            # mypath_dict = dm.gendirtree(mainPath, str_assim=str_assimilation, proc_idx=-9999, mode='')
            # processes = random.sample(['aa','bb','cc','xx','ss','tt','dd'],mysettings['No realizations'])
            processes = np.arange(1, mysettings['No realizations'] + 1, 1).astype('str')

        str2pass = [s + '-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s-%s'
                    % (mainPath, mysettings['Model mode'],
                       mysettings['Initialize'],
                       mysettings['Gen logk fields'], grid_filedir,
                       mysettings['Ini head'],
                       mysettings['Update scheme'], str_assimilation,
                       mysettings['neg2zero'],
                       mysettings['head2dwdn'],
                       mysettings['cumbtc'],
                       mysettings['norm'],
                       mysettings['moments'],
                       mysettings['Porosity'],
                       mysettings['Storativity'],
                       mysettings['Dispersivity'],
                       mysettings['Subgrid'],
                       mysettings['Spinup'],
                       mysettings['hgs']) for s in processes]

        # %% Perform realizations using the defined process Indices:
        if flag == 'done':
            print('No more work received')
        else:

            if mysettings['Parallel run'] is False:
                for ii in str2pass:
                    indicesDone = GenReal(ii)
            elif mysettings['Parallel run'] is True:
                pool = mp.Pool(mysettings['No cpu'])
                indicesDone = pool.map(GenReal, str2pass)
                pool.close()
                pool.join()

            print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Finish generating initial ensemble')

            if mysettings['Server'] is False:
                flag = 'done'
    del indicesDone
    if mysettings['Server'] is False:
        print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Relocating data into single bin files')
        Target_files2transfer = [mypath_dict['fld_kkkfiles'],
                                 mypath_dict['fld_modobs']]
        Target_filenames = ['kkk_%siter000_timestep_000' % mysettings['Model mode'],
                            'ModMeas_%siter001_timestep_001' % mysettings['Model mode']]

        # Relocate the received files and store them in a single binary file:
        for jj in range(0, len(Target_files2transfer)):

            inFileList = dirs.getdirs(Target_files2transfer[jj], mystr='ini', fullpath=False, onlylast=False)
            tempData = np.load(os.path.join(Target_files2transfer[jj], inFileList[0]))
            if (mysettings['Update scheme'] == 'full') and ('mod' in inFileList[0]):
                tempData = tempData.transpose().flatten(order='F')
            DataArray = np.empty((len(tempData), mysettings['No realizations']))

            for ii, cur_file in enumerate(inFileList):

                if ii == 0:
                    if tempData.ndim > 1:
                        if not isinstance(tempData, (int, float, str)):
                            tempData = np.squeeze(tempData, axis=1)
                    DataArray[:, ii] = tempData
                else:
                    addData = np.load(os.path.join(Target_files2transfer[jj], cur_file))
                    if (mysettings['Update scheme'] == 'full') and ('mod' in cur_file):
                        addData = addData.transpose().flatten(order='F')
                    if addData.ndim > 1:
                        if not isinstance(addData, (int, float, str)):
                            addData = np.squeeze(addData, axis=1)
                    DataArray[:, ii] = addData
                os.remove(os.path.join(Target_files2transfer[jj], cur_file))
            np.save(os.path.join(Target_files2transfer[jj], Target_filenames[jj]), DataArray)
