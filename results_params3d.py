# -*- coding: utf-8 -*-
"""
Plot some results from the EnKF assimilation
Created on Thursday 27 October 11:50:00 2015
@author: eesl
"""
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import os
import pdb
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'addfunc')))
import addfunc.fieldgen as fg
import addfunc.dirs as dm
import addfunc.gridmanip as gm
import addfunc.hgs as hg
import addfunc.mystats as mysst
import addfunc.matmult.matmul_opt as mm
import addfunc.matmult.mylikelihood as mml
import addfunc.plotResults as myplt

plt.rcParams.update({'figure.max_open_warning': 0})


def main():
    # --------------------------------------------------- #
    # -------------- Define settings -------------------- #
    # --------------------------------------------------- #
    modelname = 'test2b'  # Model under investigation: e.g '07072015'
    remarks = 'with_initialK_hete_transpparam'  # Provide remarks to identify various aspects of the assimilation
    mymode = 'tr_'  # Type of model to run: 'fl_' or 'tr_'
    synthetic = False  # Is it a synthetic example, meaning there is a synthetic true field?
    trim = True     # Trim the total model area to use only the elements from a smaller domain
    subgrid = True
    covariances = True
    # Model grid settings
    dx = 0.2
    dy = dx
    dz = 0.2
    xlim = [34, 67]  # [30, 70]     # xlim = [0, 100]
    ylim = [52, 68]  # [15, 35]      # ylim = [0, 50]
    zlim = [0, 6]  # zlim = [0, 1]
    myxticks = [30, 40, 50, 60, 70]  # [0, 20, 40, 60, 80, 100] # [10, 20, 30, 40, 50, 60, 70, 80, 90]
    myyticks = [35, 48, 62, 75]  # [0, 10, 20, 30, 40, 50] # [30, 40, 50, 60, 70, 80, 90]

    # 1. Plot settings:
    # ----------------------------------------------------------------- #
    varMax = 0.6
    logkMax = 1
    logKMin = -15
    plot = True
    eps = True
    subplotshape = 6530         # 326     # 4312      # shape=row,col,layers

    """ """

    # 0. Automatically generate necessary directories:
    # ----------------------------------------------------------------- #
    mystr = 'kkk'
    mystr = mystr + '_' + mymode
    mymaindir = os.path.join(dm.homedir(), '_dataAssim', modelname)  # Inversion directory
    # mymaindir = r'\\134.2.49.164\emilio\PhD_work\enkf_storetemp\01072016_alt'
    str_assimilation = '%s%s' % (modelname, remarks)

    modeldatadir = os.path.join(mymaindir, 'model_data')
    resdir = os.path.join(mymaindir, 'results_%s' % str_assimilation)
    realizationdir = os.path.join(mymaindir, 'realizations_%s' % str_assimilation)

    # Figure directories:
    # figdir = os.path.join(resdir, '_figs')
    paramdir = os.path.join(resdir, 'parameters')
    kkkdir = os.path.join(paramdir, 'kkk')
    dirList = [paramdir, kkkdir]
    if covariances is True:
        covdir = os.path.join(paramdir, 'Qyy')
        covdir_2 = os.path.join(paramdir, 'Qyy_diagonal')
        dirList.extend([covdir, covdir_2])

    for cur_dir in dirList:
        if os.path.isdir(cur_dir) is False:
            os.makedirs(cur_dir)

    # --------------------------------------------------- #
    # -------- Start with the calculations -------------- #
    # --------------------------------------------------- #

    # 0. Get the nodes and elements of the area of the model of interest:
    # --------------------------------------------------- #
    gridFile = os.path.join(modeldatadir, 'meshtecplot.dat')
    assert os.path.isfile(gridFile), 'No grid file in < model_data > folder. Get one!!'
    # inNodes = np.load(os.path.join(modeldatadir, 'subGrid.nodes.npy'))
    inElem = np.load(os.path.join(modeldatadir, 'subGrid.elements.npy'))
        # gm.sel_grid(gridFile, np.array(xlim), np.array(ylim), np.array(zlim))
    # elements = len(inElem)
    elY, elX, elZ = len(np.arange(ylim[0], ylim[1], dy)), len(np.arange(xlim[0], xlim[1], dx)), len(np.arange(zlim[0], zlim[1], dz))

    # 1. Load wells and adjust its coordinates for plotting:
    # ---------------------------------------------------
    well_coord = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(1, 2, 3))
    # wells_ids = np.loadtxt(os.path.join(modeldatadir, '_obswellscoord%s.dat' % mymode), usecols=(0,), dtype='S')
    well_coord[:, 0] *= 1 / dx
    well_coord[:, 1] *= 1 / dy
    well_points = np.array((well_coord[:, 0] - xlim[0] / dx, well_coord[:, 1] - ylim[0] / dy)).T
    # elif trim is False:
    #     well_points = well_coord

    # 2. Initiate relevant variables:
    # --------------------------------
    lstresdir = dm.getdirs(os.path.join(realizationdir, 'kkk'), mystr=mystr, fullpath=False, onlylast=False)
    if synthetic is True:
        nash, pears, rel_error, abs_error, param_bias, total_var, AAE,  = [], [], [], [], [], [], []
    AAESD = []
    # 3. Load output times:
    # ---------------------------------------------------------------- #
    outputtimes = np.loadtxt(os.path.join(modeldatadir, '_outputtimes%s.dat' % mymode))

    # 4. Load synthetic parameter truth ( if synthetic = True)
    # ---------------------------------------------------------------- #
    if synthetic is True:
        orig_field = np.loadtxt(os.path.join(mymaindir, 'model', 'Flow', 'kkkGaus.dat'), usecols=(1,))
        orig_field = np.log(orig_field[inElem])
        mean_value = np.mean(orig_field)  # Mean log K used to generate fields
        # orig_field = orig_field[inElem]

        orig_field_reshaped = np.transpose(np.reshape(orig_field, (elX, elY, elZ), order='F'), (1, 0, 2))
        # orig_field_reshaped = np.flipud(np.transpose(orig_field_reshaped, (1,0,2)))
        myplt.plotimshow_slices(orig_field_reshaped, elY, elX,
                         points=well_points,
                         title='Original Synthetic Field',
                         cbtitle='$ln(K)$',
                         savefile=os.path.join(kkkdir, 'Y_%ssynthetic.png' % mymode),
                         minval=logKMin, maxval=logkMax,
                         xlim=[0, elX],
                         ylim=[0, elY],
                         xticks=myxticks,
                         yticks=myyticks,
                         shape=subplotshape)

    # 5. Load initial ensemble and plot initial Qyy
    # ---------------------------------------------------------------- #
    ini_ensembleparam = np.load(os.path.join(realizationdir, 'kkk', lstresdir[0]))
    ini_ensembleparam = np.log(ini_ensembleparam)
    if subgrid is not True:
        ini_ensembleparam = np.log(ini_ensembleparam[inElem, :])

    if mymode == 'fl_':
        lstresdir = lstresdir[1:]

    if covariances is True:
        # orig_Qyy = mml.autocov(ini_ensembleparam, diag=True, parallel=False)
        orig_Qyy = np.empty((ini_ensembleparam.shape[0],))
        for ii in range(0, orig_Qyy.shape[0]):
            cur_chunk = ini_ensembleparam[ii, :]
            cur_calc = (cur_chunk - cur_chunk.mean())
            orig_Qyy[ii] = (1. / (ini_ensembleparam.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)

        np.save(os.path.join(covdir_2, 'Qyy_diag_%siter000_timestep000' % mymode), orig_Qyy)

        orig_Qyy_reshaped = np.transpose(np.reshape(orig_Qyy, (elX, elY, elZ), order='F'), (1, 0, 2))
        ini_ensembleparam_reshaped = np.transpose(np.reshape(ini_ensembleparam.mean(axis=1), (elX, elY, elZ), order='F'), (1, 0, 2))
        myplt.plotimshow_slices(orig_Qyy_reshaped, elY, elX, points=well_points,
                                title='Assimilation step 0: Initial Ensemble',
                                cbtitle='Variance $ln(K)$',
                                savefile=os.path.join(covdir, 'Qyy_diag_%siter000_timestep000.png' % mymode),
                                minval=0.01, maxval=varMax,
                                xlim=[0, elX],
                                ylim=[0, elY],
                                xticks=myxticks,
                                yticks=myyticks,
                                shape=subplotshape)
        myplt.plotimshow_slices(ini_ensembleparam_reshaped, elY, elX, points=well_points,
                                title='Assimilation step 0: Initial Ensemble',
                                cbtitle='$ln(K)$',
                                savefile=os.path.join(kkkdir, 'Y_%siter000_timestep000.png' % mymode),
                                minval=logKMin, maxval=logkMax,
                                xlim=[0, elX],
                                ylim=[0, elY],
                                xticks=myxticks,
                                yticks=myyticks,
                                shape=subplotshape)

    # 5. Plot stuff:
    # --------------
    for curidx, ii in enumerate(lstresdir):
        params = np.load(os.path.join(realizationdir, 'kkk', ii))
        params = np.log(params)
        if subgrid is not True:
            params = params[inElem, :]
        if covariances is True:
            Qyy = np.empty((params.shape[0],))
            for pp in range(0, Qyy.shape[0]):

                cur_chunk = params[pp, :]
                cur_calc = (cur_chunk - cur_chunk.mean())
                Qyy[pp] = (1. / (params.shape[1] - 1)) * np.dot(cur_calc, cur_calc.T)
            np.save(os.path.join(covdir_2, 'Qyy_diag_%siter001_timestep%s' % (mymode, ii[2:-4])), orig_Qyy)
        # 5.1 Statistical analysis:
        # -------------------------
        if synthetic is True:
            nash.append(mysst.nash_sutcliffe(params.mean(axis=1), orig_field))
            pears.append(mysst.Pearson(orig_field, params.mean(axis=1)))
            rel_error.append(mysst.relative_error(params.mean(axis=1), orig_field))
            abs_error.append(mysst.absolute_error(params.mean(axis=1), orig_field))
            param_bias.append(mysst.param_bias(orig_field, params.mean(axis=1)))
            # total_var.append(mysst.tot_var(params, np.log(mean_value)))
            total_var.append(mysst.tot_var(params, np.mean(orig_field))) # todo: check the total variance now
            AAE.append(mysst.mean_abs_error(params.mean(axis=1), orig_field))
        AAESD.append(mysst.mean_ensemble_std(params, params.mean(axis=1)))

        # 5.2 Plot fields and covariances:
        # ---------------------------------
        if plot is True:
            if covariances is True:

                myplt.plotimshow_slices(np.transpose(np.reshape(Qyy, (elX, elY, elZ), order='F'), (1, 0, 2)),
                                 elY, elX, points=well_points,
                                 title='Assimilation step %d: %s [sec]' % (curidx + 1, outputtimes[curidx]),
                                 cbtitle='Variance $ln(K)$',
                                 savefile=os.path.join(covdir, 'Qyy_diag_%s.png' % ii[2:-4]),
                                 minval=0.01, maxval=varMax,
                                 xlim=[0, elX],
                                 ylim=[0, elY],
                                 xticks=myxticks,
                                 yticks=myyticks,
                                 shape=subplotshape)

            myplt.plotimshow_slices(np.transpose(np.reshape(params.mean(axis=1), (elX, elY, elZ), order='F'), (1, 0, 2)),
                             elY, elX, points=well_points,
                             title='Assimilation step %d: %s [sec]' % (curidx + 1, outputtimes[curidx]),
                             cbtitle='$ln(K)$',
                             savefile=os.path.join(kkkdir, 'Y_%s.png' % ii[2:-4]),
                             minval=logKMin, maxval=logkMax,
                             xlim=[0, elX],
                             ylim=[0, elY],
                             xticks=myxticks,
                             yticks=myyticks,
                             shape=subplotshape)
            if synthetic is True:
                diff = orig_field - params.mean(axis=1)
                myplt.plotimshow_slices(np.transpose(np.reshape(diff, (elX, elY, elZ), order='F'), (1, 0, 2)),
                                 elY, elX, points=well_points,
                                 title='Assimilation step %d: %s [sec]' % (curidx + 1, outputtimes[curidx]),
                                 cbtitle='Difference in $ln(K)$',
                                 savefile=os.path.join(kkkdir, 'Diff_Y_%s.png' % ii[2:-4]),
                                 minval=-2, maxval=2,
                                 xlim=[0, elX],
                                 ylim=[0, elY],
                                 xticks=myxticks,
                                 yticks=myyticks,
                                 shape=subplotshape)

            # 5.3 Plot pdf´s:
            # ---------------
            if synthetic is True:

                myplt.plothist_ensemble(params, X_orig=orig_field,
                                        title='Time step:%d t=%s s' % (curidx + 1, outputtimes[curidx]),
                                        norm=False, cum=False,
                                        textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                                        xlabel='Ln K',
                                        savefile=os.path.join(kkkdir, 'Ensemble_pdf_%s.png' % ii[2:-4]),
                                        eps=eps)
                # 5.4 Plot cdf´s:
                # ---------------
                myplt.plothist_ensemble(params, X_orig=orig_field,
                                        title='Time step:%d t=%s s' % (curidx + 1, outputtimes[curidx]),
                                        norm=False, cum=True,
                                        textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                                        xlabel='Ln K',
                                        savefile=os.path.join(kkkdir, 'Ensemble_cdf_%s.png' % ii[2:-4]),
                                        eps=eps)
                # 5.5 Plot a calibration graphic:
                # ---------------------------
                # print('Time step %s--> NS-coeff: %s\nPearsons: %s' % (idx, nash, pears))
                plt.plot(orig_field, params.mean(axis=1), 'rx')
                plt.title('Synthetic vs modeled parameters.\nTime step:%d t=%s s' % (curidx + 1, outputtimes[curidx]))
                plt.xlabel('Original parameters')
                plt.ylabel('Modeled parameters')
                plt.grid(True)
                plt.xlim([-9, -2])
                plt.ylim([-9, -2])
                plt.savefig(os.path.join(paramdir, 'calibplot_EnsembleMean_%s.png' % ii[2:-4]), dpi=600, format='png')
                plt.close()

    if synthetic is True:
        # 5.6 Plot additional statistics
        # ---------------------------
        myplt.xyplot(np.arange(1, len(total_var) + 1, 1), total_var,
                     title='Total variance',
                     textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                     label='Total variance',
                     xlabel='Time step',
                     ylabel='Variance',
                     savefile=os.path.join(paramdir, 'total_variance_%s.png' % mymode), eps=eps)
        myplt.xyplot(np.arange(1, len(param_bias) + 1, 1), param_bias,
                     title='Parameter bias',
                     textbox='',
                     label='',
                     xlabel='Time step',
                     ylabel='Parameter bias',
                     savefile=os.path.join(paramdir, 'parameter_bias_%s.png' % mymode), eps=eps)
        myplt.xyplot(np.arange(1, len(AAE) + 1, 1), AAE,
                     title='Average Absolute Error',
                     textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                     label='',
                     xlabel='Time step',
                     ylabel='AAE Ln K',
                     savefile=os.path.join(paramdir, 'AAE_%s.png' % mymode), eps=eps)
        myplt.xyplot(np.arange(1, len(AAESD) + 1, 1), AAESD,
                     title='Average Ensemble Standard Deviation',
                     textbox='Mode=%s \n%s realizations' % (mymode, params.shape[-1]),
                     label='',
                     xlabel='Time step',
                     ylabel='AESD Ln(K)',
                     savefile=os.path.join(paramdir, 'AAESD_%s.png' % mymode), eps=eps)
    if synthetic is True:
        nash.append(mysst.nash_sutcliffe(params.mean(axis=1), orig_field))
        pears.append(mysst.Pearson(params.mean(axis=1), orig_field))
        rel_error.append(mysst.relative_error(params.mean(axis=1), orig_field))
        abs_error.append(mysst.absolute_error(params.mean(axis=1), orig_field))
        param_bias.append(mysst.param_bias(orig_field, params.mean(axis=1)))
        total_var.append(mysst.tot_var(params, np.log(mean_value)))
        AAE.append(mysst.mean_abs_error(params.mean(axis=1), orig_field))
        AAESD.append(mysst.mean_ensemble_std(params, params.mean(axis=1)))

        np.savetxt(os.path.join(paramdir, 'errors_summarize_%s.txt' % mymode),
                   np.array((nash, param_bias, total_var, AAE, AAESD)).T,
                   header='nash, param_bias, total_var, AAE, AAESD')
        # # 5.7 Load and save field parameters from data assim results to bin files for new assimilation:
        # # --------------------------------
        # sourceFile = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilterflow\_dataAssim\Results_assim_2d_withnoise\Y_fl_iter001_timestep059.npy'
        # targetPath = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilterflow\_dataAssim\realizations_2d_withnoise_2\kkkfiles'
        # Yfields = np.load(sourceFile)
        # for ii in range(0, Yfields.shape[-1]):
        #     np.save(os.path.join(targetPath, 'kkkGaus_%.5d.npy' % (ii + 1)), np.transpose(np.exp(Yfields[:, ii])))


if __name__ == '__main__':
    main()


# A=np.empty((64,64,64)) #This is the data array
# def f(x,y):
#     return np.sin(x/(2*np.pi))+np.cos(y/(2*np.pi))
# xx,yy= np.meshgrid(range(64), range(64))
# for x in range(64):
#     A[:,:,x]=f(xx,yy)*np.cos(x/np.pi)

